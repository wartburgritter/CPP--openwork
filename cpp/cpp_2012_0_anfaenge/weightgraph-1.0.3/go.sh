#!/bin/sh

./makepackage.sh

DEB=`ls /tmp/weightgraph*.deb|sort|tail -1`

if [ -z $DEB ]; then
  echo ".deb not found"
  exit
fi

FILENAME=`basename $DEB`

scp $DEB n900:MyDocs && \
  ssh n900 "sudo dpkg -i MyDocs/$FILENAME; run-standalone.sh /usr/bin/weightgraph"
