#ifndef WEIGHTSTATSVIEW_H
#define WEIGHTSTATSVIEW_H

#include <QWidget>
#include <QLabel>
#include "weightdata.h"

class WeightStatsView : public QWidget
{
  Q_OBJECT
public:
  explicit WeightStatsView(WeightDataModel *wdm, QWidget *parent = 0);


signals:

public slots:
  void updateStats();
private:
  WeightDataModel *wdm;
  QLabel *last;
  QLabel *change;
};

#endif // WEIGHTSTATSVIEW_H
