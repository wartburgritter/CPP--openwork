#include "batteryindicator.h"
#include "ui_batteryindicator.h"


BatteryIndicator::BatteryIndicator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BatteryIndicator),
    deviceInfo(NULL)
{
    ui->setupUi(this);
    setupGeneral();

}

BatteryIndicator::~BatteryIndicator()
{
    delete ui;
}
void BatteryIndicator::setupGeneral()
{
    deviceInfo = new QSystemDeviceInfo(this);

    ui->batteryLevelBar->setValue(deviceInfo->batteryLevel());

    connect(deviceInfo, SIGNAL(batteryLevelChanged(int)),
            ui->batteryLevelBar, SLOT(setValue(int)));

}
