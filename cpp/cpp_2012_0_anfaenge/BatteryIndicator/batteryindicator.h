#ifndef BATTERYINDICATOR_H
#define BATTERYINDICATOR_H

#include <QDialog>
#include <QSystemInfo>

QTM_USE_NAMESPACE

namespace Ui {
    class BatteryIndicator;
}


class BatteryIndicator : public QDialog
{
    Q_OBJECT

public:
    explicit BatteryIndicator(QWidget *parent = 0);
    ~BatteryIndicator();

private:
    Ui::BatteryIndicator *ui;
    void setupGeneral();

    QSystemDeviceInfo *deviceInfo;
};

#endif // BATTERYINDICATOR_H
