#include <QtGui/QApplication>
#include "batteryindicator.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    BatteryIndicator w;

    w.showFullScreen();

    return a.exec();
}
