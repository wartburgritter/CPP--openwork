#include <iostream.h>

/* Definition der Funktion (Methode) add
   Typ des Rueckgabewertes: int
   Funktionsname          : add
   Argumentenliste        : ( int x, int y)
   eigentliche Funktion   : innerhalb des Blocks {...}
   Rueckgabewert          : z     */

int add( int x, int y)
{
 int z=x+y;
 return z;
}

// Bsp f�r Methode mit Rueckgabewert void => Angabe des Rueckgabewertes nicht noetig
void ausgabe( int z)
{
 cout<<"Das Ergebnis von a+b ist:"<<z<<endl;
}


// Hauptprogramm

int main()
{
 int a=5;
 int b=12;

 int c=add(a,b);
 cout<<"a="<<a
    <<",c="<<c
  <<",a+c="<<add(a,c)<<endl;

 ausgabe(c);

 return 0;  // denke ist der R�ckgabewert der mainfunktion
            // des Hauptprogrammes nur zur akuraten Form
}