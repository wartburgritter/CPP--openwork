#include <iostream>

int main()
{
  float a, b, h, flaeche;
  int n;

  cout << "Linke Grenze ? "; cin >> a;
  cout << "Rechte Grenze ? "; cin >> b;
  cout << "Anzahl der Rechtecke ? "; cin >> n;

  if (n <= 0) 
    return 1;

  if (a > b)
  {
    float c = a;
    a = b;
    b = c;
  }

  h = (b-a)/n;
  flaeche = 0;
  float x = a;

  do
  {
    flaeche += h*x*x;
    x += h;
  } while (x < b);

  cout << "Fl�che unter Parabel ist " << flaeche << endl;

  return 0;
} 
