#include <cstdlib>
#include <iostream>
#include "../befehl.h"

const unsigned int ANZAHL = 3;

// --------------------------------------------------------
class RobotController : public KommunikationsController
{
public:
  RobotController() :
    KommunikationsController() {}
  int sendeBefehl(RobotBefehl _befehl,
    short arg1=0, short arg2=0);
};

// --------------------------------------------------------
int RobotController::sendeBefehl(
  RobotBefehl _befehl, short arg1, short arg2)
{
  Befehl einBefehl(_befehl, arg1, arg2);
  int erg = msgsnd(msgid, (void*)&einBefehl, 
      sizeof(Befehl)-sizeof(long), 0);
  if (erg == -1)
  {
    cerr << "msgsnd() fehlgeschlagen!" << endl;
    return -1;
  }
  
  return 0;
}  
     
// --------------------------------------------------------
class HanoiController : public RobotController
{
public:
  HanoiController() : 
    RobotController() {}
  void bewegeScheiben(short _tiefe, short _pos1,  
    short _pos2, short _pos3);
};

// --------------------------------------------------------
void HanoiController::bewegeScheiben(short _tiefe,
    short _pos1, short _pos2, short _pos3) 
{
  if (_tiefe > 1)
    bewegeScheiben(_tiefe-1, _pos1, _pos3, _pos2);
  
  sendeBefehl( BEWEGE_OBJEKT, _pos1, _pos2);
  
  if (_tiefe > 1)
    bewegeScheiben(_tiefe-1, _pos3, _pos2, _pos1);
}    
         
// --------------------------------------------------------
int main()
{
  HanoiController hanoi;
  
  if (hanoi.initialisiereVerbindung() == -1)
    return 1;
    
  hanoi.bewegeScheiben(ANZAHL, 1, 2, 3);
  hanoi.sendeBefehl(ENDE);
  
  return 0;
}          



