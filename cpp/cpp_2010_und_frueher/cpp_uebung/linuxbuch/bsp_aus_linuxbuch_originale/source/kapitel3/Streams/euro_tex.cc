#include <iostream>
#include <fstream>
#include <strstream>
#include <cstdlib>
#include <unistd.h>

main()
{
  int i,j;
  const double wk = 1.95583;

  ostrstream ostr;
  ostr << "euro." << getpid() << ".tex" << ends;

  ofstream out(ostr.str());
  out.precision(2);
  out.setf(ios::fixed);

  out << "\\documentclass{article}" << endl;
  out << "\\begin{document}" << endl;
  out << "\\section*{Umrechungstabelle DM -- Euro}" << endl;
  out << "\\begin{tabular}{rr|rr}" << endl;
  out << "DM & EUR & EUR & DM \\\\ \\hline" << endl;

  for(i=1;i<=1000;i*=10) {
    for(j=1;j<=5 && i*j<=1000;j+=1) {
      out << (float)i*j << " & " 
	  << i*j/wk << " & "
	  << (float)i*j << " & "
	  << i*j*wk << " \\\\" << endl;
    }
  }
  out << "\\end{tabular}" << endl;
  out << "\\end{document}" << endl;

  ostrstream ltcmd;
  ltcmd << "latex " << ostr.str() << ends;
  if (!system(ltcmd.str())) {
    ostrstream dvicmd;
    dvicmd << "dvips euro." << getpid() 
	   << ".dvi -o" << ends;
    if (system(dvicmd.str())) 
      cerr << "PS-Datei konnte nicht erzeugt werden!" << endl;
  }
  else
    cerr << "DVI-Datei konnte nicht erzeugt werden!" << endl;
  return(0);
}


