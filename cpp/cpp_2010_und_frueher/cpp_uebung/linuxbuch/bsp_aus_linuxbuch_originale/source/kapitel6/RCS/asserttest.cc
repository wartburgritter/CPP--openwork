// Datei $RCSfile: asserttest.cc,v $
// zuletzt bearbeitet von $Author: thomas $ 
// $Date: 2000/03/07 10:07:51 $, $Revision: 1.4 $

/*
 * $Log: asserttest.cc,v $
 * Revision 1.4  2000/03/07 10:07:51  thomas
 * $Id$ eingebaut
 *
 * Revision 1.3  2000/03/07 10:02:17  thomas
 * RCS-Makros eingebaut
 *
 */

#include <string>
#include <iostream>
#include <cassert>

const string Id = "$Id: asserttest.cc,v 1.4 2000/03/07 10:07:51 thomas Exp $";

// Funktion: repeatedOutput
// Parameter: 
//   ostream* _o: Ausgabestream
//   const string& _s: String
//   unsigned short _n: Wiederholung
// Bedingung: _o != 0
void repeatedOutput(ostream* _o, 
  const string& _s, unsigned short _n)
{
  assert(_o);  // entspricht _o!=0
  for(unsigned i=0; i<_n; i++)
    *_o << _s;
}

int main()
{
  cout << "Testprogramm f�r assert()" << endl;
  repeatedOutput(&cout, "-", 30);
  cout << endl;
  repeatedOutput(0, "?", 30);

  return 0;
}
