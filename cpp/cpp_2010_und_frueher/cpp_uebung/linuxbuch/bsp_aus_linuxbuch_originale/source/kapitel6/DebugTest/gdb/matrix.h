// Datei: matrix.h
#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <string>

class SymMatrix
{
public:
  SymMatrix();
  SymMatrix(unsigned int _n);
  SymMatrix(const SymMatrix& _mat);
  ~SymMatrix();
  
  bool enter();
  bool read(const string& _fname);
  double at(unsigned int _i, unsigned int _j) const;
  double& at(unsigned int _i, unsigned int _j);
  unsigned int getSize() const;
  SymMatrix subMatrix(unsigned int _k) const;
  
  friend double determinant(const SymMatrix& _a);
  
private:
  double** dat;
  unsigned int size;
  
  void initMemory(unsigned int _n);
  void freeMemory();
  void print();
};
    
#endif
