// Datei asserttest.cc

#include <cmath>
#include <iostream>
#include <cassert>

// Funktion: logarithmus(double x)
// Bedingung: x > 0
double logarithmus(double x)
{
  assert(x>0.0);
  return log(x);
}

int main()
{
  double w1 = logarithmus(1);
  cout << "Logarithmus von 1: " << w1 << endl;

  double w2 = logarithmus(-1);
  cout << "Logarithmus von -1: " << w2 << endl;

  return 0;
}
