#ifndef _DATE_H_
#define _DATE_H_

#include <iostream>

class Date
{    
 private:
  unsigned short day;
  unsigned short month;
  unsigned short year;

 public:
  Date();          

  Date(const Date& rDat) :
    day(rDat.day), month(rDat.month), 
    year(rDat.year) 
    {}

  Date(unsigned short _day, unsigned short _month,
       unsigned short _year) :
    day(_day), month(_month), year(_year) {}

  unsigned short setDay( int _day )
    { return day = _day; };
  unsigned short getDay() const                   
    { return day; };
  unsigned short setMonth( int _month )
    { return month = _month; };
  unsigned short getMonth() const                 
    { return month; };
  unsigned short setYear( int _year)     
    { return year = _year; };
  unsigned short getYear() const                  
    { return year; };

  Date inc();   

  // befreundete Testfunktion
  friend bool correctDate(const Date& d);
  friend ostream& operator<<(ostream& o, const Date& d);
};

#endif // _DATE_H_
