#include <iostream>
#include "date.h"
#include "birthlist.h"

int main()
{
  // Bestimme aktuelles Datum
  Date today;
  Date tomorrow;

  // erhoehe tomorrow um einen 1 = morgen
  tomorrow.inc();

  // lade Geburtstagsliste
  BirthList liste;
  if (liste.load(".birthlist") != 0)
    return -1;

  // ueberpruefe die Daten
  liste.check(today);
  liste.check(tomorrow);

  return 0;
}
