//
// paramref.cc
// Beispielprogramm fuer Parameteruebergabe als Referenz
//

#include <iostream>

void swap_values (int _x, int _y)
{
  int temp = _x;
  _x = _y;
  _y = temp;
  return;
}

void swap_refs (int& _x, int& _y)
{
  int temp = _x;
  _x = _y;
  _y = temp;
  return;
}

int main (void)
{
  int big = 10;
  int small = 20; 

  cout << "big1: "<< big << " small1: "<< small << endl;
  
  swap_values (big, small);
  cout << "big2: "<< big << " small2: "<< small << endl;
  
  swap_refs (big, small);
  cout << "big3: "<< big << " small3: "<< small << endl;

  return 0;
}

