#include <iostream>

// Prototypen der Funktionen
int add( int x, int y);
void ausgabe( int z);

// Hauptprogramm
int main()
{
 int a=5;
 int b=12;
 int c=add(a,b);
 ausgabe(c);
 ausgabe(add(a,c));

 return 0;
}

//Definition der Funktionen
int add( int x, int y)
{
 return (x+y);
}

void ausgabe( int z)
{
 cout<< "Ergebnis: " << z << endl;
}

