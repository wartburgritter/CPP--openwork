/****************************************************************************
** Form interface generated from reading ui file './bmidlg.ui'
**
** Created: Sa Dez 12 11:50:27 2009
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef BMIMAINFORM_H
#define BMIMAINFORM_H

#include <qvariant.h>
#include <qmainwindow.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QGroupBox;
class QTextEdit;
class QLabel;
class QButtonGroup;
class QRadioButton;
class QLineEdit;
class QPushButton;

class BmiMainForm : public QMainWindow
{
    Q_OBJECT

public:
    BmiMainForm( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~BmiMainForm();

    QGroupBox* GroupBox2;
    QTextEdit* teTextbox;
    QLabel* lBmi;
    QButtonGroup* ButtonGroup1;
    QRadioButton* rbFemale;
    QRadioButton* rbMale;
    QGroupBox* GroupBox1;
    QLabel* TextLabel3;
    QLabel* TextLabel2_2;
    QLabel* TextLabel2;
    QLineEdit* leWeight;
    QLabel* TextLabel3_2;
    QLineEdit* leHeight;
    QPushButton* pbEval;

public slots:
    virtual void calcSlot();

protected:
    QGridLayout* Layout9;
    QSpacerItem* Spacer3;
    QSpacerItem* Spacer3_2;

protected slots:
    virtual void languageChange();

};

#endif // BMIMAINFORM_H
