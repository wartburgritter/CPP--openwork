#include <iostream>
#include "raumschiff.h"
using namespace std;


/*Definition der Klasse Raumfahrzeug
************************************/

//Kontstruktor
Raumfahrzeug::Raumfahrzeug()
{
  cout << "Contructor!!!" << endl;
//  hoechstgeschw=297000;
}

//Destruktor
Raumfahrzeug::~Raumfahrzeug()
{       // zu Testzwecken: mache hier eine Ausgabe, damit man den Aufruf sehen kann
        cout << "Destructor!!!" << endl << endl;
}

// Definition der Methode setGeschwindigkeit
bool Raumfahrzeug::setGeschwindigkeit(unsigned long _tempo)
 {
  cout << "An Methode setGeschwindigkeit wurde der Wert "
       << _tempo
       << " übergeben." << endl;

  if(_tempo>hoechstgeschw)
    cout << "Geschwindigkeit ist grösser als die Höchstgeschwindigkeit." << endl;
    return false;

  geschw=_tempo;
  
  cout << "Die Methode setGeschwindigkeit hat der Variablen geschw den Wert "
       << geschw
       << " zugewiesen." << endl;

  return true;
 }

// Definition der Methode getGeschwindigkeit
unsigned long Raumfahrzeug::getGeschwindigkeit()
 {
  return geschw;
 }
