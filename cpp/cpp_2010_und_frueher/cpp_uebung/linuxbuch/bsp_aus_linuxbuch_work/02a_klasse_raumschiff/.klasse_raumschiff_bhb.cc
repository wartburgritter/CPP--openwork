#include <iostream>

using namespace std;





/*Definition der Klasse Raumfahrzeug
************************************/
//Kontstruktor
Raumfahrzeug::Raumfahrzeug()
{
  cout << "Contructor!!!" << endl;
  //hoechstgeschw=297000;
}

//Destruktor
Raumfahrzeug::~Raumfahrzeug()
{       // zu Testzwecken: mache hier eine Ausgabe, damit man den Aufruf sehen kann
        cout << "Destructor!!!" << endl;
}


// Definition der Methode setGeschwindigkeit
bool Raumfahrzeug::setGeschwindigkeit(unsigned long _tempo)
 {
  if(_tempo>hoechstgeschw)
    return false;

  geschw=_tempo;
  return true;
 }

// Definition der Methode getGeschwindigkeit
unsigned long Raumfahrzeug::getGeschwindigkeit()
 {
  return geschw;
 }



/*Hauptprogramm zum Test der Klasse Raumfahrzeug
************************************************/
int main (void)
{
  cout<<"Programmzum Test der Klasse Raumfahrzeug"<<endl;

  Raumfahrzeug ufo;
  ufo.setGeschwindigkeit(1000000);
  cout<<"Ufo fliegt "<<ufo.getGeschwindigkeit()<<" km/s"<<endl;
  // shuttle.andocken(ufo);


  Raumfahrzeug shuttle;


  return 0;
}


/*Kompileraufruf
g++ -Wall -o klasse_raumschiff_bhb klasse_raumschiff_bhb.cc
*/
