#include <iostream>
#include "Datum.hh"
using namespace std;

Datum heutigesDatum;

int main()
{
  cout << "Heute ist der ";
  heutigesDatum.ausgeben();
  return 0;
}

/*Kompileraufruf
g++ -Wall -o DatumTest Datum.cc Datum.cc
*/
