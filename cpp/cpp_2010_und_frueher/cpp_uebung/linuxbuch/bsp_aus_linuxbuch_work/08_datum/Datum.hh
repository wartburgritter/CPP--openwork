// Datei Datum.hh
//
// Datumsklasse zur Illustration von Konstruktoren und Destruktoren
// (C) 2000, T. Wieland. 

#ifndef _DATUM_H_
#define _DATUM_H_

class Datum
{
private:
  unsigned int t, m, j;

public:
  Datum();
  ~Datum();

//  Datum(unsigned int _t,
//  	unsigned int _m,
//  	unsigned int _j);
//  Datum(unsigned int _t);
//  Datum(const Datum& _datum);
//  void setze(unsigned int _t,
//  	     unsigned int _m,
//  	     unsigned int _j);
//  void setzeAufHeute();
//  void ausgeben() const;
//  bool istSchaltjahr();
};

#endif // _DATUM_H_
