//enum Bewegung { steht, startet, fliegt, landet, hyperraum};
//enum Funktion { voll, angeschossen, defekt, zerstoert};
// Deklaration der Typen Bewegung und Funktion kann auch 
// innerhalb der Klasse stehen


/*Deklaration der Klasse Raumfahrzeug
*************************************/
class Raumfahrzeug
{
private:
  //  string bezeichnung;
  //  string herkunft;
  //  float hoehe;
  unsigned long geschw;
  unsigned long hoechstgeschw;
  //  Bewegung zustand;
  //  Funktion grad;

public:
   Raumfahrzeug();       // Konstruktor
  ~Raumfahrzeug();       // Destruktor
  
  //  void starten();
  //  void landen();
  bool setGeschwindigkeit(unsigned long _tempo);
  unsigned long getGeschwindigkeit() const;
  bool setHoechstgeschwindigkeit(unsigned long _tempo);
  unsigned long getHoechstgeschwindigkeit() const;
  //  void andocken(Raumfahrzeug_anderes);
};
