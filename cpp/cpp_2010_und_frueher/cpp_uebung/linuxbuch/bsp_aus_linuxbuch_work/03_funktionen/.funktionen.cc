/* Funktionen und Methoden ab S.70*/
#include <iostream>
using namespace std;

/* Definition der Funktion (Methode) add
-----------------------------------------
   Typ des Rueckgabewertes: int
   Funktionsname          : add
   Argumentenliste        : ( int x, int y)
   eigentliche Funktion   : innerhalb des Blocks {...}
   Rueckgabewert          : z     */

int add( int x, int y)
{
 int z=x+y;
 return z;
}

int sub( int x, int y)
{
 int z=x-y;
 return z;
}

// Bsp f�r Methode mit Rueckgabewert void => Angabe des Rueckgabewertes nicht noetig
void ausgabeAdd( int z)
{
 cout<<"Das Ergebnis von a+b ist:"<<z<<endl;
}

void ausgabeSub( int z)
{
 cout<<"Das Ergebnis von a-b ist:"<<z<<endl;
}


// Hauptprogramm
//---------------

int main()
{
 int a=5;
 int b=12;
 cout << "a=" << a
      << ",c=" << c << endl;
 
 int c=add(a,b);
 cout << "a+c=" << add(a,c)
 ausgabe(c);

 int d=sub(a,b);
 cout       << ",a+c=" << add(a,c) << endl;

 ausgabe(c);

 return 0;  // denke ist der R�ckgabewert der mainfunktion
            // des Hauptprogrammes nur zur akuraten Form
}

/*Kompileraufruf
g++ -Wall -o funktionen funktionen.cc
*/
