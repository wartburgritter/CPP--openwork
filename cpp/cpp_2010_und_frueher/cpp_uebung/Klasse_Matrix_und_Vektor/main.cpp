// Hauptprogramm zum Testen der matrix-Klasse

  #include <iostream>

  #include "matrix.h"
  #include "vektor.h"
  
  using namespace std;
  
  void main()
  {
    fstream datei_1("matrix_1.dat", ios::in);   // Datei matrix_1.dat
    fstream datei_2("matrix_2.dat", ios::in);   // Datei matrix_2.dat

    vektor v1(0.5, 0.5, 0.5);
    vektor v2(1, 1, 1);
    vektor v3(0.78, 0.21, -1.2324);
    vektor v4;
    matrix A(v1, v2, v3);
    matrix B(v3, v2, v1);
    matrix C, D, E;

    C.eingabe(datei_1);   // Setzen der Matrix C
    datei_2 >> D;         // Setzen der Matrix D
    cout << A << B << endl << endl;
    cout << " A * B = " << A*B << endl;
    cout << C << endl;
    cout << D << endl;
    cin >> E;
    cout << endl << "E_gesetzt: " << endl << C << endl;
}
