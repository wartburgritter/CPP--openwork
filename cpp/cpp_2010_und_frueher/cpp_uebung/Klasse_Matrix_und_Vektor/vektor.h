  // header-File zur Klasse Vektor

  #ifndef _vektor_h
  #define _vektor_h

  #include<iostream>

  class vektor {
    friend class matrix;
    private:
      double elem[3];            // 3x1-Vektor als Array implementiert

    public:
      // Konstruktor, Copy-Konstruktor und Destruktor:
      vektor(double = 0, double = 0, double = 0);
      vektor(const vektor&);
      ~vektor();

      double liefer(int);        // liefert den Inhalt von vektor[int]
      void setze(int, double);   // setzt vektor[int] auf den double-Wert
      void ausgabe();            // Ausgabe des Vektors

      // berladen der arithmetischen Operatoren
      vektor& operator-();                 // un�res Minus
      vektor operator+(const vektor&);     // Vektor-Vektor-Addition
      vektor operator-(const vektor&);     // Vektor-Vektor-Subtraktion
      vektor& operator+=(const vektor&);   // Vektor-Vektor-Addition
      vektor& operator-=(const vektor&);   // Vektor-Vektor-Subtraktion

      vektor operator*(double);            // Vektor-double-Multiplikation
      vektor& operator*=(double);          // Vektor-double-Multiplikation

      double operator*(const vektor&);     // Skalarprodukt (Vektor x Vektor)

      friend vektor operator*(double, const vektor&);   // double-Vektor-Multiplikation

      ostream& operator<<(ostream&);       // �berladen des Ausgabe-Operators
  };

    // Standard-Ausgabeoperator:
    // - mu~ global berladen werden !
    // - hier: inline definiert

  inline ostream& operator<<(ostream& strm, vektor v)
  {
    v.ausgabe();
    return strm;
  };

  #endif 