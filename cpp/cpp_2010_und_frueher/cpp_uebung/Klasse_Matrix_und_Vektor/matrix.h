// header-File zur Klasse "Matrix"

  #ifndef _matrix_h
  #define _matrix_h

  #include "vektor.h"      // Einbinden der Vektor-Definitionen
  #include <iostream>    // Standard-I/O-Bibliothek einbinden
  #include <fstream>     // Datei-I/O-Bibliothek einbinden

  class matrix {
    private:
      vektor elem[3];      // Matrix wird implementiert als drei Spaltenvektoren

    public:
      // Deklaration des Konstruktors, Copy-Konstruktors und des Destruktors
      matrix(vektor = vektor(0), vektor = vektor(0), vektor = vektor(0));
      matrix(matrix&);
      ~matrix();

      int zulaessig(int, int);       // Pr�ft, ob (int, int) einem Matrix-Indexpaar entspricht.
      void setze(int, int, double);  // Setzt an die Stelle (int, int) in der Matrix den Wert double.
      double liefer(int, int);       // liefert den Eintrag (int, int) aus der Matrix

      void eingabe();                // Eingabe einer Matrix (�ber Standard-Input)
      void eingabe(fstream&);        // Eingabe einer Matrix (aus einer Datei heraus).
      void ausgabe();                // Ausgabe der Matrix

      matrix& operator-();           // un"res Minus

      matrix operator*(const matrix&);    // Matrix-Matrix-Multiplikation
      matrix operator*(const double);     // Matrix-Double-Multiplikation
      vektor operator*(const vektor&);    // Matrix-Vektor-Multiplikation
      matrix& operator*=(const matrix&);  // Matrix-Matrix-Multiplikation
      matrix& operator*=(const double);   // Matrix-double-Multipliaktion
      vektor& operator*=(const vektor&);  // Matrix-Vektor-Multipliaktion

      matrix operator+(const matrix&);    // Matrix-Matrix-Addition
      matrix& operator+=(const matrix&);  // Matrix-Matrix-Addition

      matrix operator-(const matrix&);    // Matrix-Matrix-Subtraktion
      matrix& operator-=(const matrix&);  // Matrix-Matrix-Subtraktion

      friend matrix operator*(double, const matrix&);  // double-Matrix-Multiplikation
  };

  /*
   - Globales �berladen der Ein-/Ausgabeoperatoren
   - hier: inline definiert
  */

  inline ostream& operator<<(ostream& strm, const matrix& A)
  {
    A.ausgabe();      // Aufruf der Elementfunktion ausgabe()
    return strm;      // R�ckgabe des Streams
  };

  inline fstream& operator>>(fstream& strm, matrix& A)
  {
    A.eingabe(strm);  // Aufruf der Elementfunktion eingabe(fstream&)
    return strm;      // R�ckgabe des Streams
  }

  inline istream& operator>>(istream& strm, matrix& A)
  {
    A.eingabe();      // Aufruf der Elementfunktion eingabe()
    return strm;      // R�ckgabe des Streams
  }

  #endif 