/* Datei auto.cpp
   In dieser Datei werden die Methoden der Klasse implementiert/umgesetzt.
*/

#include <iostream>   // f�r cout
#include <string>     // f�r strlen, strcpy

// Binde die Deklaration/Beschreibung der Klasse ein
#include "auto.h"


using namespace std;

/*************************************************************************
   Diese Methode der Klasse Auto setzt die Leistung des 
   Autos auf die �bergebene Leistung 
   Parameter:
                leistung - die zu setzende Leistung
*/
void Auto::setLeistung(int leistung)
{       // setze die Leistung dieses Autos (dessen Methode 
        // setLeistung() gerade aufgerufen wird) auf die �bergebene Leistung
        this->leistung = leistung;
}

/*************************************************************************
   Diese Methode der Klasse Auto setzt die Leistung des 
   Autos auf die �bergebene Leistung
   Return:
                die Leistung diese Autos in KW
*/
int Auto::getLeistung()
{       
        return leistung;
}

/*************************************************************************
   Eine Methode, mit der man das Fabrikat des Autos setzen kann 
   Parameter:
                einFabrikat - die neue Fabrikatsbezeichnung
*/
////
/*
void Auto::setFabrikat(char* einFabrikat)
{
        // nur wenn wirklich ein Name �bergeben wurde
        if (einFabrikat != 0)
        {
                // falls ein altes Fabrikat existierte, l�sche dieses
                if (fabrikat != 0)
                        delete fabrikat; // gib Speicher frei
                // stelle Speicher f�r das neue Fabrikat bereit (+ 1 f�r Nullbyte '\0')
                fabrikat = new char [ strlen(einFabrikat) + 1];
                if (fabrikat != 0)
                        // Speicher noch ausreichend gewesen, deshalb
                        // kopiere das Fabrikat, damit man von au�erhalb diese Eigenschaft 
                        // nicht �ber den Pointer modifizieren kann
                        strcpy(fabrikat, einFabrikat);
        }
}

/*************************************************************************
   Eine Methode, mit der man das Fabrikat des Autos auslesen kann. 
   Return:
                das Fabrikat des Autos oder 0 (NULL), falls keines gesetzt
                (genauer: einen Pointer auf die intern gespeicherte (gekapselte) Eigenschaft!?) */
char* Auto::getFabrikat()
{
        return fabrikat;
}
*/
////
/*************************************************************************
   Konstruktor zum Erzeugen eines Autos. Es werden dabei die Leistung des Autos
   standardm��ig auf 74KW und das Fabrikat auf 0 (NULL) gesetzt.
*/
Auto::Auto()
{       
        leistung=74;
        ////fabrikat=0; // hei�t hier: nichts angegeben
}

/*************************************************************************
   Der Destruktor eines Autos wird automatisch aufgerufen, wenn das Programm 
   beendet wird. 
*/
Auto::~Auto()
{       // zu Testzwecken: mache hier eine Ausgabe, damit man den Aufruf sehen kann
        cout << "Destructor!!!" << endl;

        // gib angefoderte Ressourcen frei
        ////delete fabrikat;
        // zur Sicherheit: Setze das Attribut auf 0 (NULL), damit eventuell mehrfacher Aufruf des 
        // Destruktors nicht zum Laufzeitfehler wird
        ////fabrikat = 0; 
} 
