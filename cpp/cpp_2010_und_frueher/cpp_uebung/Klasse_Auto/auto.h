/* Datei auto.h
  In dieser Datei befindet sich die Beschreibung/Deklaration der Klasse.
*/

/* Damit eine mehrfache Deklaration der Klasse durch mehrfaches Einbinden 
   dieser Headerdatei verhindert wird, wird �ber die Pr�prozessor-Anweisung
   "#define AUTO_H" ein Wert gesetzt, den man mittels "ifdef" bzw.
   "ifndef" abpr�fen kann. Dieser gibt Auskunft, ob diese Headerdatei bereits 
   eingebunden wurde.
*/
#ifndef AUTO_H
#define AUTO_H

/*
   Hier folgt die Beschreibung der Klasse Auto
*/
class Auto
{
        // privater Teil der Klasse (darauf hat nur die Klasse Zugriff)
        private:
                /* Eine Eigenschaft/ein Attribut einer Realisierung/Instanz von Auto,
                   welche die Leistung in KW beschreibt.
                 */
                int leistung;
                /* Eine weitere Eigenschaft, die das Fabrikat darstellt.
                 */
                ////char* fabrikat;

        // �ffentlicher Teil der Klasse (alles darunter angegebene kann von 
        // jedem verwendet werden)
        public:
                /* Ein Konstruktor zum Generieren eines Auto-Objektes */
                Auto();
                /* Ein Destruktor zum Zerst�ren eines Auto-Objektes */
                ~Auto();                        

                /* Eine Methode, mit der man die Leistung des Motors setzen kann */
                void setLeistung(int leistung);
                /* Eine Methode, mit der man die Leistung des Motors auslesen kann */
                int getLeistung();

                /* Eine Methode, mit der man das Fabrikat des Autos setzen kann */
                ////void setFabrikat(char* einFabrikat);
                /* Eine Methode, mit der man das Fabrikat des Autos auslesen kann */
                ////char* getFabrikat();
}; // Dieses Semikolon darf nicht fehlen!!!

#endif