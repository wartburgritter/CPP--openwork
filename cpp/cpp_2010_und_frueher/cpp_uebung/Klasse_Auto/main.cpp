/* Datei main.cpp
   In dieser Datei wird eine Beispielanwendung der Klasse Auto implementiert.
 */

// binde die Beschreibung von Auto ein
#include "auto.h"
#include "raumfahrzeug.h"
#include <iostream>
using namespace std;

int main(void)
{
        // Lege ein Auto mit dem Standard-Konstruktor an.
        Auto a;
        ////char* s;
	Raumfahrzeug ufo;

        cout << "Hier wird ein Auto getestet!" << endl;

        // teste die Methoden der Instanz a von Auto
        cout << "Leistung war: " << a.getLeistung() << endl;
        ////s = a.getFabrikat();
        ////cout << "Fabrikat ist: " << (s==0 ? "<nicht gesetzt>" : s)<< endl;

        cout << "--------------------------------" << endl;

/*
	// Leistung jetzt 10KW
        a.setLeistung(10); 
        // Fabrikat jetzt auf Volkswagen �ndern
        a.setFabrikat("Volkswagen"); 
        // ... und gleich nocheinmal auf VW setzen - Was steht jetzt in Auto?
        a.setFabrikat("VW"); 

        cout << "Leistung ist: " << a.getLeistung() << endl; // gibt den Wert 10 aus
        cout << "Fabrikat ist: " << a.getFabrikat() << endl; // gibt den Wert VW aus
*/
        cout << endl << "...beende main...." << endl;

        // R�ckgabe 0 an das Betriebssystem soll hei�en, erfolgreich beendet
        return 0;
}