// Datei Datum.hh
//
// Datumsklasse zur Illustration von Konstruktoren und Destruktoren
// (C) 2000, T. Wieland.

#ifndef _DATUM_H_
#define _DATUM_H_

class Datum
{
private:
  unsigned int t, m, j;

public:
  Datum();                             //Standardkonstruktor
  ~Datum();                            //Destruktor
  Datum(unsigned int _t,               //Allgemeiner Konstruktor
    unsigned int _m,
    unsigned int _j);
  Datum(unsigned int _t,               //Allgemeiner Konstruktor
    unsigned int _m);
  Datum(unsigned int _t);              //Allgemeiner Konstruktor
  Datum(const Datum& _datum);          //Kopierkonstruktor
  //Datum(const string& _datumstring);   //Typumwandlungskonstruktor
  //Ich weiss nicht wie ich den implementieren soll

  void setze(unsigned int _t,
    unsigned int _m,
    unsigned int _j);
  void setzeAufHeute();
  void ausgeben() const;
  bool istSchaltjahr();
};

#endif // _DATUM_H_
