#ifndef ACCOUNT_H
#define ACCOUNT_H

class Account
{
public:
  Account(double b);
  void einzahlen(double amt);
  void auszahlen(double amt);
  double getBalance() const;
  void berechneZins();
  void berechneVariablenZins(double amt);
  void falscheEingabe();  // ist eigentlich keine Klassenmethode account
private:
  double balance;
};

#endif