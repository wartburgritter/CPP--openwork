/****************************************************************
**
** Qt tutorial 2
**
****************************************************************/

#include <qapplication.h>
#include <qpushbutton.h>
#include <qfont.h>


int main( int argc, char **argv )
{
    QApplication a( argc, argv );

    QPushButton quit( "Quit", 0 );
    quit.resize( 75, 30 );
    quit.setFont( QFont( "Times", 18, QFont::Bold ) );

   QObject::connect( &quit, SIGNAL(clicked()), &a, SLOT(quit()) );
//   QObject::connect( &quit, SIGNAL(pressed()), &a, SLOT(quit()) );
//    QObject::connect( &quit, SIGNAL(released()), &a, SLOT(quit()) );

    a.setMainWidget( &quit );
    quit.show();
    return a.exec();
}


/*
qmake-qt3 -project
qmake-qt3
make
./tutorial_02
*/
