#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFont>
#include <QtGui/QVBoxLayout>


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QWidget window;
  QVBoxLayout layout(&window);

  QLabel label("Hello World!", &window);
  label.setAlignment(Qt::AlignCenter);
  label.setFont(QFont("Arial", 20, QFont::Bold));
  layout.addWidget(&label);

  QPushButton button("Close", &window);
  QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));
  layout.addWidget(&button);

  window.show();

  return app.exec();
}


/*
qmake-qt4 -project
qmake-qt4
make
*/

