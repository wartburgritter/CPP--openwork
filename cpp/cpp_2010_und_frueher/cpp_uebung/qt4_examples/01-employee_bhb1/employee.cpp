#include "employee.h"

// Der Konstruktor generiert eine Instanz mit der Attribut Gehalt = 0
Employee::Employee(QObject *parent)
 : QObject(parent)
{
  m_salary = 0;
}

int Employee::getsalary() const
{
  return m_salary;
}

void Employee::setSalary(int salary)
{
  if (m_salary != salary) {
    m_salary = salary;
    emit salaryChanged(m_salary);
  }
}

