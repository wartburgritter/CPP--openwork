#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <QtCore/QObject>

// Die Klasse Employee definiert einen Mitarbeiter (Instanzen der Klasse) der ein Gehalt (Attribute der Klasse) bekommt.
// Die Klasse QObject wir um die Klasse Employee erweitert. Die Klasse Employee erbt alle Methoden der Klasse QObject.
class Employee : public QObject
{
   Q_OBJECT
  public:
     Employee(QObject *parent = 0);    
     // Der Konstruktor nimmt einen Parameter an, der einen Zeiger auf das Elternobjekt darstellt.
     int getsalary() const;

  public slots:
    void setSalary(int salary);

  signals:
    void salaryChanged(int salary);

  private:
    int m_salary;
};

#endif // EMPLOYEE_H

