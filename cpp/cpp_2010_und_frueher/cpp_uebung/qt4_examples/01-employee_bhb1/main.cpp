#include <stdio.h>
#include <QtCore/QTextStream>
#include <stdio.h>
#include <QtCore/QTextStream>
#include "employee.h"

static QTextStream qout(stdout);

int main()
{
  Employee emp1;
  Employee emp2;
  //Folgende Methoe connect ist in der Klasse QObject implementiert.
  //Die Methode erstellt eine Verbindung zwischen einem signal und einem Slot zweier QObjects
  //Die Makros SIGNAL und SLOT nehmen als Argument die Deklaration einer Mehtode an und geben
  //speziellen String zurueck (vom Typ const char*), der durch QObject als Identifikator dieser
  //Methode verwendet wird. Wichtig ist, dass Deklaration der Methoden unnerhalb von diesen
  //Makros keine Parameternamen beinhalten. 
  // aus Doku QObject:
  //bool QObject::connect(QObject *sender, const char *signal,
  //      QObject *receiver, const char *slot);
  QObject::connect(&emp1, SIGNAL(salaryChanged(int)),
         &emp2, SLOT(setSalary(int)));
  QObject::connect(&emp2, SIGNAL(salaryChanged(int)),
         &emp1, SLOT(setSalary(int)));

  Employee emp3;
  Employee emp4;
  QObject::connect(&emp3, SIGNAL(salaryChanged(int)),
         &emp4, SLOT(setSalary(int)));
  QObject::connect(&emp4, SIGNAL(salaryChanged(int)),
         &emp3, SLOT(setSalary(int)));

  emp1.setSalary(100);
  emp3.setSalary(200);

  qout << "emp1: " << emp1.getsalary() << '\n';
  qout << "emp2: " << emp2.getsalary() << '\n';
  qout << "emp3: " << emp3.getsalary() << '\n';
  qout << "emp4: " << emp4.getsalary() << '\n'<< '\n';

  
  emp2.setSalary(1000);
  emp4.setSalary(2000);

  qout << "emp1: " << emp1.getsalary() << '\n';
  qout << "emp2: " << emp2.getsalary() << '\n';
  qout << "emp3: " << emp3.getsalary() << '\n';
  qout << "emp4: " << emp4.getsalary() << '\n';


  // Versuch disconnect, dass salery von emp2 sich nicht mehr aendert, 
  //aber funktioniert irgendwie nicht.
  /*
  QObject::disconnect(SIGNAL(salaryChanged(int)), &emp2, SLOT(setSalary(int)));
  QObject::disconnect(SIGNAL(salaryChanged(int)), &emp1, SLOT(setSalary(int)));
 
  emp2.setSalary(10000);
  emp4.setSalary(20000);

  qout << "emp1: " << emp1.getsalary() << '\n';
  qout << "emp2: " << emp2.getsalary() << '\n';
  qout << "emp3: " << emp3.getsalary() << '\n';
  qout << "emp4: " << emp4.getsalary() << '\n';
  */
    
  return 0;
}

/*
qmake-qt4
make
*/