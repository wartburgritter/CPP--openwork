/****************************************************************************
** Meta object code from reading C++ file 'calculator.h'
**
** Created: Sun Aug 22 23:21:05 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "calculator.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calculator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Calculator[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      27,   11,   11,   11, 0x08,
      50,   11,   11,   11, 0x08,
      65,   11,   11,   11, 0x08,
      85,   11,   11,   11, 0x08,
     104,   11,   11,   11, 0x08,
     115,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Calculator[] = {
    "Calculator\0\0digitClicked()\0"
    "unaryOperatorClicked()\0pointClicked()\0"
    "changeSignClicked()\0backspaceClicked()\0"
    "clearAll()\0abbruchAktion()\0"
};

const QMetaObject Calculator::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Calculator,
      qt_meta_data_Calculator, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Calculator::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Calculator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Calculator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Calculator))
        return static_cast<void*>(const_cast< Calculator*>(this));
    return QDialog::qt_metacast(_clname);
}

int Calculator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: digitClicked(); break;
        case 1: unaryOperatorClicked(); break;
        case 2: pointClicked(); break;
        case 3: changeSignClicked(); break;
        case 4: backspaceClicked(); break;
        case 5: clearAll(); break;
        case 6: abbruchAktion(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
