 #include <QtGui>

 #include <math.h>

 #include "button.h"
 #include "calculator.h"

 Calculator::Calculator(QWidget *parent)
     : QDialog(parent)
 {
     waitingForOperand = true;

     ////////////////////widget elemente erstellen///////////////////
     
     label1 = new QLabel("hallo label");
     
     display = new QLineEdit("");
     //display->setReadOnly(true);
     display->setAlignment(Qt::AlignRight);
     display->setMaxLength(15);

     QFont font = display->font();
     font.setPointSize(font.pointSize() + 8);
     display->setFont(font);

     
     display2 = new QLineEdit("0");
     display2->setReadOnly(true);
     display2->setAlignment(Qt::AlignRight);
     display2->setMaxLength(15);

     //QFont font = display2->font();
     //font.setPointSize(font.pointSize() + 12);
     display2->setFont(font);
     
     
     //die neun zahlenbuttons
     for (int i = 0; i < NumDigitButtons; ++i) {
         digitButtons[i] = createButton(QString::number(i), SLOT(digitClicked()));
     }

     Button *pointButton = createButton(tr("."), SLOT(pointClicked()));
     Button *changeSignButton = createButton(tr("\261"), SLOT(changeSignClicked()));

     Button *backspaceButton = createButton(tr("Backspace"), SLOT(backspaceClicked()));
     Button *abbruchButton = createButton(tr("Abbruch"), SLOT(abbruchAktion()));
     Button *clearAllButton = createButton(tr("Clear All"), SLOT(clearAll()));

     Button *squareRootButton = createButton(tr("Sqrt"), SLOT(unaryOperatorClicked()));
     Button *powerButton = createButton(tr("x\262"), SLOT(unaryOperatorClicked()));
     Button *reciprocalButton = createButton(tr("1/x"), SLOT(unaryOperatorClicked()));
     Button *asminButton = createButton(tr("asmin"), SLOT(unaryOperatorClicked()));
     
     Button *pseudo1Button = createButton(tr("5 0"), SLOT(clearAll()));
     Button *pseudo2Button = createButton(tr("5 0"), SLOT(clearAll()));


     ////////////widgetelemente anordnern und darstellen///////////////////////
     QGridLayout *mainLayout = new QGridLayout;
     mainLayout->setSizeConstraint(QLayout::SetFixedSize);

     //links oben ist 0,0
     //mainLayout->addWidget(buttonName, zeile, spalte)
     //oder ueberladen
     //mainLayout->addWidget(buttonName, vonzeile, vonspalte, ueberzeilen, ueberspalten)
     
     //zeile0
     mainLayout->addWidget(display, 0, 0, 1, 6);
     mainLayout->addWidget(display2, 7, 0, 1, 6);
     mainLayout->addWidget(label1, 8, 0);
     
     //zeile1
     mainLayout->addWidget(backspaceButton, 1, 0, 1, 2);   
     mainLayout->addWidget(abbruchButton, 1, 2, 1, 2);
     mainLayout->addWidget(clearAllButton, 1, 4, 1, 2);


     for (int i = 1; i < NumDigitButtons; ++i) {
         int row = ((9 - i) / 3) + 2;
         int column = ((i - 1) % 3) + 1;
         mainLayout->addWidget(digitButtons[i], row, column);
     }

     //zeile5
     mainLayout->addWidget(digitButtons[0], 5, 1);
     mainLayout->addWidget(pointButton, 5, 2);
     mainLayout->addWidget(changeSignButton, 5, 3);
     mainLayout->addWidget(asminButton, 5, 4);


     mainLayout->addWidget(squareRootButton, 2, 5);
     mainLayout->addWidget(powerButton, 3, 5);
     mainLayout->addWidget(reciprocalButton, 4, 5);
     
     //mainLayout->addWidget(abbruchButton, 5, 5);
     mainLayout->addWidget(pseudo1Button, 6, 0);
     mainLayout->addWidget(pseudo2Button, 6, 5);
     
     setLayout(mainLayout);

     setWindowTitle(tr("Hugos Rechner"));
 }


//////////////funktionen der widgetelemente (der einzelnen buttons) ////////////////
 void Calculator::digitClicked()
 {
     Button *clickedButton = qobject_cast<Button *>(sender());
     int digitValue = clickedButton->text().toInt();
     if (display->text() == "0" && digitValue == 0.0)
         return;

     if (waitingForOperand) {
         display->clear();
         waitingForOperand = false;
     }
     display->setText(display->text() + QString::number(digitValue));
 }

 void Calculator::unaryOperatorClicked()
 {
     Button *clickedButton = qobject_cast<Button *>(sender());
     QString clickedOperator = clickedButton->text();
     double operand = display->text().toDouble();
     double result = 0.0;

     if (clickedOperator == tr("Sqrt")) {
         if (operand < 0.0) {
             abortOperation();
             return;
         }
         result = sqrt(operand);
     } else if (clickedOperator == tr("x\262")) {
         result = pow(operand, 2.0);
     } else if (clickedOperator == tr("1/x")) {
         if (operand == 0.0) {
             abortOperation();
             return;
         }
         result = 1.0 / operand;
     } else if (clickedOperator == tr("asmin")) {
         if (operand < 0.0) {
             abortOperation();
             return;
         }
         result = (1.0/(1+0.5*operand*0.01/3)) * 0.26 * (100 * pow(operand, 2.0)/6) * (1/(0.95*operand*43.5)) ;
     }
     display->setText(QString::number(result));
     waitingForOperand = true;
 }


 void Calculator::pointClicked()
 {
     if (waitingForOperand)
         display->setText("0");
     if (!display->text().contains("."))
         display->setText(display->text() + tr("."));
     waitingForOperand = false;
 }

 void Calculator::changeSignClicked()
 {
     QString text = display->text();
     double value = text.toDouble();

     if (value > 0.0) {
         text.prepend(tr("-"));
     } else if (value < 0.0) {
         text.remove(0, 1);
     }
     display->setText(text);
 }

 void Calculator::backspaceClicked()
 {
     if (waitingForOperand)
         return;

     QString text = display->text();
     text.chop(1);
     if (text.isEmpty()) {
         text = "0";
         waitingForOperand = true;
     }
     display->setText(text);
 }


 void Calculator::clearAll()
 {
     display->setText("0");
     waitingForOperand = true;
 }

//hugos erster slot
void Calculator::abbruchAktion()
 {
     display->setText("Abbruch");
     waitingForOperand = true;
 }
 
 Button *Calculator::createButton(const QString &text, const char *member)
 {
     Button *button = new Button(text);
     connect(button, SIGNAL(clicked()), this, member);
     return button;
 }
 
 void Calculator::abortOperation()
 {
     clearAll();
     display->setText(tr("Nicht durch 0!!!"));
 }
