#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFont>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QWidget window;
  window.resize(200, 100);

  QLabel label("Hello World!", &window);
  label.setGeometry(0, 0, 200, 50);
  label.setAlignment(Qt::AlignCenter);
  label.setFont(QFont("Arial", 20, QFont::Bold));
  QPushButton button("Close", &window);
  button.setGeometry(10, 60, 180, 30);
  QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));

  window.show();

  return app.exec();
}


/*
qmake-qt4 -project
qmake-qt4
make
*/

