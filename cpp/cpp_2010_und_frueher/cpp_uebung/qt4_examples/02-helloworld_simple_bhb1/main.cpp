#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFont>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  
  QWidget window;                        // Instanz der Klasse QWidget mit Name window
  window.resize(300, 200);                   // breite, hoehe

  
  QLabel label("Hello Hugo!", &window);
  label.setGeometry(0, 0, 300, 50);        //(x,y,b,h)    // breite wie window, hoehe die haelfte von window
  label.setAlignment(Qt::AlignCenter);
  label.setFont(QFont("Arial", 20, QFont::Light));
 
  QPushButton button("Close", &window);
  button.setGeometry(25, 75, 250, 100);     // button von 25,75 bis 275,175
  button.setFont(QFont("Arial", 30, QFont::Black));
  
  // siehe connect-funktion in employee, genauso, nur dass die funktionen clicked und close in QWidget schon existieren
  QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));

  
  window.show();

  
  return app.exec();
}


// Befehle zum kompelieren
// Ablauf:
// "qmake-qt4" ausfuehren --> makefile fuer die aktuelle Plattform wird erstellt
// Falls keine .pro Datei vorhanden ist muss erst "qmake-qt4 -project" ausgefuehrt werden -->
// "make" ausfuehren um dateien zu kompelieren
/*
qmake-qt4 -project
qmake-qt4
make
*/

