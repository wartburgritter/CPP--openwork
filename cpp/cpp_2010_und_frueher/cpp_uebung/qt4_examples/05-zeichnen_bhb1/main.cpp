#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QMouseEvent>
#include <QtGui/QFrame>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>
#include <QtGui/QLabel>


class BoardWidget : public QFrame
{
  Q_OBJECT
public:
  BoardWidget(QWidget *parent = 0);

public slots:
  void clear();
  void red();
  void blue();
  void green();
  void yellow();
  void magenta();
   
protected:
  void mouseMoveEvent(QMouseEvent *event);
  void resizeEvent(QResizeEvent *event);
  void paintEvent(QPaintEvent *event);

private:
  QPixmap m_pixmap;
};

// Konstruktor
BoardWidget::BoardWidget(QWidget *parent)
  : QFrame(parent)
{
  setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
}

// pixmap leeren
void BoardWidget::clear()
{
  m_pixmap.fill(Qt::white);
  update();
}

void BoardWidget::red()
{
  m_pixmap.fill(Qt::red);
  update();
}

void BoardWidget::blue()
{
  m_pixmap.fill(Qt::blue);
  update();
}

void BoardWidget::green()
{
  m_pixmap.fill(Qt::green);
  update();
}

void BoardWidget::yellow()
{
  m_pixmap.fill(Qt::yellow);
  update();
}

void BoardWidget::magenta()
{
  m_pixmap.fill(Qt::magenta);
  update();
}

// zeichenevent
void BoardWidget::mouseMoveEvent(QMouseEvent *event)
{
  event->accept();
  QPoint pos = event->pos();
  QPainter painter(&m_pixmap);
  //  painter.fillRect(pos.x() - 2, pos.y() - 2, 4, 4, Qt::black);  //orginal ort entfernt von mauszeiger und fuellquadrate = linienstaerke
  //kreise zeichnen
  painter.drawArc(pos.x() - 5, pos.y() - 5, 20, 20, 0*16, 360*16 );
  update();
}

// trotz resize bleibt pixmap erhalten 
// woher weiss die funktion das sie aufgerufen werden muss 
// Die Urpixmap bei programmstart ist auch cyan, Warum?
void BoardWidget::resizeEvent(QResizeEvent *event)
{
  event->accept();
  QPixmap new_pixmap(event->size());  // ist das der funktionsaufruf?
  new_pixmap.fill(Qt::cyan);
  QPainter painter(&new_pixmap);
  painter.drawPixmap(QPoint(0, 0), m_pixmap);
  m_pixmap = new_pixmap;
}

// zeichnet fertige pixmap auf sich selbst
// wird durch das update() in mouseMoveEvent aufgerufen
void BoardWidget::paintEvent(QPaintEvent *event)
{
  event->accept();
  QPainter painter(this);
  painter.drawPixmap(QPoint(0, 0), m_pixmap);
  QFrame::paintEvent(event);
}


int main(int argc, char *argv[])
{
  // Im vertikalen layout verden das zeichenboar und ein horizontales layout angeordnet
  // Im horizontalen layout werden die Button angeordnet.
  // Der Rest ist einfach. Die Tricks stecken in der Klasse BoardWidget
  
  QApplication myApp(argc, argv);

  QWidget myWindow;
    myWindow.move(850, 125);
    myWindow.resize(500, 350);
    myWindow.setWindowTitle("Hugos Painter");
    QVBoxLayout myTop_layout(&myWindow);
      myTop_layout.setMargin(20);

      BoardWidget myBoard(&myWindow);
      myTop_layout.addWidget(&myBoard);
    
      QHBoxLayout myColourButton_layout;
       myTop_layout.addLayout(&myColourButton_layout);
  
        QPushButton myRed_button("red",&myWindow);
        QObject::connect(&myRed_button, SIGNAL(clicked()), &myBoard, SLOT(red()));
        myColourButton_layout.addWidget(&myRed_button);
        
        QPushButton myBlue_button("blue",&myWindow);
        QObject::connect(&myBlue_button, SIGNAL(clicked()), &myBoard, SLOT(blue()));
        myColourButton_layout.addWidget(&myBlue_button);
 
        QPushButton myGreen_button("green",&myWindow);
        QObject::connect(&myGreen_button, SIGNAL(clicked()), &myBoard, SLOT(green()));
        myColourButton_layout.addWidget(&myGreen_button);

        QPushButton myYellow_button("yellow",&myWindow);
        QObject::connect(&myYellow_button, SIGNAL(clicked()), &myBoard, SLOT(yellow()));
        myColourButton_layout.addWidget(&myYellow_button);
	
        QPushButton myMagenta_button("magenta",&myWindow);
        QObject::connect(&myMagenta_button, SIGNAL(clicked()), &myBoard, SLOT(magenta()));
        myColourButton_layout.addWidget(&myMagenta_button);

      QHBoxLayout myButton_layout;
      myTop_layout.addLayout(&myButton_layout);

        QLabel myLabel("Viel Spass beim malen!", &myWindow);
        myButton_layout.addWidget(&myLabel);
  
        myButton_layout.addStretch();

        QPushButton myClear_button("Leeren",&myWindow);
        QObject::connect(&myClear_button, SIGNAL(clicked()), &myBoard, SLOT(clear()));
        myButton_layout.addWidget(&myClear_button);

        QPushButton myClose_button("Schliessen", &myWindow);
        QObject::connect(&myClose_button, SIGNAL(clicked()), &myWindow, SLOT(close()));
        myButton_layout.addWidget(&myClose_button);

  
    myWindow.show();

  return myApp.exec();
}

#include "main.moc"


/*
qmake-qt4 -project
qmake-qt4
make
*/

