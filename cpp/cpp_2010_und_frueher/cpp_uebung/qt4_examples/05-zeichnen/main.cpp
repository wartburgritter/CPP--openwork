#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QMouseEvent>
#include <QtGui/QFrame>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>


class BoardWidget : public QFrame
{
  Q_OBJECT
public:
  BoardWidget(QWidget *parent = 0);

public slots:
  void clear();

protected:
  void mouseMoveEvent(QMouseEvent *event);
  void resizeEvent(QResizeEvent *event);
  void paintEvent(QPaintEvent *event);

private:
  QPixmap m_pixmap;
};

BoardWidget::BoardWidget(QWidget *parent)
  : QFrame(parent)
{
  setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
}

void BoardWidget::clear()
{
  m_pixmap.fill(Qt::white);
  update();
}

void BoardWidget::mouseMoveEvent(QMouseEvent *event)
{
  event->accept();
  QPoint pos = event->pos();
  QPainter painter(&m_pixmap);
  painter.fillRect(pos.x() - 1, pos.y() - 1, 2, 2, Qt::black);
  update();
}

void BoardWidget::resizeEvent(QResizeEvent *event)
{
  event->accept();
  QPixmap new_pixmap(event->size());
  new_pixmap.fill(Qt::white);
  QPainter painter(&new_pixmap);
  painter.drawPixmap(QPoint(0, 0), m_pixmap);
  m_pixmap = new_pixmap;
}

void BoardWidget::paintEvent(QPaintEvent *event)
{
  event->accept();
  QPainter painter(this);
  painter.drawPixmap(QPoint(0, 0), m_pixmap);
  QFrame::paintEvent(event);
}


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QWidget window;
  window.resize(500, 350);
  window.setWindowTitle("Painter");
  QVBoxLayout top_layout(&window);
  top_layout.setMargin(3);

  BoardWidget board(&window);
  top_layout.addWidget(&board);

  QHBoxLayout button_layout;
  top_layout.addLayout(&button_layout);

  button_layout.addStretch();

  QPushButton clear_button("Clear",&window);
  QObject::connect(&clear_button, SIGNAL(clicked()), &board, SLOT(clear()));
  button_layout.addWidget(&clear_button);

  QPushButton close_button("Close", &window);
  QObject::connect(&close_button, SIGNAL(clicked()), &window,
      SLOT(close()));
  button_layout.addWidget(&close_button);

  window.show();

  return app.exec();
}

#include "main.moc"


/*
qmake-qt4 -project
qmake-qt4
make
*/

