#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFont>
#include <QtGui/QVBoxLayout>


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QWidget window;
//  window.resize(200, 100);
  window.resize(200, 120);
  QVBoxLayout layout(&window);

  QLabel label("Hello World!", &window);
  label.setAlignment(Qt::AlignCenter);
  label.setFont(QFont("Arial", 20, QFont::Bold));
  layout.addWidget(&label);

  QPushButton button("Close", &window);
  button.setFont(QFont("Arial", 20));
//  button.setGeometry(10, 60, 180, 30);
  QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));
  layout.addWidget(&button);

  window.show();

  return app.exec();
}


/*
qmake-qt4 -project
qmake-qt4
make
*/

