#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QMouseEvent>
#include <QtGui/QPalette>
#include <QtGui/QVBoxLayout>
#include <math.h>

class HelloWorldLabel : public QLabel
{
  Q_OBJECT
public:
  HelloWorldLabel(const QString &text, QWidget *parent =0);
protected:
  virtual void mousePressEvent(QMouseEvent *event);
};

HelloWorldLabel::HelloWorldLabel(const QString &text, QWidget *parent)
  :QLabel(text, parent)
{}

void HelloWorldLabel::mousePressEvent(QMouseEvent *event)
{
  event->accept();
  QPalette palette = this->palette();
  QPoint pos = event->pos();
  int r = 255 * pos.x() / width();
  int g = 255 * pos.y() / height();
  int b = 100;
  palette.setColor(QPalette::Foreground, QColor(r,g,b));
  setPalette(palette);
  update();
}


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QWidget window;
  QVBoxLayout layout(&window);

  HelloWorldLabel label("Hello World!", &window);
  label.setAlignment(Qt::AlignCenter);
  label.setFont(QFont("Arial", 30));
  layout.addWidget(&label);

  QPushButton button("Close", &window);
  button.setFont(QFont("Arial", 20, QFont::Bold));
  QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));
  layout.addWidget(&button);

  window.show();

  return app.exec();
}

#include "main.moc"


/*
qmake-qt4 -project
qmake-qt4
make
*/

