#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <QtCore/QObject>

class Employee : public QObject
{
   Q_OBJECT
  public:
     Employee(QObject *parent = 0);
     int salary() const;

  public slots:
    void setSalery(int salary);

  signals:
    void salaryChanged(int salary);

  private:
    int m_salary;
};

#endif // EMPLOYEE_H

