/****************************************************************************
** Bmi meta object code from reading C++ file 'bmi.h'
**
** Created: Sun May 19 18:34:45 2002
**      by: The Qt MOC ($Id:  qt/moc_yacc.cpp   3.0.3   edited Mar 18 10:45 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "bmi.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 19)
#error "This file was generated using the moc from 3.0.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *Bmi::className() const
{
    return "Bmi";
}

QMetaObject *Bmi::metaObj = 0;
static QMetaObjectCleanUp cleanUp_Bmi;

#ifndef QT_NO_TRANSLATION
QString Bmi::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Bmi", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString Bmi::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Bmi", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* Bmi::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = BmiMainForm::staticMetaObject();
    metaObj = QMetaObject::new_metaobject(
	"Bmi", parentObject,
	0, 0,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_Bmi.setMetaObject( metaObj );
    return metaObj;
}

void* Bmi::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "Bmi" ) ) return (Bmi*)this;
    return BmiMainForm::qt_cast( clname );
}

bool Bmi::qt_invoke( int _id, QUObject* _o )
{
    return BmiMainForm::qt_invoke(_id,_o);
}

bool Bmi::qt_emit( int _id, QUObject* _o )
{
    return BmiMainForm::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool Bmi::qt_property( int _id, int _f, QVariant* _v)
{
    return BmiMainForm::qt_property( _id, _f, _v);
}
#endif // QT_NO_PROPERTIES
