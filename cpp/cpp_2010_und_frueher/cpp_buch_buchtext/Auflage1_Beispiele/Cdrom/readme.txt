             
            Thomas Wieland 
            C++-Entwicklung mit Linux 

            Eine Einf�hrung in die Sprache und die wichtigsten Werkzeuge - von 
            GCC und XEmacs bis SNiFF+ 

            Diese CD-ROM enth�lt alle Beispiele, die im Buch besprochen werden, 
            die Open Source-Werkzeuge, die verwendet werden, sowie die 
            Penguin-Edition von SNiFF+, einer freien SNiFF+-Variante f�r C++ 
            unter Linux. Diese Informationen finden Sie in folgenden 
            Unterverzeichnissen: 

              source, das die Beispieldateien getrennt nach Kapiteln umfasst. 
              Alle Beispiele lassen sich entweder direkt zu einer ausf�hrbaren 
              Datei �bersetzen oder �ber das beiliegende Makefile. Einige 
              Beispiele enthalten zus�tzlich Projekt-Dateien f�r die Verwendung 
              mit SNiFF+.    

              software, in dem Sie alle Software finden. F�r jedes Softwarepaket 
              liegen je nach Verf�gbarkeit das zur Zeit der Produktion 
              aktuellste Quelltext-Paket sowie RPMs f�r RedHat- und 
              SuSE-Installationen bei. 

            Die Verwendung der CD-ROM erfolgt unter Ausschluss jeglicher Haftung 
            und Garantie. Insbesondere schliessen wir jegliche Haftung fuer 
            Schaeden aus, die auf Grund der Benutzung der auf der CD-ROM 
            enthaltenen Programme entstehen. 
            Die auf dieser CD-ROM enthaltenen Beispieldateien sind freie 
            Software. Sie k�nnen sie weitergeben und ver�ndern unter den 
            Bedingungen der GNU General Public License, wie sie von der Free 
            Software Foundation ver�ffentlicht wurde. Eine Kopie der 
            Lizenzbestimmungen finden Sie im gleichen Verzeichnis wie diese 
            Datei. 
             
            Autor: 
            Thomas Wieland studierte Mathematik, Physik und Informatik an der 
            Universit�t Bayreuth und promovierte dort 1996. Danach arbeitete er 
            als Softwareingenieur beim Deutschen Zentrum f�r Luft- und Raumfahrt 
            (DLR) in Oberpfaffenhofen. Heute ist er bei der Siemens AG in 
            M�nchen als Softwarearchitekt f�r verteilte Anwendungen t�tig. 
            Au�erdem ist er Chefredakteur der Zeitschrift "Linux 
            Enterprise".
