#include <iostream>
#include <cstdlib>
#include <string>

// Systembibliothek
#include <unistd.h>

class Configuration
{
public:
  string text;
  unsigned short line;
  bool space;

  Configuration() :
    line(0), space(false)
    {}
};

int main(int argc, char* argv[])
{
  int opt=0;
  Configuration config;

  while((opt = getopt(argc, argv, "d:l:n:t:s")) 
	!= -1)
  {
    switch(opt)
    {
      case 'd': 
	cout << "Argument d gegeben!" << endl;
              
      case 't': 
	cout << "Argument t gegeben!" << endl;
	config.text = optarg;
	break;

      case 's': 
	cout << "Argument s gegeben!" << endl;
	config.space = true;
	break;

      case 'l': 
      case 'n':
        config.line = atoi(optarg);
        break;

      default: 	
        cout << "Unbekannte Option " 
	     << (char)optopt << endl;
        break;
    }
  }

  cout << "---------------" << endl;
  
  if (config.line)
    cout << config.line << ": ";

  cout << config.text << endl;

  if (config.space)
    cout << endl;

  cout << "----------------" << endl;
  
  return 0;
}

