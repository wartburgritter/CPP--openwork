#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  if (argc<3)
  {
    cerr << "Bitte zwei Dateinamen als Argumente angeben!" << endl;
    return 1;
  }

  ifstream in(argv[1]);
  if (!in)
  {
    cerr << "Datei " << argv[1] << " kann nicht ge�ffnet werden!" << endl;
    return 2;
  }

  ofstream out(argv[2]);
  if (!out)
  {
    cerr << "Datei " << argv[2] << " kann nicht ge�ffnet werden!" << endl;
    return 3;
  }

  char c;

  while(in.get(c)) out.put(c);

  return 0;
}


