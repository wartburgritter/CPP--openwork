#include <cstdlib>
#include <ctime>
#include "date.h"


//-----------------------------------------------------------------------------
// class Date: Methoden
//-----------------------------------------------------------------------------
// Klasse : Date
// Methode: Standardkonstruktor
// Beschr.: Als Ausgangsdatum wird das aktuelle Systemdatum benutzt
//-----------------------------------------------------------------------------
Date::Date()
{    
  time_t jetzt = time(NULL);          // Datum und Uhrzeit des Systems
  tm*    z     = localtime(&jetzt);   // Konvertierung in 'lesbare' Form

  year  = z->tm_year + 1900;          // Jahr ab 1900
  month = z->tm_mon + 1;              // localtime() liefert 0..11
  day   = z->tm_mday;
}


//-----------------------------------------------------------------------------
// Klasse   : Date
// Methode  : inc (1 Tag weiterzaehlen)
// Rueckgabe: Objekt selbst
//-----------------------------------------------------------------------------
Date Date::inc()
{    
  do
  {    
    day++;
    if(day > 31)  
    { 
      day = 1; 
      month++; 
    }
          
    if(month > 12)    
    { 
      month = 1; 
      year++; 
    }
  } while( !correctDate(*this) );
  
  return *this;
}

//-----------------------------------------------------------------------------
// Klasse    : - (befreundet mit Date)
// Funktion  : correctDate
// Beschr.   : Ueberpruefung, ob angegebenes Datum korrekt ist
// Definition: bool correctDate(const Datum& d)
// Parameter : d: Datum, das ueberprueft werden soll
// Rueckgabe : true, falls Datum korrekt
//             false sonst
//-----------------------------------------------------------------------------
bool correctDate(const Date& d)
{    
  // Tage pro Monat
  int dpm[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

  // Schaltjahr beruecksichtigen
  if( d.year % 4 == 0 && !(d.year % 100 == 0) || d.year % 400 == 0 )
    dpm[1] = 29;

  return d.month >= 1 && d.month <= 12 &&
    d.year >= 1900 &&
    d.day >= 1 && d.day <= dpm[d.month - 1];
}


//-----------------------------------------------------------------------------
// Klasse    : - (befreundet mit Date)
// Funktion  : Ausgabeoperator
// Beschr.   : gibt Datum auf stream aus
// Definition: ostream& operator<<(ostream& o, const Date& d)
// Parameter : o: Ausgabestrom, d: Datum, das ausgegeben werden soll
// Rueckgabe : o
//-----------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& o, const Date& d)
{
  if (d.day <10)
    o << "0";

  o << d.day << ".";

  if (d.month <10)
    o << "0";

  o << d.month << "." << d.year;

  return o;
}


