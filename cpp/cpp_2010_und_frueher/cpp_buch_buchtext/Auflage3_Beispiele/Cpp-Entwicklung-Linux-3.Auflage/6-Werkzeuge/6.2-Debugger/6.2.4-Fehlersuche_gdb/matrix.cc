// Datei: matrix.cc
#include <cmath>
#include <iostream>
#include <fstream>
#include "matrix.h"

using namespace std;

//Fre M�r 29 20:14:56 MET 2002
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

SymMatrix::SymMatrix ():
dat (0), size (0)
{
}

SymMatrix::SymMatrix (unsigned int _n)
{
  if (_n == 0)
    return;

  initMemory (_n);
}

SymMatrix::SymMatrix (const SymMatrix & _mat)
{
  initMemory (_mat.size);

  for (unsigned int i = 0; i < size; i++)
    for (unsigned int j = 0; j < size; j++)
      at (i, j) = _mat.at (i, j);
}

SymMatrix::~SymMatrix ()
{
  freeMemory ();
}

void
SymMatrix::initMemory (unsigned int _n)
{
  size = _n;
  dat = new double *[_n];
  for (unsigned int i = 0; i < _n; i++)
    dat[i] = new double[_n];
}

void
SymMatrix::freeMemory ()
{
  if (!dat)
    return;

  for (unsigned i = 0; i < size; i++)
    if (dat[i])
      delete[]dat[i];

  delete[]dat;
  dat = 0;
  size = 0;
}

bool
SymMatrix::enter ()
{
  // Pr�fe, ob Matrix bereits initialisiert ist
  if (size != 0)
    {
      cerr << "Matrix ist bereits belegt!" << endl;
      return false;
    }

  // Fordere die Gr��e an
  cout << "Bitte geben Sie die Gr��e der Matrix ein: ";
  cin >> size;

  // Bei 0 Abbruch der Eingabe
  if (size == 0)
    return false;

  // Belege Speicher
  initMemory (size);

  // Lasse Elemente eingeben
  for (unsigned int i = 0; i < size; i++)
    {
      for (unsigned int j = 0; j < size; j++)
	{
	  cout << "a(" << i << "," << j << ")=";
	  double x;
	  cin >> x;
	  at (i, j) = x;
	}
      cout << endl;
    }

  return true;
}

bool
SymMatrix::read (const string & _fname)
{
  // Pr�fe, ob Matrix bereits initialisiert ist
  if (size != 0)
    {
      cerr << "Matrix ist bereits belegt!" << endl;
      return false;
    }

  // �ffne Datei  
  ifstream in (_fname.c_str ());
  if (in.bad ())
    {
      cerr << "Datei " << _fname
	<< " kann nicht ge�ffnet werden!" << endl << flush;
      return false;
    }

  // Lies Gr��e der Matrix
  in >> size;

  // Bei 0 Abbruch des Einlesens
  if (size == 0)
    return false;

  // Belege Speicher
  initMemory (size);

  // Lies Elemente
  for (unsigned int i = 0; i < size; i++)
    for (unsigned int j = 0; j < size; j++)
      {
	double x;
	in >> x;
	at (i, j) = x;

	// Pr�fe ob Dateiende erreicht
	if (in.eof ())
	  {
	    cerr << "Dateiende bei a(" << i
	      << "," << j << ") erreicht!" << endl << flush;
	    return false;
	  }
      }

  return true;
}

double
SymMatrix::at (unsigned int _i, unsigned int _j) const
{
  if (_i > size || _j > size)
    {
      cerr << "Index (" << _i << "," << _j <<
	") au�erhalb des g�ltigen Bereichs!" << endl;
      exit (-1);
    }

  return dat[_i][_j];
}

double &
SymMatrix::at (unsigned int _i, unsigned int _j)
{
  if (_i > size || _j > size)
    {
      cerr << "Index (" << _i << "," << _j <<
	") au�erhalb des g�ltigen Bereichs!" << endl;
      exit (-1);
    }

  return dat[_i][_j];
}

unsigned int
SymMatrix::getSize () const
{
  return size;
}

SymMatrix
SymMatrix::subMatrix (unsigned int _k) const
{
  // Untermatrix eine Dimension kleiner
  SymMatrix u (size - 1);

  // Untermatrix nach der ersten Zeile
  for (unsigned int i = 0; i < size - 1; i++)
    for (unsigned int j = 0, uj = 0; j < size; j++)
      if (j != _k)		// Betrachte nur Spalten ungleich _k
	u.at (i, uj++) = dat[i + 1][j];

  return u;
}

void
SymMatrix::print ()
{
  for (unsigned i = 0; i < size; i++)
    {
      cout << "(";
      for (unsigned j = 0; j < size; j++)
	cout << at (i, j) << " ";
      cout << ")" << endl << endl;
    }
}

double
determinant (const SymMatrix & _a)
{
  double det = 0.0;

  // Fange Spezialf�lle ab
  switch (_a.getSize ())
    {
    case 0:
      return 0.0;
    case 1:
      return _a.at (0, 0);
    case 2:
      return _a.at (0, 0) * _a.at (1, 1) - _a.at (0, 1) * _a.at (1, 0);
    default:
      {
	// allgemeiner Fall
	for (unsigned int k = 0; k < _a.getSize (); k++)
	  {
	    SymMatrix u = _a.subMatrix (k);

	    // Vorzeichen wechselnd
	    if (k % 2)
	      det -= _a.at (0, k) * determinant (u);
	    else
	      det += _a.at (0, k) * determinant (u);
	  }
	break;
      }
    }

  return det;
}

