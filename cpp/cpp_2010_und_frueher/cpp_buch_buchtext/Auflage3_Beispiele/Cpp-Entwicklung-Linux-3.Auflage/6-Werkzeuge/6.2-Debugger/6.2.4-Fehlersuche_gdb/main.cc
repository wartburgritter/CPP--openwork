// Datei main.cc
#include <iostream>
#include <iomanip>
#include "matrix.h"

using namespace std;

int main()
{
  string filename;
  cout << "Datei mit Matrix: ";
  cin >> filename;
    
  SymMatrix a;
  if (!a.read(filename))
    return -1;
    
  double d=determinant(a);
  cout << "Determinante ist " 
       << setprecision(6) << d << endl;
  
  return 0;
}
  
