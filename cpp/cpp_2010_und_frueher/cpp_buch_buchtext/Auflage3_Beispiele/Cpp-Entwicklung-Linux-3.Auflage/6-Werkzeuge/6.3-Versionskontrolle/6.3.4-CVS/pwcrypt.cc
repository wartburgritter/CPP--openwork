#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

using namespace std;

char getSaltChar()
{
  // erzeuge Zahl zwischen 0 und 25
  char s = (char) (26.0*random()/(RAND_MAX+1.0)); 

  // addiere 65 (Großbuchstaben) oder 97 
  // (Kleinbuchstaben) hinzu
  s += (random()/(RAND_MAX+1.0) > 0.5) ? 65 : 97;

  return s;
}

int main(int argc, char** argv)
{
  // initialisiere den Zufallszahlengenerator
  srandom(time(0));

  // brich das Programm ab, wenn es ohne Argument 
  // aufgerufen wurde
  if (argc < 2)
  {
    cerr << "Verwendung: pwcrypt <passwort>" 
         << endl << endl;
    return -1;
  }

  char sa[2];

  // erzeuge zwei zufällige Zeichen 
  sprintf(sa, "%c%c", getSaltChar(), 
          getSaltChar());

  // verschlüssele Passwort und gib es aus
  cout << crypt(argv[1], sa) << endl;

  return 0;
}
