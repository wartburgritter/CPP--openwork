/* Das erste Programm:
   Summe der Zahlen von 1 bis 10
*/

#include <iostream>

int main(void)
{
  // Variable deklarieren und initialisieren
  int zahl;
//  zahl = 0;
  
  // Schleife durchlaufen
  for (int i = 1; i <= 10; i++)
  {
    zahl += i;
    std::cout << "Summe bis " << i << ": "; 
    std::cout << zahl << "\n";
  }
}
