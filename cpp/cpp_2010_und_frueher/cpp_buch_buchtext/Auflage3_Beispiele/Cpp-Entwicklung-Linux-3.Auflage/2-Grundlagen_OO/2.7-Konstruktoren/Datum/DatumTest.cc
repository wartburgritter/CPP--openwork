#include <iostream>
#include "Datum.hh"

Datum heutigesDatum;

int main()
{
  std::cout << "Heute ist der ";
  heutigesDatum.ausgeben();
  return 0;
}
