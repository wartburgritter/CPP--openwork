#include <iostream>

using namespace std;

class Basis
{
public:
  virtual int id() {
    return 0; }
};

class Unterklasse : public Basis
{
public:
  virtual int id() {
    return 1; }
};

void test1(Basis* _b)
{
  Unterklasse* u;
  // Testweise auf static_cast zu �ndern
  // u = static_cast<Unterklasse*>(_b);
  u = dynamic_cast<Unterklasse*>(_b);
  if (u)
    cout << "test1: " << u->id() << endl;
  else
    cout << "_b nicht vom Typ Unterklasse!" << endl;
}

void test2(Basis& _b)
{
  try{
    Unterklasse& u = dynamic_cast<Unterklasse&>(_b);
  //  if (u)
    cout << "test2: " << u.id() << endl;
    //  else
  }
  catch(...)
  {
    cout << "_b nicht vom Typ Unterklasse!" << endl;
  }
}

int main()
{
  Basis        b;
  Unterklasse  u;

  test1(&b);
  test1(&u);

  test2(b);
  test2(u);

  return 0;
}


