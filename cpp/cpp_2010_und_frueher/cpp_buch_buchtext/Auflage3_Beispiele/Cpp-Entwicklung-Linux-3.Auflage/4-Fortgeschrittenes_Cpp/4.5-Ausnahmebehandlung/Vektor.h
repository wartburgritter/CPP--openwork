#ifndef _VEKTOR_H_
#define _VEKTOR_H_

#include <string>
#include <sstream>

// Ausnahmebasisklasse
class VektorException
{
public:
  VektorException() throw() {}
  virtual ~VektorException();
  virtual std::string what() const = 0;
};

// Ausnahme bei Bereichüberschreitung
class VektorOutOfRange
{
private:
  unsigned int size, index;

public:
  VektorOutOfRange(unsigned int _size,
		   unsigned int _index) :
    size(_size), index(_index) {}
  virtual ~VektorOutOfRange() {};
  virtual std::string what() const
  {
    std::ostringstream s;
    s << "Bereichsüberschreitung (Größe: " 
      << size << ", Index: " << index
      << ")" ;
    return s.str();
  }
};

// Ausnahme bei Speichermangel
class VektorNoMem
{
private:
  unsigned long size;

public:
  explicit VektorNoMem(unsigned long _size) :
    size(_size) {}
  virtual ~VektorNoMem() {};
  virtual std::string what() const
  {
    std::ostringstream s;
    s << "Speicher voll beim Anlegen von " 
      << size << " bytes";
    return s.str();
  }
};

// Deklaration der Klasse
template <typename T> class Vektor
{
private:
  unsigned int size;
  T* v;

public:
  Vektor() : size(0), v(0) {}
  Vektor(unsigned int _size) throw(VektorNoMem);
  Vektor(const Vektor& _vek) throw(VektorNoMem);
  ~Vektor() { if (v) delete[] v; }
  void resize(unsigned int _size) throw(VektorNoMem);
  unsigned int getSize() 
  { return size; }
  
  const T& at(unsigned int _i) const throw(VektorOutOfRange);
  T& at(unsigned int _i) throw(VektorOutOfRange);
  
  // Index-Operatoren ohne Bereichsprüfung
  const T& operator[](unsigned int _i) const throw()
  { return v[_i]; }
  T& operator[](unsigned int _i) throw()
  { return v[_i]; }
  
  Vektor<T>& operator=(const Vektor<T>& _vector) throw(VektorNoMem);
  operator const T*() const;
};

// Konstruktor mit Groessenvorgabe
template <typename T> 
Vektor<T>::Vektor(unsigned int _size) throw(VektorNoMem) :         
  size(_size)
{ 
  v = new T[size]; 
  if (v == 0)
    throw VektorNoMem(size);
}

// Kopierkonstruktor
template <typename T>
Vektor<T>::Vektor(const Vektor<T>& _vek) throw(VektorNoMem):
  size(0) 
{
  operator=(_vek);
}

// Initialisierung mit Groessenvorgabe
template <typename T> 
void Vektor<T>::resize(unsigned int _size) throw(VektorNoMem)
{
  if (v)
    delete[] v;

  size = _size;
  v = new T[size];
  if (v == 0)
    throw VektorNoMem(size);
}

// Lesezugriff
template <typename T>
const T& Vektor<T>::at(unsigned int _i) const throw(VektorOutOfRange)
{
  // Vorbedingung: _i gültig und v vorhanden
  if (v == 0 || _i>=size)
    throw VektorOutOfRange(size,_i);

  return v[_i];
}

// Schreibzugriff strstr()
template <typename T>
T& Vektor<T>::at(unsigned int _i) throw(VektorOutOfRange) 
{
  // Vorbedingung: _i gültig und v vorhanden
  if (v == 0 || _i>=size)
    throw VektorOutOfRange(size,_i);

  return v[_i];
}

// Zuweisungsoperator
template <typename T>
Vektor<T>& Vektor<T>::operator=(const Vektor<T>& _vek) throw(VektorNoMem)
{
  if (this != &_vek)
  {
    // 1. Kopieren auf lokalen Zeiger
    T* tmp = v;

    // 2. Reservieren des Speichers
    v = new T[_vek.size];
    if (v == 0) // kein Speicher!
    {
      v = tmp;
      throw VektorNoMem(size);
    }
    size = _vek.size;

    // 3. Kopieren des Inhalts
    for(unsigned i=0; i<size; i++)
      v[i] = _vek.v[i];

    // 4. Freigeben des bisherigen Speichers
    if (tmp)
      delete[] tmp;
  }

  return *this;
}

// Typumwandlungsoperator
template <typename T>
Vektor<T>::operator const T*() const
{
  return v;
}


#endif  
