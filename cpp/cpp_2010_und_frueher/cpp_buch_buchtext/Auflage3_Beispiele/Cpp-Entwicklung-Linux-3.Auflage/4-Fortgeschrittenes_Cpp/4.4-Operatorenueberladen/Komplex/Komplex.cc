#include <iostream>
#include <cassert>
#include <cmath>

using namespace std;

class komplex
{
private:
  double re, im;

public:
  komplex(double _re = 0.0, 
          double _im = 0.0) :
    re(_re), im(_im) {}

  double real() const {
    return re; }
  double img() const {
    return im; }

  double abs() const {
    return sqrt(re*re + im*im); }
    
  komplex& operator+=(komplex _k);
  komplex& operator-=(komplex _k);
  komplex& operator*=(komplex _k);
  komplex& operator/=(komplex _k);
  
  // Freunde
  friend istream& operator>>(istream& _i, komplex& _k);
};

komplex& komplex::operator+=(komplex _k)
{
  re += _k.re;
  im += _k.im;

  return *this;
}

komplex operator+(komplex _x, komplex _y)
{
  komplex tmp = _x;
  return tmp += _y;
}

komplex& komplex::operator-=(komplex _k)
{
  re -= _k.re;
  im -= _k.im;

  return *this;
}

komplex operator-(komplex _x, komplex _y)
{
  komplex tmp = _x;
  return tmp -= _y;
}

komplex& komplex::operator*=(komplex _k)
{
  double r, i;
  r = re*_k.re - im*_k.im;
  i = re*_k.im - im*_k.re;
  
  re = r;
  im = i;

  return *this;
}

komplex operator*(komplex _x, komplex _y)
{
  komplex tmp = _x;
  return tmp *= _y;
}

komplex& komplex::operator/=(komplex _k)
{
  double r, i, abs_k = _k.abs();
  abs_k *= abs_k;
  // Division durch 0 nicht erlaubt!
  assert(abs_k); 
  r = (re*_k.re + im*_k.im)/abs_k;
  i = (-re*_k.im + im*_k.re)/abs_k;
  
  re = r;
  im = i;
  
  return *this;
}

komplex operator/(komplex _x, komplex _y)
{
  komplex tmp = _x;
  return tmp /= _y;
}

bool operator==(const komplex& _k1, 
                const komplex& _k2)
{
  return (_k1.real() == _k2.real() &&
          _k1.img()  == _k2.img());
}

bool operator!=(const komplex& _k1, 
                const komplex& _k2)
{
  return !(_k1==_k2);
}

ostream& operator<<(ostream& _o, const komplex& _k)
{
  _o << "(" << _k.real() << ", "
     << _k.img() << ")";
  return _o;
}

istream& operator>>(istream& _i, komplex& _k)
{
  _i >> _k.re >> _k.im;
  return _i;
}

int main()
{
   komplex a,b;
   komplex i(0,1);
   komplex c = 3.0;
   
   // Bilde Inverses von i
   komplex i_1 = 1.0/i;
   cout << "i " << i<< " * i^-1 " << i_1 
        << " = " << i*i_1 << endl;
        
   // Ein paar Rechenoperationen
   a = c + i;
   a += c;
   if (b==a)
   {
     cerr << "Da stimmt was nicht!" << endl;
   }
   else
     cout << "a = " << a << ", c = " << c << endl;
   
   return 0;
}
