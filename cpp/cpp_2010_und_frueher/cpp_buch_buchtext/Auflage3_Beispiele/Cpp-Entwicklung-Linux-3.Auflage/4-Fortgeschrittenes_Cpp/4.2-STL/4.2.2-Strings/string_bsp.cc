
#include <string>
#include <iostream>
#include <fstream>

using namespace std;
//---------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    string s1 = "Die C++-Standardbibliothek";
    string s2( "Die C++-Standardbibliothek", 7);

    s2 += "-Standardbibliothek";

    if (s1==s2)
        cout << "Die beiden Strings '" << s1 << "' und '"
             << s2 << "' stimmen �berein." << endl;

    s2 = s1.substr(16, 10) + ".dat";
    s1.replace(4, 3, "C");

    ofstream out_file(s2.c_str());
    out_file << "s1: '" << s1 << "' und s2: '" << s2 << "'" << endl;
    out_file << "'Standard' beginnt in s1 bei " << s1.find("Standard") << endl;

    return 0;
}
 
