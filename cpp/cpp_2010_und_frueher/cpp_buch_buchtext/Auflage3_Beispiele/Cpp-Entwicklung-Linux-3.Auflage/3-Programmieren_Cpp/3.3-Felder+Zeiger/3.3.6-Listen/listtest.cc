#include <iostream>
#include "Liste.h"

using namespace std;

// ------------------------------------------------------
// Hauptfunktion
// ------------------------------------------------------
int main()
{
  Liste liste;
 
  // fuelle Liste
  liste.push_back("JFK","New York (J.F.Kennedy)");
  liste.push_back("MUC","Muenchen");
  liste.push_back("FRA","Frankfurt Rhein/Main");
  liste.push_back("TXL","Berlin Tegel"); 
  liste.push_back("DUS","Duesseldorf"); 
  liste.push_back("DRS","Dresden"); 

  // gibt Liste aus
  ListElement* tmp;
  for(tmp = liste.front(); tmp != 0; tmp = tmp->naechstes)
  {
    cout << tmp->schluessel << ": " 
         << tmp->wert << endl;
  }

  return 0;
}
  

  
