#include <iostream>
#include <csignal>
#include <unistd.h>

using namespace std;

// Reaktionsfunktion auf das Signal
void handler(int signal)
{
  if (signal == SIGINT)
    cout << "Mit Strg-C bin ich nicht zu stoppen!" << endl;
  else
    psignal(signal, "Unbekanntes Signal: ");
}

int main()
{
  // Signal mit Reaktionsfunktion verbinden
  signal(SIGINT, handler);

  cout << "Ich arbeite endlos." << flush;
  
  while(1)
  {
    cout << "." << flush;
    sleep(1);
  }
  
  return 0;
}

     
