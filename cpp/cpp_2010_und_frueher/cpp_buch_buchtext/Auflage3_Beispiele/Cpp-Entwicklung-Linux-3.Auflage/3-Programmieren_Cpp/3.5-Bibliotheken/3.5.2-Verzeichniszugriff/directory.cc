#include <iostream>
#include <iomanip>
#include <sys/stat.h>
#include <unistd.h>
#include "directory.h"

using namespace std;

void DirEntry::print() const
{
  cout << setw(12) << m_size << "\t"
       << setw(4) << m_userid << ":"
       << setw(4) << m_groupid << "\t"
       << m_name;
}

Directory::Directory(const string& path)
{
  setDir(path);
}

int Directory::setDir(const string& path)
{
  // L�sche evtl. Inhalte des Vektors
  m_vector.clear();

  // �ffne das Verzeichnis
  m_dir = opendir(path.c_str());

  // Nicht vorhanden -> return
  if (!m_dir)
    return 0;

  dirent* entry;
  struct stat file_state;

  // Schleife �ber alle Eintr�ge
  entry = readdir(m_dir);
  while (entry)
  {
    stat(entry->d_name, &file_state);
    DirEntry d(entry->d_name, 
	       file_state.st_uid,
	       file_state.st_gid,
	       file_state.st_size);
    m_vector.push_back(d);
    
    entry = readdir(m_dir);
  }

  // Gib Anzahl der Eintr�ge zur�ck
  return (m_vector.size());
}

unsigned int Directory::numberOfEntries() const
{
    return (m_vector.size());
}

const DirEntry& Directory::getEntry(unsigned int number) const
{
  if (number < m_vector.size())
    return m_vector[number];

  cerr << "Fehler: Index " << number << " zu gro�!!" << endl;

  // Zu gro�? Gib Dummy zur�ck
  return m_dummy;
}









