#include <iostream>
#include <string>
#include "globber.h"
#include "directory.h"

using namespace std;

int main()
{
  Directory dir("./");

  if (dir.numberOfEntries())
  {
    cout << "Verzeichniseintraege in ." << endl;
    
    for(unsigned int i=0; i<dir.numberOfEntries(); i++)
    {
      dir.getEntry(i).print();
      cout << endl;
    }

    cout << endl << endl;
  }
  
  Globber myGlobber;
  string pattern = "*.cc";

  if (myGlobber.runMatch(pattern))
  {
    for(unsigned int i=0; i<myGlobber.numberOfMatches(); i++)
      cout << i << ".: " << myGlobber.getResult(i) << endl;
  }
  else 
    cout << "Muster [" << pattern << "] nicht gefunden!" << endl;

  return 0;
}





