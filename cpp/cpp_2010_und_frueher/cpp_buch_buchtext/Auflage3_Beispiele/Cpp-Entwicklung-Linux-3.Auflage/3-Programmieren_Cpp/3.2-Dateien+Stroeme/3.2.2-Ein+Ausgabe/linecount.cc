#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char** argv)
{
  if (argc<2) {
    cerr << "Bitte einen Dateinamen als"
         << " Argument angeben!" << endl;
    return -1;
  }

  ifstream in(argv[1]);
  if (!in) {
    cerr << "Datei " << argv[1] << " kann "
         << "nicht ge�ffnet werden!" << endl;
    return -2;
  }

  string buff;
  unsigned long l=0;

  while(!in.eof()) {
    getline(in, buff);
    l++;
  }

  cout << "Zeilen: " << l-1 << endl;
  return 0;
}
