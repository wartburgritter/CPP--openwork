#include <iostream>
#include <string>
#include <ctime>

using namespace std;

//-------------------------------------
// Abstrakte Basisklasse 
// fuer Telefongesellschaften
//-------------------------------------
class Telefongesellschaft
{
protected:
  string name;

public:
  Telefongesellschaft(const string& _name) :
    name(_name)
  {}

  const string& getName() const
  { return name; }

  virtual float berechneGebuehr(
    int _minuten) = 0;
};

//-------------------------------------
// Abgeleitete Klasse fuer 
// eine spezielle Gesellschaft
//-------------------------------------
class SiriusCom : 
  public Telefongesellschaft
{
public:
  SiriusCom() :
    Telefongesellschaft("SiriusCom")
  {}

  virtual float berechneGebuehr(
    int _minuten);
};

//-------------------------------------
float SiriusCom::berechneGebuehr(
  int _minuten)
{
  time_t now = time(NULL);
  tm z = *(localtime(&now));

  int wochentag = z.tm_wday;
  int stunde = z.tm_hour;
  float minutenpreis;

  if (wochentag > 0 && wochentag < 6)
  {
    // Werktag
    if (stunde >= 7 && stunde < 19)
    {
      // Zwischen 7 und 19 Uhr
      minutenpreis = 5.0;
    }
    else
    {
      // Abends und nachts
      minutenpreis = 3.0;
    }
  }
  else
  {
    // Wochenende
    minutenpreis = 2.0;
  }

  // Gib Ergebnis aus
  cout << "Ein " << _minuten << "-min�tiges  " 
       << "Gespr�ch kostet bei " << name 
       << " jetzt " << minutenpreis * _minuten 
       << " Cent." << endl;
  
  // Gib Berechnung zurueck
  return minutenpreis * _minuten;
}

//-------------------------------------
int main()
{
  SiriusCom carrier;

  float x = carrier.berechneGebuehr(3);
  cout << "Gesamtkosten: " << x/100
       << " EUR." << endl;

  return 0;
}

