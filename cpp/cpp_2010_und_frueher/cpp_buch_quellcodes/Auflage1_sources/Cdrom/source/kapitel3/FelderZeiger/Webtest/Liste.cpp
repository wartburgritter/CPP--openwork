#include "Liste.h"

Liste::~Liste()
{
  while(anzahl != 0)
    pop_front();
}

void Liste::push_back(const string& _schluessel,
    const string& _wert)
{
  ListElement* tmp = 
    new ListElement(_schluessel, _wert);
    
  if (letztes != 0)
    letztes->naechstes = tmp;
  else
    erstes = tmp;
    
  letztes = tmp;
  anzahl++;
}
    
void Liste::pop_front()
{
  if (anzahl == 0)
    return;
    
  ListElement* tmp = erstes;
  erstes = tmp->naechstes;
  if (erstes == 0)
    letztes = 0;
    
  delete tmp;
  anzahl--;
}  

