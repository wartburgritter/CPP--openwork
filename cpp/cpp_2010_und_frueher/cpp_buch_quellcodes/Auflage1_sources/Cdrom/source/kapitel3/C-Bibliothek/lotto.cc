// lotto.cc
//
// Testprogramm fuer Zufallszahlen und Quick-Sort

#include <cstdlib>  // fuer random() und qsort()
#include <iostream> // fuer cout
#include <iomanip>  // fuer setw()
#include <ctime>    // fuer time()

typedef unsigned int UINT;

// Vergleichsfunktion fuer qsort
int vergleiche(const void* a, const void* b)
{
  int a1 = *((int*)a);
  int b1 = *((int*)b);

  // Differenz ist 
  // < 0 wenn a1 < b1
  // = 0 bei Gleichheit
  // > 0 wenn a1 > b1
  return a1-b1;
}

UINT neue_zufallszahl(UINT* iarr, UINT idx)
{
  UINT z;
  // gibt es die neue Zahl schon?
  bool exists;

  do
  {
    exists = false;

    // Zufallszahl zwischen 1 und 49
    z = 1 + (UINT) (49.0*random()/(RAND_MAX+1.0));

    // pruefe ob bereits vorhanden
    for(UINT j=0; j<idx; j++)
      if (z == iarr[j])
      {
	exists = true;
	break;
      }
  } while (exists);

  return z;
}

int main()
{
  const UINT anzahl = 6;
  UINT iarr[anzahl];
  UINT zusatzz;

  // Initialisiere Zufallszahlengenerator
  srandom(time(0));

  // Bestimme 6 Zufallszahlen
  for(UINT i=0; i<anzahl; i++)
  {
    iarr[i] = neue_zufallszahl(iarr, i);
  }
    
  // und die Zusatzzahl
  zusatzz = neue_zufallszahl(iarr, anzahl);

  // Sortiere das Feld
  qsort( iarr, anzahl, sizeof(UINT), vergleiche);

  // Gib das Ergebnis aus
  cout << "Die Lottozahlen: ";

  for(UINT i=0; i<anzahl; i++)
    cout << setw(3) << iarr[i];

  cout << " (" << zusatzz << ")" << endl;

  return 0;
}
