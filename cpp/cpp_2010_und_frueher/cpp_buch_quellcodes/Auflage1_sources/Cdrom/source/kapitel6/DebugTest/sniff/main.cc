#include <iostream>
#include <iomanip>
#include "matrix.h"


int main()
{
  const string testname="cyclic.dat";
  string filename;
  
  cout << "Datei mit Matrix [cyclic.dat]: ";
  cin >> filename;
  if (filename == "d")
    filename = testname;
    
  SymMatrix a;
  if (!a.read(filename))
    return -1;
    
  double d=determinant(a);
  cout << "Determinante ist " << setprecision(6) << d << endl;
  
  return 0;
}
  
