// Datei: birthlist.h
#include <string>
#include "date.h"

//---------------------------------------------
// Klasse fuer Elemente der Liste
//---------------------------------------------
struct BirthListElement
{
  BirthListElement* next;
  string            name;
  string            surname;
  Date              date;
  
  // Standardkonstruktor
  BirthListElement() :
    next(0) {}
    
  // Spezieller Konstruktor
  BirthListElement(const string& _name,
    const string& _surname, unsigned short _day,
    unsigned short _month, unsigned short _year) :
    next(0),
    name(_name),
    surname(_surname),
    date(_day, _month, _year) 
    {}
};

//---------------------------------------------
// Listenklasse 
//---------------------------------------------
class BirthList
{
private:
  BirthListElement* first;
  BirthListElement* last;
  int size;
  
public:
  BirthList() :
    first(0), last(0), size(0) {}
    
  virtual ~BirthList();
  
  bool empty() const 
    { return (size == 0); }
  
  int getSize() const
    { return size; }
    
  void pushBack(const string& _name,
    const string& _surname, unsigned short _day,
    unsigned short _month, unsigned short _year);

  void popFront();
  
  BirthListElement* front()
    { return first; }

  int load(const string& filename);

  bool check(const Date& d);
};
  
  
    
