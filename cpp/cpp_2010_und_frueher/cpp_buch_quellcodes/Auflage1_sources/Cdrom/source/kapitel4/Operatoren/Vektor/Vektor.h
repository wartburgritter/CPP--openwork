#ifndef _VEKTOR_H_
#define _VEKTOR_H_

// Verwendung von Assertions
#include <cassert>

// Deklaration der Klasse
template <typename T> class Vektor
{
private:
  unsigned int size;
  T* v;

public:
  Vektor() : size(0), v(0) {}
  Vektor(unsigned int _size);
  Vektor(const Vektor& _vek);
  ~Vektor() { if (v) delete[] v; }
  void resize(unsigned int _size);
  unsigned int getSize() 
  { return size; }
  
  const T& at(unsigned int _i) const;
  T& at(unsigned int _i);
  
  // Index-Operatoren ohne Bereichspr�fung
  const T& operator[](unsigned int _i) const
  { return v[_i]; }
  T& operator[](unsigned int _i)
  { return v[_i]; }
  
  Vektor<T>& operator=(const Vektor<T>& _vector);
  operator const T*() const;
};

// Konstruktor mit Groessenvorgabe
template <typename T> 
Vektor<T>::Vektor(unsigned int _size) :         
  size(_size)
{ 
  v = new T[size]; 
}

// Kopierkonstruktor
template <typename T>
Vektor<T>::Vektor(const Vektor<T>& _vek) :
  size(0)
{
  operator=(_vek);
}

// Initialisierung mit Groessenvorgabe
template <typename T> 
void Vektor<T>::resize(unsigned int _size)
{
  if (v)
    delete[] v;

  size = _size;
  v = new T[size];
}

// Lesezugriff
template <typename T>
const T& Vektor<T>::at(unsigned int _i) const
{
  // Vorbedingung: _i g�ltig und v vorhanden
  assert(_i<size && v!=0);
  return v[_i];
}

// Schreibzugriff
template <typename T>
T& Vektor<T>::at(unsigned int _i) 
{
  // Vorbedingung: _i g�ltig und v vorhanden
  assert(_i<size && v!=0);
  return v[_i];
}

// Zuweisungsoperator
template <typename T>
Vektor<T>& Vektor<T>::operator=(const Vektor<T>& _vek)
{
  if (this != &_vek)
  {
    // 1. Kopieren auf lokalen Zeiger
    T* tmp = v;

    // 2. Reservieren des Speichers
    v = new T[_vek.size];
    if (v == 0) // kein Speicher!
    {
      v = tmp;
      return *this;
    }
    size = _vek.size;

    // 3. Kopieren des Inhalts
    for(unsigned i=0; i<size; i++)
      v[i] = _vek.v[i];

    // 4. Freigeben des bisherigen Speichers
    if (tmp)
      delete[] tmp;
  }

  return *this;
}

// Typumwandlungsoperator
template <typename T>
Vektor<T>::operator const T*() const
{
  return v;
}


#endif  
