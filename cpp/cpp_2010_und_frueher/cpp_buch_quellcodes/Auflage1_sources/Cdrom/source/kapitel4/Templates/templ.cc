#include <iostream>

template <typename T>
T max (T a, T b)
{
  return (a>b)? a: b;
}

template<>
const char* max(const char* a, const char* b)
{
  return strcmp(a,b)>0? a: b;
}

int main()
{
  cout << "max(3, 3.5): " << max<double>(3, 3.5) << endl;
  cout << "max(abc, bcde): " << max("abc", "bcde") << endl;

  return 0;
}
