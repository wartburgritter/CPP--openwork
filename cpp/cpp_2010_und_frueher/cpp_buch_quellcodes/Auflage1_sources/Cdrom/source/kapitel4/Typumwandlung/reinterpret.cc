#include <fstream.h>
#include <string>

class ValueBuffer
{
private:
  int* buffer;
  unsigned int size;

public:
  ValueBuffer() :
    buffer(0), size(0) {}
  ~ValueBuffer() {
    if (buffer) delete[] buffer; }

  void fill();
  int write(const string& _name) const;
  int read(const string& _name);
  void print();
};

void ValueBuffer::fill()
{
  size = 20;
  buffer = new int[size];

  for(unsigned int i=0; i<size; i++)
    buffer[i] = 2*i+1;
}

int ValueBuffer::write(const string& _name) const
{
  // Datei �ffnen
  ofstream out(_name.c_str(), ios::out | ios::binary);

  if(out.bad())
  {
    cerr << _name 
         << " kann nicht ge�ffnet werden!" 
         << endl;
    return -1;
  }

  // Anzahl der Daten schreiben
  out << size;
  if (out.fail())
  {
    cerr << "Schreibfehler!" << endl;
    return -2;
  }

  // Daten selbst schreiben
  out.write(reinterpret_cast<char*>(buffer), size*sizeof(int));
  out.close();
  return 0;
}

int ValueBuffer::read(const string& _name)
{
  // Datei �ffnen
  ifstream in(_name.c_str(), ios::in | ios::binary);
  if(in.bad())
  {
    cerr << _name 
         << " kann nicht ge�ffnet werden!" 
         << endl;
    return -1;
  }

  // Anzahl der Daten einlesen
  in >> size;

  // Puffer in der Gr��e reservieren
  buffer = new int[size];

  // Daten selbst lesen
  in.read(reinterpret_cast<char*>(buffer), size*sizeof(int));

  if (in.fail())
  {
    cerr << "Zu wenig Daten in " << _name 
         << "!" << endl;
    return -2;
  }

  in.close();
  return 0;
}

void ValueBuffer::print()
{
  if (!size)
    return;

  cout << "(";
  for(unsigned int i=0; i<size-1; i++)
    cout << buffer[i] << ", ";

  cout << buffer[size-1] << ")" << endl;
}

// Test der Klasse
int main()
{
  ValueBuffer vb_out, vb_in;

  vb_out.fill();
  vb_out.write("feld.dat");

  vb_in.read("feld.dat");
  vb_in.print();
  return 0;
}

