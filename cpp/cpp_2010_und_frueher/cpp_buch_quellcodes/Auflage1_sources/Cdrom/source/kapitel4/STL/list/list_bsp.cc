
#include <list>
#include <stack>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

struct Konto {
    unsigned long nummer;
    string inhaber;

    Konto() : nummer(0L) {}
};

//---------------------------------------------------------------------------
void liesKonten(list<Konto>& kontoListe,
  list<Konto>& inverseListe, const string& dateiName)
{
    ifstream in_file(dateiName.c_str());
    while (in_file)
    {
      Konto einKonto;
      in_file >> einKonto.nummer >> einKonto.inhaber;
      inverseListe.push_front(einKonto);   // am Anfang einfügen
      kontoListe.push_back(einKonto);    // am Ende einfügen
    }

    list<Konto>::iterator  iter;

    for(iter=kontoListe.begin(); iter != kontoListe.end(); iter++)
      cout << iter->nummer << "\t "      // erste Zugriffsmöglichkeit
           << (*iter).inhaber << endl;   // zweite Zugriffsmöglichkeit

}

//---------------------------------------------------------------------------
void testStack(const string& dateiName)
{
    stack<string>  myStack;

    ifstream in_file(dateiName.c_str());
    while (in_file)
    {
      unsigned long nummer;
      string inhaber;
      in_file >> nummer >> inhaber;

      myStack.push(inhaber);
      cout << myStack.top() << endl;
    }

    cout << "---------------" << endl;

    while (!myStack.empty())
    {
      cout << myStack.top() << endl;
      myStack.pop();
    }
}

//---------------------------------------------------------------------------
int main(void)
{
  list<Konto> kontoListe;
  list<Konto> inverseListe;

  liesKonten(kontoListe, inverseListe,"konten.txt");
  testStack("konten.txt");

  return 0;
}
