
#include <list>
#include <stack>
#include <string>
#include <vector>
#include <algorithm>    // f�r find, replace, sort etc.
#include <functional>   // f�r greater
#include <strstream>
#include <iostream>
#include <fstream>

//---------------------------------------------------------------------------
//using namespace std;

struct Konto {
  unsigned long nummer;
  string inhaber;

  Konto() : nummer(0L) {}  // string hat selbst einen Default-Konstruktor
};

//---------------------------------------------------------------------------
void liesKonten(list<Konto>& kontoListe,
  list<Konto>& inverseListe, const string& dateiName)
{
  ifstream in_file(dateiName.c_str());
  if (in_file.bad()) return;

  while (in_file)
  {
    Konto einKonto;
    in_file >> einKonto.nummer >> einKonto.inhaber;
    inverseListe.push_front(einKonto);   // am Anfang einf�gen
    kontoListe.push_back(einKonto);    // am Ende einf�gen
  }

  list<Konto>::iterator  iter;

  for(iter=kontoListe.begin(); iter != kontoListe.end(); iter++)
    cout << iter->nummer << "\t "      // erste Zugriffsm�glichkeit
         << (*iter).inhaber << endl;   // zweite Zugriffsm�glichkeit
}

//---------------------------------------------------------------------------

void findeInListe()
{
  list<unsigned>  l;
  unsigned n;

  for(n=1; n<=5; n++)
    for(unsigned m=1; m<=n; m++)
        l.push_back(m);

  // Beispiel f�r Suchen und Ersetzen
  list<unsigned>::iterator  iter = find(l.begin(), l.end(), 3);

  while (iter != l.end())
  {
    *iter = 99;
    iter++;
    iter = find(iter, l.end(), 3);  // n�chstes Element suchen
  }

  replace(l.begin(), l.end(), 99, 3);

  // Doch noch eine Liste mit '99' erzeugen
  list<unsigned>  l2;

  copy(l.begin(), l.end(), back_inserter(l2));
  replace(l2.begin(), l2.end(), 3, 99);
}

//---------------------------------------------------------------------------
void sortBeispiel()
{
  vector<unsigned>            v(12);
  vector<unsigned>::iterator  iter;

  for(unsigned i=0; i<4; i++)
    for(unsigned j=0; j<3; j++)
      v[i*3+j] = i+j;

  // Beispiel f�r Sortieren
  cout << "Unsortierter Vektor: ";
  for(iter = v.begin(); iter != v.end(); iter++)
    cout << *iter << " ";
  cout << endl;

  sort(v.begin(), v.end());

  cout << "Sortierter Vektor: ";
  for(iter = v.begin(); iter != v.end(); iter++)
    cout << *iter << " ";
  cout << endl;
}
//---------------------------------------------------------------------------
// Pr�dikatsfunktion
bool hat28(const Konto& einKonto)
{
  ostrstream ostr;
  ostr << einKonto.nummer;
  string nrstr(ostr.str());
  if (nrstr.find("28") != string::npos)
    return true;

  return false;
}

//---------------------------------------------------------------------------
void sucheNach28(const string& dateiName)
{
  list<Konto>  kontoListe;

  // Konten einlesen
  ifstream in_file(dateiName.c_str());
  if (in_file.bad()) return;

  while (in_file)
  {
    Konto einKonto;
    in_file >> einKonto.nummer >> einKonto.inhaber;
    kontoListe.push_back(einKonto);    // am Ende einf�gen
  }

  // Beispiel f�r bedingtes Suchen
  cout << endl << "Suche nach Kontonummern mit '28':" << endl;
  list<Konto>::iterator  iter =
    find_if(kontoListe.begin(), kontoListe.end(), hat28);

  while (iter != kontoListe.end())
  {
    cout << iter->nummer << "\t " << iter->inhaber << endl;
    iter++;

    // n�chstes Element suchen
    iter = find_if(iter, kontoListe.end(), hat28);
  }
}

//---------------------------------------------------------------------------
int main(void)
{
  list<Konto> kontoListe;
  list<Konto> inverseListe;

  liesKonten(kontoListe, inverseListe,"konten.txt");

  findeInListe();

  sortBeispiel();

  sucheNach28("konten.txt");

  return 0;
}

