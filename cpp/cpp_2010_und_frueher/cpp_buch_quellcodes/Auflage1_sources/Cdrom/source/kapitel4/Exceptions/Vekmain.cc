#include <iostream>
#include <cstring>
#include "Vektor.h"
#include "Datum.hh"

int main()
{
  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

  try
  {
  // einen Index zu viel
  for(i=0; i<5; i++)
    cout << v.at(i) << " ";
  }
  catch (VektorOutOfRange& _exc)
  {
    cerr << "Fehler: " << _exc.what() << endl;
  }

  cout << endl;

  // Vektor von Datum
  Vektor<Datum> daten(31);

  for(i=0; i<31; i++)
    daten.at(i).setze(i+1, 12, 2000);

  for(i=23; i<31; i++)
    daten.at(i).ausgeben();

  // Vektor von Vektor
  Vektor<Vektor<double> > m(3);
  for(i=0; i<3; i++) 
    m[i].resize(3);

  for(i=0; i<3; i++)
    m.at(i).at(i) = 1;

  // Vektor von Zeichen
  Vektor<char> code(25);
  char c='a';
  for(unsigned short i=0; i<25; i++)
    code[i] = c++;
    
  char* occ;
  occ = strstr((const char*)code, "fghi");
  cout << "'fghi' kommt bei " << occ << " vor." << endl;

  return 0;
}
