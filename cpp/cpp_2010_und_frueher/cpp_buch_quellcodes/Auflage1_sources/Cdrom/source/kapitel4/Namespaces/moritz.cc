#include <cmath>
#include "moritz.h"

using namespace Moritz;

const double EPS = 0.1;

double algorithm(double x)
{
  return std::sin(x);
}
