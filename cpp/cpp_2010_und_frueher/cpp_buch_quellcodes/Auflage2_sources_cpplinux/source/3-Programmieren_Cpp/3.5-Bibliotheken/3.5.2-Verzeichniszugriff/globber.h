#include <glob.h>
#include <string>
#include <vector>

class Globber
{
private:
  glob_t   m_globbuf;
  std::vector<std::string> m_vector;
  std::string   m_dummy;

public:
  Globber();

  int runMatch(const std::string& pattern);
  unsigned int numberOfMatches() const;
  const std::string& getResult(unsigned int number) const;
};



