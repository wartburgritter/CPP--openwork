#include <unistd.h>
#include <glob.h>
#include "globber.h"

using namespace std;

Globber::Globber() 
{
  m_globbuf.gl_offs = 0;
}

int Globber::runMatch(const string& pattern)
{
  // starte den Mustervergleich
  int res = glob(pattern.c_str(), 0, NULL, &m_globbuf);

  // Keine Eintr�ge erf�llen das Muster
  if (res)
    return 0;

  // Speichere Eintr�ge
  for (int i=0; i<m_globbuf.gl_pathc; i++)
    m_vector.push_back(m_globbuf.gl_pathv[i]);

  // Gib den Glob-Puffer frei
  globfree(&m_globbuf);

  return m_vector.size();
}

unsigned int Globber::numberOfMatches() const
{
  return (m_vector.size());
}

const string& Globber::getResult(unsigned int number) const
{
  if (number < m_vector.size())
    return m_vector[number];

  return m_dummy;
}







