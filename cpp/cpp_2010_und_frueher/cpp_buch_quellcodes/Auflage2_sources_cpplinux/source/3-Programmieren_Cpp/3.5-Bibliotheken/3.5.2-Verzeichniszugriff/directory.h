#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <vector>

class DirEntry
{
private:
  std::string m_name;
  long m_userid;
  long m_groupid;
  unsigned long m_size;

public:
  DirEntry() : 
    m_userid(0L), m_groupid(0L),
    m_size(0L)
    {}

  DirEntry(const std::string& name, long userid,
	   long groupid, unsigned long size) :
    m_name(name), m_userid(userid), 
    m_groupid(groupid), m_size(size)
    {}

  void print() const;
};

class Directory
{
private:
  DIR*  m_dir;
  std::vector<DirEntry> m_vector;
  DirEntry   m_dummy;

public:
  Directory() {}
  Directory(const std::string& path);

  int setDir(const std::string& path);
  unsigned int numberOfEntries() const;
  const DirEntry& getEntry(unsigned int number) const;
};



