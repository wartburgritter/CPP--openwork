#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <unistd.h>

using namespace std;

main()
{
  int i,j;
  const double wk = 1.95583;

  ostringstream ostr;
  ostr << "euro." << getpid() << ".tex";

  ofstream out((ostr.str()).c_str());
  out.precision(2);
  out.setf(ios::fixed);

  out << "\\documentclass{article}" << endl;
  out << "\\begin{document}" << endl;
  out << "\\section*{Umrechungstabelle"
      << " DM -- Euro}" << endl;
  out << "\\begin{tabular}{rr|rr}" << endl;
  out << "DM & EUR & EUR & DM \\\\ \\hline" 
      << endl;

  for(i=1;i<=1000;i*=10) {
    for(j=1;j<=5 && i*j<=1000;j+=1) {
      out << (float)i*j << " & " 
	  << i*j/wk << " & "
	  << (float)i*j << " & "
	  << i*j*wk << " \\\\" << endl;
    }
  }
  out << "\\end{tabular}" << endl;
  out << "\\end{document}" << endl;
  out.close();

  ostringstream ltcmd;
  ltcmd << "latex " << ostr.str();
  if (!system((ltcmd.str()).c_str())) {
    ostringstream dvicmd;
    dvicmd << "dvips euro." << getpid() 
           << ".dvi -o";
    if (system((dvicmd.str()).c_str()))
      cerr << "PS-Datei konnte nicht"
           << " erzeugt werden!" << endl;
  }
  else
    cerr << "DVI-Datei konnte nicht" 
	 << " erzeugt werden!" << endl;
  return(0);
}


