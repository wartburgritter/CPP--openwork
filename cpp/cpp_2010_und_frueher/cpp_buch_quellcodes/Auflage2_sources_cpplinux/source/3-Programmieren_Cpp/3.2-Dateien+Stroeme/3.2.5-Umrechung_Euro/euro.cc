#include <iostream>
#include <iomanip>

using namespace std;

main()
{
  int i,j;
  const double wk = 1.95583;

  cout.precision(2);
  cout.setf(ios_base::right | ios_base::fixed, 
    ios_base::adjustfield | ios_base::floatfield);
  cout << setw(10) << "DM" << setw(10) 
       << "Euro" << " | " << setw(10) 
       << "Euro" << setw(10) << "DM" << endl;

  for(i=1;i<=44;i++) cout << '-';
  cout << endl;

  for(i=1;i<=1000;i*=10) {
    for(j=1;j<=5 && i*j<=1000;j+=1) {
      cout << setw(10) << (float)i*j 
	   << setw(10) << i*j/wk << " | "
           << setw(10) << (float)i*j 
	   << setw(10) << i*j*wk << endl;
    }
  }
  return(0);
}


