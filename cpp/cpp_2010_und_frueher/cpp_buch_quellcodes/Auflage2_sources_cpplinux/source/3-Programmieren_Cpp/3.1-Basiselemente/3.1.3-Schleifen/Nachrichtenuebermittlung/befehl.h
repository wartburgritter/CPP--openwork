// Systembibliotheken
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

//---- Klasse Befehl ------------------------------------------------------
enum RobotBefehl {
  START,
  BEWEGE, 
  BEWEGE_OBJEKT,
  ENDE
};

class Befehl
{
public:
  long msg_typ;
  RobotBefehl befehl;
  short arg1;
  short arg2;
  
  Befehl() : 
    msg_typ(1), 
    befehl(START),
    arg1(0),
    arg2(0)
  {}
  
  Befehl(RobotBefehl _befehl,
    short _arg1, short _arg2) :
    msg_typ(1),
    befehl(_befehl),
    arg1(_arg1), arg2(_arg2)
  {}
};

//---- Klasse KommunikationsController --------------------------------------
class KommunikationsController
{
protected:
  int msgid;
  
public:
  KommunikationsController() :
    msgid(0) {}
  int initialisiereVerbindung();
  int kappeVerbindung();
};

inline int KommunikationsController::initialisiereVerbindung()
{
  // Initialisierung der Nachrichten-Schlange
  msgid = msgget((key_t)123, 0666 | IPC_CREAT);
  
  if (msgid == -1)
  {
    cerr << "Nachrichten-Schlange konnte nicht "
         << "erzeugt werden!" << endl;
    return -1;
  }
  return 0;
}  

inline int KommunikationsController::kappeVerbindung()
{    
  if (msgctl(msgid, IPC_RMID, 0) == -1)
  {
    cerr << "msgctl() fehlgeschlagen!" << endl;
    return -1;
  }
  return 0;
}

