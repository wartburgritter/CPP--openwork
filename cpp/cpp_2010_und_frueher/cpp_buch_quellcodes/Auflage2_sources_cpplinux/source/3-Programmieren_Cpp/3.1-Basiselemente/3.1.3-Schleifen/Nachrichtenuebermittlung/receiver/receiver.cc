#include <cstdlib>
#include <iostream>
#include "../befehl.h"

// --------------------------------------------------------
class Robot : public KommunikationsController
{
public:
  Robot() :
    KommunikationsController() {}
  void bewegeArm(short _pos) {
    cout << "Arm nun an Position " << _pos << endl;
  }

  void bewegeScheibe(short _pos1, short _pos2) {
    cout << "Bewege Scheibe von " << _pos1 
         << " nach " << _pos2 << endl;  
  }
  
  int verarbeiteNachrichten();
};

// --------------------------------------------------------
int Robot::verarbeiteNachrichten()
{
  bool istEnde = false;
  Befehl einBefehl;
  
  while(!istEnde)
  {
    int erg = msgrcv(msgid, (void*)&einBefehl, 
      sizeof(Befehl)-sizeof(long), 0, 0);
    if (erg == -1)
    {
      cerr << "msgrcv() fehlgeschlagen!" << endl;
      return -1;
    }
     
    switch(einBefehl.befehl)
    {
      case START:
        bewegeArm(1);
        break;
        
      case BEWEGE: 
        bewegeArm(einBefehl.arg1);
        break;
        
      case BEWEGE_OBJEKT:
        bewegeScheibe(einBefehl.arg1,
          einBefehl.arg2);
        break;
        
      case ENDE:
        istEnde = true;
        break;
    }
  }
  
  return 0;
}  
        
// --------------------------------------------------------
int main()
{
  Robot robot;

  if (robot.initialisiereVerbindung() != 0)
    return 1;
    
  cout << "Robot empfangsbereit!" << endl;
    
  robot.verarbeiteNachrichten();
  
  if (robot.kappeVerbindung() != 0)
    return 2;
    
  return 0;
}  
