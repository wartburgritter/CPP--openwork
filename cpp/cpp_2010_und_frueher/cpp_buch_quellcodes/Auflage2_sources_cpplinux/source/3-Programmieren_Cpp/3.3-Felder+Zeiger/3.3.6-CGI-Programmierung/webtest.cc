#include <iostream>
#include <fstream>
#include <cstdlib>
#include <list>
#include <pair.h>
#include <string>

using namespace std;

// ------------------------------------------------------
// Datenstrukturen
// ------------------------------------------------------
typedef list<pair<string,string> >  Liste;
typedef list<pair<string,string> >::iterator  ListenIterator;

// ------------------------------------------------------
// Funktionsprototypen
// ------------------------------------------------------
bool analysiereAnfrage(Liste& _liste);
void antworte(Liste& _liste);
void antwortFehler();
void konvertiereLeerzeichen(string& _s);

// ------------------------------------------------------
// Hauptfunktion
// ------------------------------------------------------
int main()
{
  Liste liste;
  
  if (analysiereAnfrage(liste) == false)
    antwortFehler();
  else
    antworte(liste);

  return 0;
}
  
// ------------------------------------------------------
// Auswertung und Speicherung der Anfrage
// ------------------------------------------------------
bool analysiereAnfrage(Liste& _liste)
{
  // Puffer fuer uebergebene Daten
  char* buffer = 0;
  unsigned int len;
  
  // Bestimme die Anforderungsart
  string request_method = getenv("REQUEST_METHOD");
  
  // Behandle eine POST-Anforderung
  if (request_method == "POST")
  {
    len = atoi(getenv("CONTENT_LENGTH"));
    buffer = new char[len+1];
    
    for(unsigned int i=0; i<len; i++)
      cin.get(buffer[i]);
  }
  
  // Behandle eine GET-Anforderung
  if (request_method == "GET")
  {
    len = strlen(getenv("QUERY_STRING"));
    buffer = new char[len+1];
    strcpy(buffer,getenv("QUERY_STRING"));
  }
  
  // Null-Zeichen zur Terminierung
  buffer[len] = 0;    

  // Kopiere Puffer in String
  string eingabe = buffer;  
  delete[] buffer;
  
  // Lokale Variablen zur Teilstringsuche
  size_t pos = 0;
  size_t old_pos = 0;
  
  // Lies Schluessel/Wert-Paare
  while(pos < len)
  {
    pos = eingabe.find("&", old_pos);
    // einziges oder letztes Paar
    if (pos == string::npos)
      pos = eingabe.length();
      
    // zerlege das Paar
    string paar = eingabe.substr(old_pos, pos-old_pos);
    size_t eq_pos = paar.find("=");

    string schluessel = paar.substr(0, eq_pos);
    konvertiereLeerzeichen(schluessel);

    string wert = paar.substr(eq_pos+1);
    konvertiereLeerzeichen(wert);
    
    // fuege Paar in Liste ein
    _liste.push_back(pair<string,string>(schluessel,wert));
    old_pos = pos+1;
  }

  return true;
}
    
// ------------------------------------------------------
// Formulierung der Antwort
// ------------------------------------------------------
void antworte(Liste& _liste)
{
  cout << "Content-type: text/html" << endl << endl;
  cout << "<HTML>" << endl;
  cout << "<HEAD><TITLE>Eingabe verstanden";
  cout << "</TITLE></HEAD><BODY>" << endl;
  cout << "<H2>Sie hatten folgende Angaben gemacht: </H2>"<<endl;
  cout << "<UL>" << endl;
  
  ListenIterator li; 
  for(li = _liste.begin(); li != _liste.end(); li++)
  {
    cout << "<LI>" << li->first << ": ";
    cout << "<EM>" << li->second << "</EM></LI>" << endl;
  }
  
  cout << "</UL>" << endl;
  cout << "</BODY></HTML>" << endl;
}

// ------------------------------------------------------
// Antwort bei einem Fehler
// ------------------------------------------------------
void antwortFehler()
{
  cout << "Content-type: text/html" << endl;
  cout << "<HTML>" << endl;
  cout << "<HEAD><TITLE>Eingabe nicht verstanden";
  cout << "</TITLE></HEAD><BODY>" << endl;
  cout << "<H2>Ihre Angaben konnten nicht verarbeitet werden.</H2>"<<endl;
  cout << "</BODY></HTML>" << endl;
}  
  
void konvertiereLeerzeichen(string& _s)
{
  for(unsigned int i=0; i<_s.length(); i++)
    if (_s[i] == '+')
      _s[i] = ' ';
}      

  
