#include <iostream.h>

int main()
{
  double d[10];
  double *dp1 = d;
  double *dp2 = dp1 + 1 ;
	
  cout << "dp2 - dp1: " << dp2 - dp1 << endl;
  cout << "int(dp2) - int(dp1): " << int(dp2) - int(dp1) << endl;

  dp2 += 3;
  cout << "dp2 - dp1: " << dp2 - dp1 << endl;
  cout << "int(dp2) - int(dp1): " << int(dp2) - int(dp1) << endl;

  return 0;
}
	
