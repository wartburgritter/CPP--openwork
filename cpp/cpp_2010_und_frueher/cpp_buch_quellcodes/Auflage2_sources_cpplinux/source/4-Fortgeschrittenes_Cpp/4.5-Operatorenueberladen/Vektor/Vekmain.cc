#include <iostream>
#include <cstring>
#include "Vektor.h"
#include "Datum.hh"

using namespace std;

int main()
{
  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

 {
  // lokale konstante Referenz
  const Vektor<int>& rv = v;

  for(i=0; i<5; i++)
    cout << rv.at(i) << " ";
 }

  cout << endl;

  // Vektor von Datum
  Vektor<Datum> daten(31);

  for(i=0; i<31; i++)
    daten.at(i).setze(i+1, 12, 2000);

  for(i=23; i<31; i++)
    daten.at(i).ausgeben();

  // Vektor von Vektor
  Vektor<Vektor<double> > m(3);
  for(i=0; i<3; i++) 
    m[i].resize(3);

  for(i=0; i<3; i++)
    m.at(i).at(i) = 1;

  // Vektor von Zeichen
  Vektor<char> code(25);
  char c='a';
  for(unsigned int i=0; i<25; i++)
    code[i] = c++;
    
  char* occ;
  occ = strstr((const char*)code, "fghi");
  cout << "'fghi' kommt bei " << occ << " vor." << endl;

  return 0;
}
