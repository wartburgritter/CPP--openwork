#include<iostream.h>

int add(int x, int y)
{
  return (x+y);
} 

double add(double x, double y)
{
  return (x+y);
} 

int main()
{
  double a= 100.2;
  double b= 333.777;
  int c= 15; 
  int d= 2726;

  cout << add(a,b) << endl;
  cout << add(c,d) << endl;
  cout << add(31,'a') << endl;
//  cout << add(3.1, c);     doppeldeutig!
  cout << add(3.1, float(c)) << endl;
//  cout << add(b,c);        doppeldeutig!
}
