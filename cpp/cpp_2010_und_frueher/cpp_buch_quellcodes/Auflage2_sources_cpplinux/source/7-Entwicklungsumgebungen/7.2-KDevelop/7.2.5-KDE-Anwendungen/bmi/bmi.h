/***************************************************************************
                          bmi.h  -  description
                             -------------------
    begin                : Fre Apr 19 19:45:59 MEST 2002
    copyright            : (C) 2002 by Thomas Wieland
    email                : thomas@drwieland.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BMI_H
#define BMI_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapp.h>
#include <qwidget.h>
#include "bmidlg.h"

/** Bmi is the base class of the project */
class Bmi : public BmiMainForm
{
  Q_OBJECT 
  public:
    /** construtor */
    Bmi(QWidget* parent=0, const char *name=0);
    /** destructor */
    ~Bmi();

public:
    virtual void calcSlot();
};

#endif
