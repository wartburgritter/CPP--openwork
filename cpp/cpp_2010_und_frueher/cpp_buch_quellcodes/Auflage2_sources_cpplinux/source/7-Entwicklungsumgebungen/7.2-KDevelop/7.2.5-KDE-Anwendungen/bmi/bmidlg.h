/****************************************************************************
** Form interface generated from reading ui file './bmidlg.ui'
**
** Created: Son Mai 19 18:34:37 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef BMIMAINFORM_H
#define BMIMAINFORM_H

#include <qvariant.h>
#include <qmainwindow.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QButtonGroup;
class QGroupBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QRadioButton;
class QTextEdit;

class BmiMainForm : public QMainWindow
{ 
    Q_OBJECT

public:
    BmiMainForm( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~BmiMainForm();

    QGroupBox* GroupBox2;
    QTextEdit* teTextbox;
    QLabel* lBmi;
    QPushButton* pbEval;
    QGroupBox* GroupBox1;
    QLabel* TextLabel3;
    QLabel* TextLabel2_2;
    QLabel* TextLabel2;
    QLineEdit* leWeight;
    QLabel* TextLabel3_2;
    QLineEdit* leHeight;
    QButtonGroup* ButtonGroup1;
    QRadioButton* rbMale;
    QRadioButton* rbFemale;


public slots:
    virtual void calcSlot();

protected:
    QGridLayout* Layout9;
};

#endif // BMIMAINFORM_H
