/***************************************************************************
                          bmi.cpp  -  description
                             -------------------
    begin                : Fre Apr 19 19:45:59 MEST 2002
    copyright            : (C) 2002 by Thomas Wieland
    email                : thomas@drwieland.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "bmi.h"
#include <qstring.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qradiobutton.h>
#include <qmessagebox.h>

Bmi::Bmi(QWidget *parent, const char *name) : BmiMainForm(parent, name)
{
}

Bmi::~Bmi()
{
}

void Bmi::calcSlot()
{
  // Wandle Groesse und Gewicht nach float um
  float height = (leHeight->text()).toFloat();
  float weight = (leWeight->text()).toFloat();

  // Pruefe Plausibilitaet der Groesse
  if (height < 50 || height > 300)
  {
    QMessageBox mb("Unzul�ssiger Wert",
      "Bitte geben Sie die Gr��e in cm an!",
      QMessageBox::Warning,
      QMessageBox::Ok | QMessageBox::Default,
      QMessageBox::NoButton,
      QMessageBox::NoButton);

    mb.exec();

    return;
  }

  // Pruefe Plausibilitaet des Gewichts
  if (weight < 20 || weight > 1000)
  {
    QMessageBox mb("Unzul�ssiger Wert",
      "Bitte geben Sie das Gewicht in kg an!",
      QMessageBox::Warning,
      QMessageBox::Ok | QMessageBox::Default,
      QMessageBox::NoButton,
      QMessageBox::NoButton);

    mb.exec();

    return;
  }

  // Berechne BMI
  height /= 100;
  float bmi = weight/(height*height);

  // Gib BMI in Label aus
  QString s;
  s.sprintf("Ihr BMI ist %.1f", bmi);
  lBmi->setText(s);

  // Stelle Hinweistext zusammen
  if (bmi < 18.5)
  {
    s = "Sie haben Untergewicht.";
  }
  else
     if (bmi < 25)
     {
       s = "Sie haben Normalgewicht. ";
       if (rbMale->isChecked())
         s += "Keinerlei Risiko.";
       else
         s += "Keine Gefahr f�r Diabetes und Herzkrankheiten, au�er bei zuviel Fett.";
     }
     else
       if (bmi < 30)
       {
         s = "Sie haben �bergewicht. ";
         if (rbMale->isChecked())
           s += "Ihr Blutdruck tendiert nach oben, das Risiko f�r Diabetes steigt.";
         else
           s += "Sie haben ein deutlich h�heres Risiko, an Diabetes im Alter zu erkranken.";
       }
       else
         if (bmi < 35)
         {
           s = "Sie sind fettleibig. ";
           if (rbMale->isChecked())
           {
             s += "Sie laufen Gefahr, am Herzen zu erkranken; auch f�r Krebs und ";
             s += "Gallensteine sind Sie gef�hrdet.";
           }
           else
             s += "Sie haben ein erh�htes Gallenstein- und Herzinfarkt-Risko.";
         }
         else
         {
           s = "Sie sind schwer fettleibig. ";
           if (rbMale->isChecked())
           {
             s += "Sie haben ein hohes Risiko, an Krebs, Herzinfarkt oder Arthritis ";
             s += "zu erkranken.";
           }
           else
           {
             s += "Sie haben ein hohes Risiko, an Krebs, Schlaganfall oder Diabetes ";
             s += "zu erkranken.";
           }
         }

  // Gib Hinweistext aus
  teTextbox->setText(s);

  return;
}

