/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Fre Apr 19 19:45:59 MEST 2002
    copyright            : (C) 2002 by Thomas Wieland
    email                : thomas@drwieland.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>

#include "bmi.h"

static const char *description =
	I18N_NOOP("Bmi");
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE
	
	
static KCmdLineOptions options[] =
{
  { 0, 0, 0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "bmi", I18N_NOOP("Bmi"),
    VERSION, description, KAboutData::License_GPL,
    "(c) 2002, Thomas Wieland", 0, 0, "thomas@drwieland.de");
  aboutData.addAuthor("Thomas Wieland",0, "thomas@drwieland.de");
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication a;
  Bmi *bmi = new Bmi();
  a.setMainWidget(bmi);
  bmi->show();  

  return a.exec();
}
