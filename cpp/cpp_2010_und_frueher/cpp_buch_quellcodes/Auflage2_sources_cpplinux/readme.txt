             
            Thomas Wieland 
            C++-Entwicklung mit Linux 

            Eine Einf�hrung in die Sprache und die wichtigsten Werkzeuge - von 
            GCC und XEmacs bis KDevelop 

            Dieses Archiv enth�lt alle Beispiele, die im Buch besprochen werden. 
	    Diese Informationen finden Sie in folgenden Unterverzeichnissen: 

              source, das die Beispieldateien getrennt nach Kapiteln umfasst. 
              Alle Beispiele lassen sich entweder direkt zu einer ausf�hrbaren 
              Datei �bersetzen oder �ber das beiliegende Makefile. Das Beispiel 
              zu Kapitel 7 enth�lt zus�tzlich Projekt-Dateien f�r die Verwendung 
              mit KDevelop.    

            Die Verwendung der Programme erfolgt unter Ausschluss jeglicher Haftung 
            und Garantie. Insbesondere schlie�en wir jegliche Haftung fuer 
            Schaeden aus, die auf Grund der Benutzung der im Archiv 
            enthaltenen Programme entstehen. 
            Die in diesem Archvi enthaltenen Beispieldateien sind freie 
            Software. Sie k�nnen sie weitergeben und ver�ndern unter den 
            Bedingungen der GNU General Public License, wie sie von der Free 
            Software Foundation ver�ffentlicht wurde. Eine Kopie der 
            Lizenzbestimmungen finden Sie im gleichen Verzeichnis wie diese 
            Datei. 
             
            Autor: 
            Thomas Wieland arbeitete nach seinem Studium der Mathematik, Physik 
            und Informatik als Softwareingenieur beim Deutschen Zentrum f�r Luft- 
            und Raumfahrt (DLR) in Oberpfaffenhofen. Anschlie�end leitete er bei 
            der Zentralabteilung Corporate Technology der Siemens AG in M�nchen die 
            Vorfeldentwicklung auf dem Gebiet der Softwarearchitekturen f�r mobile 
            und selbstkonfigurierende Anwendungen. Seit dem Wintersemester 2002/03 ist 
            Dr. Wieland als Professor f�r Informatik an der Fachhochschule Coburg 
            t�tig. Zudem war er zeitweise Chefredakteur der Zeitschrift "Linux 
            Enterprise".
