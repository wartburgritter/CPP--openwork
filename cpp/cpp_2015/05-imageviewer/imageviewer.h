#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;

class ImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    ImageViewer();

private slots:
    void open();
    void aboutViewer();

private:
    void createActions();
    void createMenus();
    void adjustScrollBar(QScrollBar *scrollBar);

    QLabel *imageLabel;
    QScrollArea *scrollArea;

    QAction *exitAct;
    QAction *aboutViewerAct;
    QAction *aboutQtAct;
    QAction *openAct;


    QMenu *programmMenu;
    QMenu *fileMenu;
};

#endif