/********************************************************************************
** Form generated from reading UI file 'FileDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILEDIALOG_H
#define UI_FILEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FileDialogClass
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton;
    QListWidget *listWidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *FileDialogClass)
    {
        if (FileDialogClass->objectName().isEmpty())
            FileDialogClass->setObjectName(QString::fromUtf8("FileDialogClass"));
        FileDialogClass->resize(348, 513);
        centralwidget = new QWidget(FileDialogClass);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(25, 25, 301, 50));
        listWidget = new QListWidget(centralwidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(25, 100, 301, 371));
        FileDialogClass->setCentralWidget(centralwidget);
        menubar = new QMenuBar(FileDialogClass);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 348, 22));
        FileDialogClass->setMenuBar(menubar);
        statusbar = new QStatusBar(FileDialogClass);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        FileDialogClass->setStatusBar(statusbar);

        retranslateUi(FileDialogClass);

        QMetaObject::connectSlotsByName(FileDialogClass);
    } // setupUi

    void retranslateUi(QMainWindow *FileDialogClass)
    {
        FileDialogClass->setWindowTitle(QApplication::translate("FileDialogClass", "MainWindow", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("FileDialogClass", "Change Directory", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FileDialogClass: public Ui_FileDialogClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILEDIALOG_H
