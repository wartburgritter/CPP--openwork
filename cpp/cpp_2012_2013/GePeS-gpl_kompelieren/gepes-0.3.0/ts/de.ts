<!DOCTYPE TS>
<TS version="2.0" language="de">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <source>GPS and compass based tools</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <source>Website:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open website</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CompassPage</name>
    <message>
        <source>Azimuth:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>accuracy</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GPSBase</name>
    <message>
        <source>Position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Latitude:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Longitude:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Altitude:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GPSPage</name>
    <message>
        <source>Accuracy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Vertical:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Horizontal:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Source:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Tools</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sun</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Moon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Compass</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>GPS details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MoonPage</name>
    <message>
        <source>Moon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Moon phase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>New moon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Waxing crescent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>First quarter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Waxing gibbous</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Full moon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Waning gibbous</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Last quarter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Waning crescent </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Dark moon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Sensors</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Refresh frequency:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>auto</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Appearance</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SunCompassPage</name>
    <message>
        <source>Sun azimuth:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Azimuth:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>accuracy</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SunPage</name>
    <message>
        <source>Sun</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sun rise:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Solar noon:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sun set:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Azimuth</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sun position:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>
