/* Funktionen und Methoden ab S.70*/
#include <iostream>
using namespace std;

/* Definition der Funktion (Methode) add
-----------------------------------------
   Typ des Rueckgabewertes: int
   Funktionsname          : add
   Argumentenliste        : ( int x, int y)
   eigentliche Funktion   : innerhalb des Blocks {...}
   Rueckgabewert          : z     */

int add( int x, int y)
{
 int z=x+y;
 return z;
}

int sub( int x, int y)
{
 int z=x-y;
 return z;
}

// Bsp f�r Methode mit Rueckgabewert void => Angabe des Rueckgabewertes nicht noetig
void ausgabe( int z)
{
 cout<<"Das Ergebnis ist "<<z<<endl;
}


// Hauptprogramm
//---------------

int main()
{
 int a=5;
 int b=12;
 cout<<"a="<< a
     <<", b="<< b <<endl;

 int c=add(a,b);
 cout<<"Methode add: c=a+b, c="
     << c <<endl;

 int d=sub(b,a);
 cout<<"Methode sub: d=b-a, d="
     << d <<endl;

  cout<<"Geschachtelte Methoden: c+d= ";
  ausgabe(add(c,d));

  cout<<"Noch mehr Schachtelzeug: c-(a+d)= ";
  ausgabe(sub(c,add(a,d)));

// cout << "Geschachtelte Subtraktion: c-b= " << Ausgabe(sub(b,c)) << endl;

 return 0;  // R�ckgabewert der mainfunktion
}


/*Kompileraufruf
g++ -Wall -o funktionen_bhb funktionen_bhb.cc
*/

