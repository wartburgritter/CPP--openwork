/* Beispiel Referenzen ab S. 80*/
#include <iostream>
using namespace std;

int main()
{
 int height = 800;  // Variable height
 int& h = height;   // Referenz  h auf Speicherstelle von height

 h += 100;                             // h = 900

 cout << " Höhe: " << height++;        // Inhalt Speicherstelle h+1
 cout << " Höhe: " << h++;             // h+1
 cout << " Höhe: " << height << endl;  // height=h=902
}

/*Kompileraufruf
g++ -Wall -o referenzen_bhb referenzen_bhb.cc
*/

