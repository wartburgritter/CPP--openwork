/* Beispiel Referenzen ab S. 80*/
#include <iostream>
using namespace std;

int main()
{
 int height = 800;  // Variable
 int& h = height; // Referenz  h auf Speicherstelle von height

 h += 100; // h = h + 100

 cout << " Höhe: " << height++;
 cout << " Höhe: " << h++;
 cout << " Höhe: " << height << endl;
}

/*Kompileraufruf
g++ -Wall -o referenzen_bhb referenzen_bhb.cc
*/

