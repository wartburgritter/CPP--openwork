 /*Das erste Porgramm:
  Summe der Zahlen von 1 bis 10 (S.36)
*/
using namespace std;
#include <iostream>

int main(void)
{
  // Variable deklarieren und initialisieren
   long zahl;
   zahl = 0;

   // Schleife durchlaufen
   for (long i = 1; i <= 10000; i++)
   {
     zahl += i;   //bedeutet zahl = zahl + i
     cout << "Summe bis " << i << ": ";
     cout << zahl << "\n";
   }
}

/*Kompileraufruf
g++ -Wall -o summe_bhb summe_bhb.cc
*/

