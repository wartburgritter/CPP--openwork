#include <iostream>
#include "raumschiff.h"
using namespace std;


/*Definition der Klasse Raumfahrzeug
************************************/

//Standardkonstruktor
Raumfahrzeug::Raumfahrzeug()
{
  cout << "Standardkonstruktor" << endl;
}

//Destruktor
Raumfahrzeug::~Raumfahrzeug()
{
  cout << "Destruktor" << endl;
}

// Definition der Methode setGeschwindigkeit
bool Raumfahrzeug::setGeschwindigkeit(unsigned long _tempo)
 {
  cout << "An Methode setGeschwindigkeit wurde der Wert "
       << _tempo
       << " übergeben." << endl;

  geschw=_tempo;
  
  cout << "Die Methode setGeschwindigkeit hat der Variablen geschw den Wert "
       << geschw
       << " zugewiesen." << endl;

  return true;
 }

// Definition der Methode getGeschwindigkeit
unsigned long Raumfahrzeug::getGeschwindigkeit()
 {
  return geschw;
 }
