#include <iostream>
#include "raumschiff.h"
using namespace std;


/*Definition der Klasse Raumfahrzeug
************************************/

//Kontstruktor
Raumfahrzeug::Raumfahrzeug()
{
  cout << "Contructor!!!" << endl;
//  hoechstgeschw=297000;
}

//Destruktor
Raumfahrzeug::~Raumfahrzeug()
{       // zu Testzwecken: mache hier eine Ausgabe, damit man den Aufruf sehen kann
        cout << "Destructor!!!" << endl << endl;
}

// Definition der Methode setGeschwindigkeit
bool Raumfahrzeug::setGeschwindigkeit(unsigned long _tempo)
 {
  if(_tempo>hoechstgeschw)
    {
     cout << "Geschwindigkeit ist groesser als die Hoechstgeschwindigkeit." << endl;
     return false;  // funktion wird abgebrochen, und geschw bekommt einen irrsinnigen wert
    }
  geschw=_tempo;
  return true;
 }

// Definition der Methode getGeschwindigkeit
unsigned long Raumfahrzeug::getGeschwindigkeit() const
 {
  return geschw;
 }

// Definition der Methode setHoechstgeschwindigkeit
bool Raumfahrzeug::setHoechstgeschwindigkeit(unsigned long _tempo)
 {
  hoechstgeschw=_tempo;
  return true;
 }

// Definition der Methode getHoechstgeschwindigkeit
unsigned long Raumfahrzeug::getHoechstgeschwindigkeit() const
 {
  return hoechstgeschw;
 }
