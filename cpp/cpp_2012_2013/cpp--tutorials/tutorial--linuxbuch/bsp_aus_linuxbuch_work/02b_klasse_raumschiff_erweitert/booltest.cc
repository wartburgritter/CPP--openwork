/* Implementation der Klasse Raumschiff ab S. 62*/
#include <iostream>
using namespace std;



int main (void)
{
  cout << endl << "Programm booltest"<<endl;
  cout<<"------------------------------"<<endl;

  bool mybool1 = true;
  bool mybool2 = false;
  
  int a=5;
  bool is_positive = (a>0);
  
  cout <<"mybool1: "  << mybool1 << endl;
  cout <<"mybool2: "  << mybool2 << endl << endl;
  cout <<"a= "  << a << endl;
  cout <<"is_positive (a>0): "  << is_positive << endl;
  a=-5;
  is_positive = (a>0);
  cout <<"a= "  << a << endl;
  cout <<"is_positive (a>0): "  << is_positive << endl;

  return 0;
}





/*Kompileraufruf
g++ -Wall -o booltest booltest.cc
*/

