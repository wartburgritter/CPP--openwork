/* Implementation der Klasse Raumschiff ab S. 62*/
#include <iostream>
#include "raumschiff.h"
using namespace std;

//Deklaration zusaetzliche Funktionen Prototypen
void zeigeStatus(const Raumfahrzeug& rf);


int main (void)
{
  cout << endl << " Programm Klasse Raumfahrzeug"<<endl;
  cout<<"------------------------------"<<endl;
  
  Raumfahrzeug ufo;
  //Raumfahrzeug shuttle;

  cout << "Ufos Hoechstgeschwindigkeit ist "
       << ufo.getHoechstgeschwindigkeit()
       << " km/s" << endl << endl;

  ufo.setHoechstgeschwindigkeit(1000);
  cout << "Ufos Hoechstgeschwindigkeit ist "
       << ufo.getHoechstgeschwindigkeit()
       << " km/s" << endl;
 
  ufo.setGeschwindigkeit(10000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;
  ufo.setGeschwindigkeit(90000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;

 
   zeigeStatus(ufo);

//ufo.geschw = 5000; // Compilererror, da geschw privates Attribut der Klasse Raumschiff
  
  //ufo.setGeschwindigkeit(1000000); //Fehler, da grosser Hoechstgeschw.
  
  // shuttle.andocken(ufo);

  cout << endl << "...beende main...." << endl;
  return 0;
}


//Definition der Funktionen
void zeigeStatus(const Raumfahrzeug& rf)
{
  cout << endl 
       << "Geschwindigkeit: "
       << rf.getGeschwindigkeit() << endl; // erlaubt!
       
       //rf.setGeschwindigkeit(100); 
       // Compilererror, da Referenz rf vom Typ Raumfahrzeug const ist und 
       // Methode setGeschwindigkeit nicht als const definiert und deklariert ist.
       
}



/*Kompileraufruf
g++ -Wall -o klasse_raumschiff_bhb raumschiff.cc main.cc
*/

