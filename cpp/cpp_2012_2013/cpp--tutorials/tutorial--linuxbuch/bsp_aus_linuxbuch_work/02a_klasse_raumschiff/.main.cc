/* Implementation der Klasse Raumschiff ab S. 62*/
#include <iostream>
#include "raumschiff.h"
using namespace std;

int main (void)
{
  cout << endl << " Programm Klasse Raumfahrzeug"<<endl;
  cout<<"------------------------------"<<endl;
  
  Raumfahrzeug ufo;
  //Raumfahrzeug shuttle;

  ufo.setGeschwindigkeit(10000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;
  ufo.setGeschwindigkeit(90000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;

  ufo.geschw = 5000;
  
  //ufo.setGeschwindigkeit(1000000); //Fehler, da grosser Hoechstgeschw.
  
  // shuttle.andocken(ufo);

  cout << endl << "...beende main...." << endl;
  return 0;
}

/*Kompileraufruf
g++ -Wall -o klasse_raumschiff_bhb raumschiff.cc main.cc
*/

