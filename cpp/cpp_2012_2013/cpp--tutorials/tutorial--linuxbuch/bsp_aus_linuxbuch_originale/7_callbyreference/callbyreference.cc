#include<iostream>

void swap_values(int x, int y)
{
 int temp = x;
 x = y;
 y = temp;
}

void swap_refs(int& x, int& y)
{
 int temp = x;
 x = y;
 y = temp;
}


int main()
{
 int big = 10;
 int small = 2;

 cout << "big1: "<< big
      << " small1: "<< small << endl;

 swap_values (big,small);
 cout << "big2: "<< big
      << " small2: "<< small << endl;

 swap_refs (big,small);
 cout << "big3: "<< big
      << " small3: "<< small << endl;

 return 0;
}
