#include <string>
#include "birthlist.h"

class BirthFileList : public BirthList
{
  BirthFileList() : BirthList() {}

  int load(const string& filename);
};
