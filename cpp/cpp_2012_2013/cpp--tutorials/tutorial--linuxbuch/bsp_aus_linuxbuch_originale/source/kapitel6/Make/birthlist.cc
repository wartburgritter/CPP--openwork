#include "birthlist.h"
#include <fstream>
#include <iostream>

BirthList::~BirthList()
{
  while(size != 0)
    popFront();
}

void BirthList::pushBack(const string& _name,
    const string& _surname, unsigned short _day,
    unsigned short _month, unsigned short _year)
{
  BirthListElement* tmp = 
    new BirthListElement(_name, _surname, _day, _month, _year);
    
  if (last != 0)
    last->next = tmp;
  else
    first = tmp;
    
  last = tmp;
  size++;
}
    
void BirthList::popFront()
{
  if (size == 0)
    return;
    
  BirthListElement* tmp = first;
  first = tmp->next;
  if (first == 0)
    last = 0;
    
  delete tmp;
  size--;
}  

int BirthList::load(const string& filename)
{
  ifstream in(filename.c_str());

  if (in.bad())
  {
    cerr << filename << " cannot be opened!" << endl;
    return -1;
  }
  int i=1;

  while (!in.eof())
  {
    string nm, sn;
    unsigned short d=0, m=0, y=0;

    in >> nm >> sn >> d >> m >> y;

    if (in.eof())
      break;

    if (in.fail())
    {
      cerr << "Error while reading " << i << "th entry!" << endl;
      return -1;
    }

    pushBack(nm, sn, d, m, y);
    i++;
  }

  return 0;
}

bool BirthList::check(const Date& d)
{
  bool found = false;
  BirthListElement* tmp; 
  for(tmp = first; tmp != 0; tmp = tmp->next)
  {
    if (d.getDay() == tmp->date.getDay() &&
	d.getMonth() == tmp->date.getMonth())
    {
      cout << tmp->name << " " << tmp->surname 
	   << " hat am " << d << " Geburtstag und wird "
	   << d.getYear() - tmp->date.getYear() << " Jahre." << endl;
      found = true;
    }
  }

  return found;
}

  
