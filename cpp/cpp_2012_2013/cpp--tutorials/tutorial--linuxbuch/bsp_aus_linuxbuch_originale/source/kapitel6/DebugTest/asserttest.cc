// Datei asserttest.cc

#include <string>
#include <iostream>
#include <cassert>

// Funktion: repeatedOutput
// Parameter: 
//   ostream* _o: Ausgabestream
//   const string& _s: String
//   unsigned short _n: Wiederholung
// Bedingung: _o != 0
void repeatedOutput(ostream* _o, 
  const string& _s, unsigned short _n)
{
  assert(_o);  // entspricht _o!=0
  for(unsigned i=0; i<_n; i++)
    *_o << _s;
}

int main()
{
  repeatedOutput(&cout, "-", 30);
  cout << endl;
  repeatedOutput(0, "?", 30);

  return 0;
}
