#include <iostream>
#include "Vektor.h"
#include "Datum.hh"

int main()
{
  unsigned int i=0;

  // Ganzzahlvektor
  Vektor<int> v(5);

  for(i=0; i<5; i++)
    v.at(i) = i+3;

  for(i=0; i<5; i++)
    cout << v.at(i) << " ";

  cout << endl;

  // Vektor von Datum
  Vektor<Datum> daten(31);

  for(i=0; i<31; i++)
    daten.at(i).setze(i+1, 12, 2000);

  for(i=23; i<31; i++)
    daten.at(i).ausgeben();

  // Vektor von Vektor
  Vektor<Vektor<double> > m(3);
  for(i=0; i<3; i++) 
    m.resize(3);

  for(i=0; i<3; i++)
    m.at(i).at(i) = 1;

  return 0;
}
