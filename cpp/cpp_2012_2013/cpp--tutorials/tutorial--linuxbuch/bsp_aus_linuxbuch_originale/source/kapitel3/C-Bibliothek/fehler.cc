#include <iostream>
#include <cmath>
#include <cstdio>
#include <cerrno>

int main()
{
  double s = sqrt(-1.0);

  if (errno)
  {
    if (errno < sys_nerr)
      cout << "Fehler aus sys_errlist: "
	   << sys_errlist[errno]
	   << endl;

    perror("Fehler aus perror(): ");
  }

  return 0;
}
