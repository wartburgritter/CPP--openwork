#include <string>

//---------------------------------------------
// Klasse fuer Elemente der Liste
//---------------------------------------------
struct ListElement
{
  ListElement* naechstes;
  string       schluessel;
  string       wert;
  
  // Standardkonstruktor
  ListElement() :
    naechstes(0) {}
    
  // Spezieller Konstruktor
  ListElement(const string& _schluessel,
    const string& _wert) :
    naechstes(0),
    schluessel(_schluessel),
    wert(_wert) 
    {}
};

//---------------------------------------------
// Listenklasse 
//---------------------------------------------
class Liste
{
private:
  ListElement* erstes;
  ListElement* letztes;
  int anzahl;
  
public:
  Liste() :
    erstes(0), letztes(0), anzahl(0) {}
    
  virtual ~Liste();
  
  bool empty() const 
    { return (anzahl == 0); }
  
  int size() const
    { return anzahl; }
    
  void push_back(const string& schluessel,
    const string& wert);
  void pop_front();
  
  ListElement* front()
    { return erstes; }
};
  
  
    
