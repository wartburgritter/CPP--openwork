#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './bmidlg.ui'
**
** Created: Sa Dez 12 13:43:30 2009
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "bmidlg.h"

#include <qvariant.h>
#include <qgroupbox.h>
#include <qtextedit.h>
#include <qlabel.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qaction.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qtoolbar.h>

/*
 *  Constructs a BmiMainForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 */
BmiMainForm::BmiMainForm( QWidget* parent, const char* name, WFlags fl )
    : QMainWindow( parent, name, fl )
{
    (void)statusBar();
    if ( !name )
	setName( "BmiMainForm" );
    setMinimumSize( QSize( 21, 22 ) );
    setMaximumSize( QSize( 360, 385 ) );
    setBaseSize( QSize( 120, 120 ) );
    setCentralWidget( new QWidget( this, "qt_central_widget" ) );

    GroupBox2 = new QGroupBox( centralWidget(), "GroupBox2" );
    GroupBox2->setGeometry( QRect( 10, 190, 341, 170 ) );
    GroupBox2->setFrameShape( QGroupBox::Box );
    GroupBox2->setFrameShadow( QGroupBox::Sunken );

    teTextbox = new QTextEdit( GroupBox2, "teTextbox" );
    teTextbox->setGeometry( QRect( 10, 50, 321, 104 ) );
    teTextbox->setReadOnly( TRUE );

    lBmi = new QLabel( GroupBox2, "lBmi" );
    lBmi->setGeometry( QRect( 10, 20, 320, 31 ) );
    QFont lBmi_font(  lBmi->font() );
    lBmi_font.setFamily( "Helvetica" );
    lBmi_font.setPointSize( 12 );
    lBmi_font.setBold( TRUE );
    lBmi->setFont( lBmi_font ); 
    lBmi->setAlignment( int( QLabel::AlignCenter ) );

    ButtonGroup1 = new QButtonGroup( centralWidget(), "ButtonGroup1" );
    ButtonGroup1->setGeometry( QRect( 10, 0, 340, 40 ) );
    ButtonGroup1->setFrameShape( QButtonGroup::Box );
    ButtonGroup1->setExclusive( FALSE );
    ButtonGroup1->setRadioButtonExclusive( TRUE );

    rbFemale = new QRadioButton( ButtonGroup1, "rbFemale" );
    rbFemale->setGeometry( QRect( 203, 11, 106, 19 ) );
    rbFemale->setMouseTracking( TRUE );
    rbFemale->setAutoMask( TRUE );

    rbMale = new QRadioButton( ButtonGroup1, "rbMale" );
    rbMale->setGeometry( QRect( 91, 11, 106, 19 ) );
    rbMale->setMouseTracking( TRUE );
    rbMale->setChecked( TRUE );

    GroupBox1 = new QGroupBox( centralWidget(), "GroupBox1" );
    GroupBox1->setGeometry( QRect( 10, 40, 340, 100 ) );
    QFont GroupBox1_font(  GroupBox1->font() );
    GroupBox1->setFont( GroupBox1_font ); 

    QWidget* privateLayoutWidget = new QWidget( GroupBox1, "Layout9" );
    privateLayoutWidget->setGeometry( QRect( 10, 20, 270, 70 ) );
    Layout9 = new QGridLayout( privateLayoutWidget, 1, 1, 0, 6, "Layout9"); 

    TextLabel3 = new QLabel( privateLayoutWidget, "TextLabel3" );

    Layout9->addWidget( TextLabel3, 0, 4 );

    TextLabel2_2 = new QLabel( privateLayoutWidget, "TextLabel2_2" );

    Layout9->addMultiCellWidget( TextLabel2_2, 1, 1, 0, 1 );

    TextLabel2 = new QLabel( privateLayoutWidget, "TextLabel2" );

    Layout9->addWidget( TextLabel2, 0, 0 );
    Spacer3 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout9->addMultiCell( Spacer3, 0, 0, 1, 2 );

    leWeight = new QLineEdit( privateLayoutWidget, "leWeight" );
    leWeight->setAlignment( int( QLineEdit::AlignRight ) );

    Layout9->addWidget( leWeight, 1, 3 );
    Spacer3_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout9->addItem( Spacer3_2, 1, 2 );

    TextLabel3_2 = new QLabel( privateLayoutWidget, "TextLabel3_2" );

    Layout9->addWidget( TextLabel3_2, 1, 4 );

    leHeight = new QLineEdit( privateLayoutWidget, "leHeight" );
    leHeight->setAlignment( int( QLineEdit::AlignRight ) );

    Layout9->addWidget( leHeight, 0, 3 );

    pbEval = new QPushButton( centralWidget(), "pbEval" );
    pbEval->setGeometry( QRect( 100, 150, 151, 31 ) );
    pbEval->setFocusPolicy( QPushButton::TabFocus );
    pbEval->setAutoDefault( TRUE );
    pbEval->setDefault( TRUE );

    // toolbars

    languageChange();
    resize( QSize(360, 382).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( pbEval, SIGNAL( clicked() ), this, SLOT( calcSlot() ) );

    // tab order
    setTabOrder( leHeight, leWeight );
    setTabOrder( leWeight, pbEval );
    setTabOrder( pbEval, rbMale );
    setTabOrder( rbMale, rbFemale );
    setTabOrder( rbFemale, teTextbox );
}

/*
 *  Destroys the object and frees any allocated resources
 */
BmiMainForm::~BmiMainForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void BmiMainForm::languageChange()
{
    setCaption( tr2i18n( "Body-Mass-Index-Rechner" ) );
    GroupBox2->setTitle( tr2i18n( "Auswertung" ) );
    lBmi->setText( QString::null );
    ButtonGroup1->setTitle( tr2i18n( "Geschlecht" ) );
    rbFemale->setText( tr2i18n( "weiblich" ) );
    rbMale->setText( tr2i18n( "männlich" ) );
    GroupBox1->setTitle( tr2i18n( "Persönliche Angaben" ) );
    TextLabel3->setText( tr2i18n( "cm" ) );
    TextLabel2_2->setText( tr2i18n( "Gewicht:" ) );
    TextLabel2->setText( tr2i18n( "Größe:" ) );
    TextLabel3_2->setText( tr2i18n( "kg" ) );
    pbEval->setText( tr2i18n( "&Auswertung starten" ) );
    pbEval->setAccel( QKeySequence( tr2i18n( "Alt+A" ) ) );
}

void BmiMainForm::calcSlot()
{
    qWarning( "BmiMainForm::calcSlot(): Not implemented yet" );
}

#include "bmidlg.moc"
