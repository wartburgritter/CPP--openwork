/*

  Beispiel zu Kapitel 12 - Grundlegendes zum OOP

  		Fortf�hrung des Beispiels Vektor-/Matrixstruktur

*/



#include<iostream.h>

// ----------------------------------------
// | Klassendefiniion der Klasse "vektor" |
// ----------------------------------------

class vektor {

// ----- private Klassenelemente -----

	private:

		double elem[3];

// ----- �ffentliche Klassenelemente -----

	public:

		// Konstruktor (mit Default-Werten = 0.0)

		vektor(double = 0, double = 0, double = 0);

		// Destruktor

		~vektor();


		// weitere Elementfunktionen:

		int ist_zulaessig(int);	   // Index x im zulaessigen Bereich?
		double liefer(int);     	// liefert Wert der Komponente x des Vektors
      void setze(int, double);   // setzt double-Wert an Stelle x

      void ausgabe();            // Ausgabe des Vektors
};

// -----------------------------------------
// | Klassendefinition der Klasse "matrix" |
// -----------------------------------------

class matrix {

// ----- private Klassenelemente -----

	private:

		vektor elem[3]; 	// Array der Dimension 3 von Vektor-Objekten

// ----- �ffentliche Klassenelemente -----

	public:

		// Konstruktor (mit Default-Werten)

		matrix(vektor = vektor(0, 0, 0), vektor = vektor(0,0,0), vektor = vektor(0,0,0));

		// Detsruktor

		~matrix();


		// weitere Elementfunktionen 

      int ist_zulaessig(int, int);  // Index-Paar(x,y) zulaessig ?	
		double liefer(int, int);      // liefert Wert an Position (x,y)
		void setze(int, int, double); // setzte Wert(x,y) auf den Wert d

      void ausgabe();               // Ausgabe der Matrix
};

// Implementierung der Klassendeklaration

/* 
        +----------------+
	| Klasse VEKTOR: |
        +----------------+
*/ 


vektor::vektor(double d1, double d2, double d3)
{
	elem[0] = d1;
	elem[1] = d2;     	// Array-Elemente setzen
   elem[2] = d3;
};

vektor::~vektor()
{
};

int vektor::ist_zulaessig(int x)
{
	if ( (0<=x) && (x<3) ) 	// falls 0 <= x <= 2
		return 1;
	else
   	return 0;
};

double vektor::liefer(int x)
{
	if (ist_zulaessig(x) == 1)
		return elem[x];     	// R�ckgabe des Wertes an Position x
	else
	{
		cout << "Zugriff (vektor::liefer)nicht zulaessig" << endl;
      return 0;
   };
};

void vektor::setze(int x, double d)
{
	if (ist_zulaessig(x) == 1)
		elem[x] = d;			// Setze elem[x] auf den Wert d
	else
		cout << "Zugriff (vektor::setze) nicht zulaessig" << endl; 
};

void vektor::ausgabe()
{
	cout << endl << "Vektor: " << endl;

	// zeilenweise Ausgabe des Splatenvektors

	for (int i=0; i<3; i++)
   	cout << "  ( " << elem[i] << " ) " << endl;
};


/* 
        +----------------+
	| Klasse MATRIX: |
        +----------------+
*/

matrix::matrix(vektor v1, vektor v2, vektor v3)
{
	elem[0] = v1;	
	elem[1] = v2;  // Array mit Vektoren initialisieren
        elem[3] = v3;
};

 
matrix::~matrix()
{
};


int matrix::ist_zulaessig(int x, int y)
{
	// falls 0<= x <=2    und    0<= y <=2

	if ( (0<=x) && (x<3) && (0<=y) && (y<3) )
		return 1; 	
	else return 0;
};


double matrix::liefer(int x, int y)
{
	if ( ist_zulaessig(x,y) == 1 )
		return elem[y].liefer(x);
	else
       {
	  cout << " Zugriff (matrix::liefer) nicht zulaessig!" << endl; 
   	  return 0;
	};
};

void matrix::setze(int x, int y, double d)
{
	if ( ist_zulaessig(x,y) == 1 )
		elem[y].setze(x,d);
	else
   	cout << " Zugriff (matrix::setze) nicht zulaessig!" << endl;
};

void matrix::ausgabe()
{
	cout << endl << "Matrix:" << endl;

   // zeilenweise Ausgabe der Matrix

	for (int i=0; i<3; i++)
	{
		cout 	<< " ( " << liefer(i,0) << "  " << liefer(i,1) << "  "
				<< liefer(i,2) << " ) " << endl;
   };
};


/*
        +----------------+
	| HAUPTPROGRAMM: |
        +----------------+
*/


void main()
{
	vektor v1(0.6, 0.8, -1.2);
	vektor v2(1.0, 0.0, 6.4);
	vektor v3(0.21, -9.0, 0.0);

	matrix A(v1, v2, v3);

	v1.ausgabe();
	A.ausgabe();

   // ...
};
