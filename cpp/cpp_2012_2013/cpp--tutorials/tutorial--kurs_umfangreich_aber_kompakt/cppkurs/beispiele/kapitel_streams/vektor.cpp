// Implementierung der Klassendeklaration von
// 	vektor_h


#include"vektor.h"
#include<iostream.h>

vektor::vektor(double a, double b, double c)
{
	elem[0] = a;
	elem[1] = b;
	elem[2] = c;	
}


vektor::vektor(const vektor& v)
{
	for (int i = 0; i < 3; i++)
		elem[i] = v.elem[i];
}

vektor::~vektor()
{
}


double vektor::liefer(int i)
{
	if (i < 0 || i > 2) {
		cout << "Zugriff nicht moeglich!" << endl;
		return 0;
	}

	return elem[i];
}

void vektor::setze(int i, double d)
{
	if (i < 0 || i > 2) 
		cout << "Zugriff nicht moeglich!" << endl;
	else
		elem[i] = d;
}


void vektor::ausgabe()
{
	for (int i = 0; i < 3; i++)
   	cout << endl << elem[i];
}


vektor& vektor::operator-()
{
	for (int i = 0; i < 3; i++)
		elem[i] *= (-1);

   return *this;
}


vektor vektor::operator+(const vektor& b)
{
	vektor c;

	for (int i = 0; i < 3; i++)
		c.elem[i] = elem[i] + b.elem[i];

   return c;
}


vektor vektor::operator-(const vektor& b)
{
	vektor c;

	for (int i = 0; i < 3; i++)
		c.elem[i] = elem[i] - b.elem[i];

   return c;
}


vektor& vektor::operator+=(const vektor& b)
{
	for (int i = 0; i < 3; i++)
		elem[i] += b.elem[i];

	return *this;
}


vektor& vektor::operator-=(const vektor& b)
{
	for (int i = 0; i < 3; i++)
		elem[i] -= b.elem[i];

   return *this;

}


vektor vektor::operator*(double d)
{
	vektor c;

	for (int i = 0; i < 3; i++)
   	c.elem[i] = elem[i] * d;

   return c;
}


vektor& vektor::operator*=(double d)
{
	for (int i = 0; i < 3; i++)
   	elem[i] *= d;

	return *this;
}


double vektor::operator*(const vektor& b)
{
	double a = 0;

	for (int i = 0; i < 3; i++)
		a += elem[i] * b.elem[i];

   return a;
}

ostream& vektor::operator<<(ostream& strm)
{
	ausgabe();
   return strm;
}


vektor operator*(double d, const vektor& v)
{
	vektor c;

	for (int i = 0; i < 3; i++)
		c.elem[i] = d * v.elem[i];

	return c;
}
