// Definition der Klasse "matrix"

#include<iostream.h>
#include<iomanip.h>

#include"matrix.h"
#include"vektor.h"


// -------------------------------------------
// | Konstruktor (inkl. Default-Konstruktor) |
// -------------------------------------------

matrix::matrix(vektor v1, vektor v2, vektor v3)
{
	elem[0] = v1;	// Spalte 1 der Matrix = Vektor_1
	elem[1] = v2;	// Spalte 2 der Matrix = Vektor_2
   elem[2] = v3;	// Spalte 3 der Matrix = Vektor_3
}

// ---------------------
// | Copy-Konstruktor: |
// ---------------------

matrix::matrix(matrix& M)
{
	for (int i = 0; i < 3; i++)
   	elem[i] = M.elem[i];
}

// ---------------
// | Destruktor: |
// ---------------

matrix::~matrix()
{
}

// ------------------------------------------------------------------------
// | Funktion zum �berpr�fen, ob x,y Indizes des zul�ssigen Bereichs sind |
// ------------------------------------------------------------------------

int matrix::zulaessig(int x, int y)
{
	if ( (0<=x) && (x<3) && (0<=y) && (y<3) )
		return 1;
   else return 0;
}

// -------------------------------------------------
// | Funktion zum Setzen eines bestimmten Elements |
// -------------------------------------------------
 
void matrix::setze(int x,int y, double d)
{
	// x : Zeilenindex;     y : Spaltenindex

   if ( zulaessig(x,y) == 1)
		elem[y].elem[x] = d;
	else
		cout << "Setzen nicht moeglich (ausserhalb des Bereiches) !" << endl;
}


// ---------------------------------------------------------
// | Funktion zum Auslesen eines bestimmten Matrix-Element |
// ---------------------------------------------------------

double matrix::liefer(int x, int y)
{
	if (zulaessig(x,y) == 1)
		return elem[x].elem[y];
	else
	{
		cout << "Zugriff nicht moeglich (ausserhalb des Bereiches) !" << endl;
      return 0;
	}	
}


// ---------------------------------------------------
//	|	Funktion zur formatierten Ausgabe einer Matrix |
//	---------------------------------------------------

void matrix::ausgabe()
{

	// Festlegung der Ausgabenotaion:

	cout.setf(ios::scientific,  ios::floatfield);	// scientific-Notation
   cout.setf(ios::adjustfield, ios::right);        // rechtsb�ndig
	cout.setf(ios::showpoint);                      // Dezimalpunkt und ab-
   																// schlie�ende Nullen ausgeben

	cout.precision(2);			  //	Geanuigkeit der Gelitkommawerte = 2	

	// zeilenweises Ausgeben der Matrix

   cout << "Matrix: " << endl;
	for (int i = 0; i < 3; i++) {
		cout 	<< "  (  " << setw(9) << elem[0].elem[i] << "   " << setw(9) <<
				elem[1].elem[i]	<< "   "	<< setw(9) << elem[2].elem[i]
				<< "  )" << endl;
	}
}

/*
		OPERATOREN:
*/

// ----------------
// | Un�res Minus |
// ----------------

matrix& matrix::operator-()
{
	return (*this*(-1));
}


// -----------------------------------------------
// | Multiplikationsoperator ( Matrix * Matrix ) |
// -----------------------------------------------

matrix matrix::operator*(const matrix& M)
{
	matrix H;

	// Implementierung der Matrizen-Multiplikation unter Verwendung der
   // Matrix-Vektor-Multiplikation:

	for (int i = 0; i < 3; i++)
      H.elem[i] = *this * M.elem[i];

   return H;
	
}

// -----------------------------------------------
// | Multiplikationsoperator ( Matrix * Skalar ) |
// -----------------------------------------------

matrix matrix::operator*(const double d)
{
	matrix H(*this);

	for (int i = 0; i < 3; i++)      // Schleife �ber die Spalten
		for (int j = 0; j < 3; j++)	// Schleife �ber die Zeilen
      	H.elem[i].elem[j] *= d;

   return H;
}


// -----------------------------------------------
// | Multiplikationsoperator ( Matrix * Vektor ) |
// -----------------------------------------------

vektor matrix::operator*(const vektor& v)
{
	vektor h;

	for (int i = 0; i < 3; i++)
		{
      h.elem[i] = 0;
		for (int j = 0; j < 3; j++)
      	h.elem[i] += elem[j].elem[i] * v.elem[j];
		}
   return h;
}

// -----------------------------------------------------------
// | Multiplikations-/Zuweisungsoperator ( Matrix * Matrix ) |
// -----------------------------------------------------------

matrix& matrix::operator*=(const matrix& M)
{
	return (*this* M);
}



// -----------------------------------------------------------
// | Multiplikations-/Zuweisungsoperator ( Matrix * Skalar ) |
// -----------------------------------------------------------


matrix& matrix::operator*=(const double d)
{
	return (*this * d);
}


// -----------------------------------------
// | Additionsoperator ( Matrix + Matrix ) |
// -----------------------------------------

matrix matrix::operator+(const matrix& M)
{
	matrix H;

	for (int i = 0; i < 3; i++)
		H.elem[i] = elem[i] + M.elem[i];

	return H;
}


// -----------------------------------------------------
// | Additions-/Zuweisungsoperator ( Matrix + Matrix ) |
// -----------------------------------------------------

matrix& matrix::operator+=(const matrix& M)
{
	for (int i = 0; i < 3; i++)
		elem[i] += M.elem[i];

	return *this;
}


// --------------------------------------------
// | Subtraktionsoperator ( Matrix - Matrix ) |
// --------------------------------------------

matrix matrix::operator-(const matrix& M)
{
	matrix H;

	for (int i = 0; i < 3; i++)
		H.elem[i] = elem[i] - M.elem[i];

	return H;

}

// --------------------------------------------------------
// | Subtraktions-/Zuweisungsoperator ( Matrix - Matrix ) |
// --------------------------------------------------------

matrix& matrix::operator-=(const matrix& M)
{
	for (int i = 0; i < 3; i++)
		elem[i] -= M.elem[i];

	return *this;
}



/*
		FRIEND-Funktionen
*/

// ----------------------------------------------
// | Multiplkationsoperator ( Skalar + Matrix ) |
// ----------------------------------------------


matrix operator*(double d, const matrix& M)
{
	matrix H;

	for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
			H.elem[i].setze(j, M.elem[i].liefer(j) * d);

	 return H;	 
}
