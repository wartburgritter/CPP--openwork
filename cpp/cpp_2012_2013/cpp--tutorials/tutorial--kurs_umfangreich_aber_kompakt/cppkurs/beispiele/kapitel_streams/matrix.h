// Header-File zur Klasse "Matrix"

#ifndef _matrix_h
#define _matrix_h

#include"vektor.h"
#include<iostream.h>

class matrix {

	private:

		vektor elem[3];

	public:

		matrix(vektor = vektor(0), vektor = vektor(0), vektor = vektor(0));
      matrix(matrix&); 		// Copy-Konstruktor
      ~matrix();

		int zulaessig(int, int);
		void setze(int, int, double);
		double liefer(int, int);
      void ausgabe();

		matrix& operator-();	// un�res Minus

		matrix operator*(const matrix&);
		matrix operator*(const double);
      vektor operator*(const vektor&);
		matrix& operator*=(const matrix&);
		matrix& operator*=(const double);

		matrix operator+(const matrix&);
		matrix& operator+=(const matrix&);

		matrix operator-(const matrix&);
		matrix& operator-=(const matrix&);

      friend matrix operator*(double, const matrix&);
};

	// Standard-Ausgabeoperator:
	// 	- mu� global �berladen werden
	// 	- inline definiert

		inline ostream& operator<<(ostream& strm, const matrix& A)
		{
			A.ausgabe();
         return strm;
		};

#endif
