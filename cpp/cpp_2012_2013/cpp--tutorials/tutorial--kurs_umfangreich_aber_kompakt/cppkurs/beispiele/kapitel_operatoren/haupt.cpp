// Hauptporgramm zum Testen der Klasse "Vektor";

#include<iostream.h>
#include"vektor.h"

void main()
{
	vektor a;
   vektor b(1.2, 3.4, 4.5);

	a.ausgabe();
	b.ausgabe();
	(-b).ausgabe();

	cout << endl << " a + b = " <<  endl;
	(a + b).ausgabe();
	cout << endl << " 0.6 * b = "<< endl;
	(0.6 * b).ausgabe();
}
