// Hauptprogramm zum Testen der matrix-Klasse

#include<iostream.h>

#include"matrix.h"
#include"vektor.h"

void main()
{
	vektor v1(0.5, 0.5, 0.5);
	vektor v2(1, 1, 1);
	vektor v3(0.78, 0.21, -1.2324);
   vektor v4;
	matrix A(v1, v2, v3);
	matrix B(v3, v2, v1);

//	cout << v1 << v2 << v3 << endl;
//	cout << A << B ;

	cout << A << B << endl << endl;
   cout << " A * B = " << A*B << endl;
 
}
