// Header-File zur Klasse Vektor
#ifndef _vektor_h
#define _vektor_h

#include<iostream.h>

class vektor {

	friend class matrix;

	private:

		// 3x1-Vektor als Array implementiert

      double elem[3];

	public:

   	// Konstruktor und Destruktor:

		vektor(double = 0, double = 0, double = 0);
		vektor(const vektor&);		// Copy-Konstruktor

		~vektor();

		// Elementfunktionen:

		double liefer(int);
		void setze(int, double);
		void ausgabe();

		// �berladen von Operatoren

      vektor& operator-();			// un�res Minus
		vektor  operator+(const vektor&);
		vektor  operator-(const vektor&);
		vektor& operator+=(const vektor&);
		vektor& operator-=(const vektor&);

		vektor  operator*(double);
		vektor& operator*=(double);

		double operator*(const vektor&);	// SKP

		friend vektor operator*(double, const vektor&);

		ostream& operator<<(ostream&);
};

	// Standard-Ausgabeoperator:
	// 	- mu� global �berladen werden !
	//		- inline definiert
	 
inline ostream& operator<<(ostream& strm, vektor v)
{
	v.ausgabe();
	return strm;
};

#endif
