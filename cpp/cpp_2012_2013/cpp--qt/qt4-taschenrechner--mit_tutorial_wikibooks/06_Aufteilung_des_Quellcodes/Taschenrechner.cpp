//--- Taschenrechner.cpp - start ---
 
#include "Taschenrechner.h"
#include "myAdd.h"
#include "mySub.h"
 
Taschenrechner::Taschenrechner(QMainWindow *parent) : QMainWindow(parent){
      setupUi(this);
      
      myAdd *addWidget = new myAdd(this);
      mySub *subWidget = new mySub(this);

      //stackedWidget -> setCurrentIndex(0);
      InputA -> setText("0");
      InputA -> setAlignment(Qt::AlignRight);
      InputB -> setText("0");
      InputB -> setAlignment(Qt::AlignRight);
      ResultC -> setAlignment(Qt::AlignRight);
      InputA_2 -> setText("0");
      InputA_2 -> setAlignment(Qt::AlignRight);
      InputB_2 -> setText("0");
      InputB_2 -> setAlignment(Qt::AlignRight);
      ResultC_2 -> setAlignment(Qt::AlignRight);
      
      connect(Calculate, SIGNAL(clicked()), addWidget, SLOT(addAB()));
      connect(Calculate_2, SIGNAL(clicked()), subWidget, SLOT(subAB()));
      
      connect(actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
      connect(actionAdd, SIGNAL(triggered()), this, SLOT(showAdd()));
      connect(actionSub, SIGNAL(triggered()), this, SLOT(showSub()));     
 }
 
Taschenrechner::~Taschenrechner(){
}
 
void Taschenrechner::showAdd() {
   stackedWidget -> setCurrentIndex(0);
}

void Taschenrechner::showSub() {
   stackedWidget -> setCurrentIndex(1);
}


//--- Taschenrechner.cpp - end ---
