//--- mySub.cpp - start ---
 

#include "mySub.h"
#include "Taschenrechner.h"

mySub::mySub(Taschenrechner *TRechner):myTaschenrechner(TRechner){
}

mySub::~mySub(){
}

void mySub::subAB() {
   double a, b;
   a = (myTaschenrechner -> InputA_2 -> text()).toDouble();
   b = (myTaschenrechner -> InputB_2 -> text()).toDouble();
   myTaschenrechner -> ResultC_2 -> setText(QString("%1").arg(a-b,0,'f',0));
}

 
//--- mySub.cpp - end ---
