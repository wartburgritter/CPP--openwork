//--- myAdd.cpp - start ---
 

#include "myAdd.h"
#include "Taschenrechner.h"

myAdd::myAdd(Taschenrechner *TRechner):myTaschenrechner(TRechner){
}

myAdd::~myAdd(){
}

void myAdd::addAB() {
   double a, b;
   a = (myTaschenrechner -> InputA -> text()).toDouble();
   b = (myTaschenrechner -> InputB -> text()).toDouble();
   myTaschenrechner -> ResultC -> setText(QString("%1").arg(a+b,0,'f',0));
}

 
//--- myAdd.cpp - end ---
