//--- Taschenrechner.cpp - start ---
 
#include "Taschenrechner.h"
 
Taschenrechner::Taschenrechner(QMainWindow *parent) : QMainWindow(parent){
      setupUi(this);
      InputA -> setText("0");
      InputA -> setAlignment(Qt::AlignRight);
      InputB -> setText("0");
      InputB -> setAlignment(Qt::AlignRight);
      ResultC -> setAlignment(Qt::AlignRight);
 }
 
Taschenrechner::~Taschenrechner(){
}
 
//--- Taschenrechner.cpp - end ---
