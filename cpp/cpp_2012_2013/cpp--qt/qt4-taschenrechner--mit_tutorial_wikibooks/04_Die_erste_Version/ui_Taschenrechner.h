/********************************************************************************
** Form generated from reading UI file 'Taschenrechner.ui'
**
** Created: Wed Sep 8 07:58:17 2010
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASCHENRECHNER_H
#define UI_TASCHENRECHNER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *Calculate;
    QLineEdit *InputA;
    QLineEdit *InputB;
    QLineEdit *ResultC;
    QLabel *LabelA;
    QLabel *LabelB;
    QLabel *LabelC;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(277, 253);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        Calculate = new QPushButton(centralwidget);
        Calculate->setObjectName(QString::fromUtf8("Calculate"));
        Calculate->setGeometry(QRect(25, 175, 226, 51));
        QFont font;
        font.setPointSize(12);
        Calculate->setFont(font);
        InputA = new QLineEdit(centralwidget);
        InputA->setObjectName(QString::fromUtf8("InputA"));
        InputA->setGeometry(QRect(125, 25, 126, 25));
        InputB = new QLineEdit(centralwidget);
        InputB->setObjectName(QString::fromUtf8("InputB"));
        InputB->setGeometry(QRect(125, 75, 126, 25));
        ResultC = new QLineEdit(centralwidget);
        ResultC->setObjectName(QString::fromUtf8("ResultC"));
        ResultC->setGeometry(QRect(125, 125, 126, 26));
        LabelA = new QLabel(centralwidget);
        LabelA->setObjectName(QString::fromUtf8("LabelA"));
        LabelA->setGeometry(QRect(25, 25, 51, 26));
        LabelA->setFont(font);
        LabelB = new QLabel(centralwidget);
        LabelB->setObjectName(QString::fromUtf8("LabelB"));
        LabelB->setGeometry(QRect(25, 75, 51, 26));
        LabelB->setFont(font);
        LabelC = new QLabel(centralwidget);
        LabelC->setObjectName(QString::fromUtf8("LabelC"));
        LabelC->setGeometry(QRect(25, 125, 51, 26));
        LabelC->setFont(font);
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        Calculate->setText(QApplication::translate("MainWindow", "Calculate", 0, QApplication::UnicodeUTF8));
        LabelA->setText(QApplication::translate("MainWindow", "A", 0, QApplication::UnicodeUTF8));
        LabelB->setText(QApplication::translate("MainWindow", "B", 0, QApplication::UnicodeUTF8));
        LabelC->setText(QApplication::translate("MainWindow", "A+B", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASCHENRECHNER_H
