//--- Taschenrechner.cpp - start ---
 
#include "Taschenrechner.h"
 
Taschenrechner::Taschenrechner(QMainWindow *parent) : QMainWindow(parent){
      setupUi(this);
      stackedWidget -> setCurrentIndex(0);
      InputA -> setText("0");
      InputA -> setAlignment(Qt::AlignRight);
      InputB -> setText("0");
      InputB -> setAlignment(Qt::AlignRight);
      ResultC -> setAlignment(Qt::AlignRight);
      InputA_2 -> setText("0");
      InputA_2 -> setAlignment(Qt::AlignRight);
      InputB_2 -> setText("0");
      InputB_2 -> setAlignment(Qt::AlignRight);
      ResultC_2 -> setAlignment(Qt::AlignRight);
      
      connect(Calculate, SIGNAL(clicked()), this, SLOT(addAB()));
      connect(Calculate_2, SIGNAL(clicked()), this, SLOT(subAB()));
      
      connect(actionQuit, SIGNAL(triggered()), this, SLOT(slotClose()));
      connect(actionAdd, SIGNAL(triggered()), this, SLOT(showAdd()));
      connect(actionSub, SIGNAL(triggered()), this, SLOT(showSub()));     
 }
 
Taschenrechner::~Taschenrechner(){
}
 
void Taschenrechner::addAB() {
   double a, b;
   a = (InputA -> text()).toDouble();
   b = (InputB -> text()).toDouble();
   ResultC -> setText(QString("%1").arg(a+b,0,'f',0));
}

void Taschenrechner::subAB() {
   double a, b;
   a = (InputA_2 -> text()).toDouble();
   b = (InputB_2 -> text()).toDouble();
   ResultC_2 -> setText(QString("%1").arg(a-b,0,'f',0));
}

void Taschenrechner::slotClose() {
   close();
}

void Taschenrechner::showAdd() {
   stackedWidget -> setCurrentIndex(0);
}

void Taschenrechner::showSub() {
   stackedWidget -> setCurrentIndex(1);
}


//--- Taschenrechner.cpp - end ---
