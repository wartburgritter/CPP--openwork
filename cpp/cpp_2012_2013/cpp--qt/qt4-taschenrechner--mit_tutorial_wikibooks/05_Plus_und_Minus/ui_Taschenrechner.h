/********************************************************************************
** Form generated from reading UI file 'Taschenrechner.ui'
**
** Created: Thu Sep 9 19:36:28 2010
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASCHENRECHNER_H
#define UI_TASCHENRECHNER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionQuit;
    QAction *actionAdd;
    QAction *actionSub;
    QWidget *centralwidget;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QPushButton *Calculate;
    QLabel *LabelA;
    QLabel *LabelC;
    QLineEdit *InputA;
    QLineEdit *InputB;
    QLineEdit *ResultC;
    QLabel *LabelB;
    QLabel *LabelD;
    QWidget *page_2;
    QLineEdit *InputB_2;
    QLabel *LabelA_2;
    QLineEdit *InputA_2;
    QLineEdit *ResultC_2;
    QLabel *LabelB_2;
    QLabel *LabelC_2;
    QPushButton *Calculate_2;
    QLabel *LabelD_2;
    QMenuBar *menuBar;
    QMenu *menuChoose;
    QMenu *menuTaschenrechner;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(279, 321);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionAdd = new QAction(MainWindow);
        actionAdd->setObjectName(QString::fromUtf8("actionAdd"));
        actionSub = new QAction(MainWindow);
        actionSub->setObjectName(QString::fromUtf8("actionSub"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 25, 276, 276));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        Calculate = new QPushButton(page);
        Calculate->setObjectName(QString::fromUtf8("Calculate"));
        Calculate->setGeometry(QRect(25, 200, 226, 51));
        QFont font;
        font.setPointSize(12);
        Calculate->setFont(font);
        LabelA = new QLabel(page);
        LabelA->setObjectName(QString::fromUtf8("LabelA"));
        LabelA->setGeometry(QRect(25, 50, 51, 26));
        LabelA->setFont(font);
        LabelC = new QLabel(page);
        LabelC->setObjectName(QString::fromUtf8("LabelC"));
        LabelC->setGeometry(QRect(25, 150, 51, 26));
        LabelC->setFont(font);
        InputA = new QLineEdit(page);
        InputA->setObjectName(QString::fromUtf8("InputA"));
        InputA->setGeometry(QRect(125, 50, 126, 25));
        InputB = new QLineEdit(page);
        InputB->setObjectName(QString::fromUtf8("InputB"));
        InputB->setGeometry(QRect(125, 100, 126, 25));
        ResultC = new QLineEdit(page);
        ResultC->setObjectName(QString::fromUtf8("ResultC"));
        ResultC->setGeometry(QRect(125, 150, 126, 26));
        LabelB = new QLabel(page);
        LabelB->setObjectName(QString::fromUtf8("LabelB"));
        LabelB->setGeometry(QRect(25, 100, 51, 26));
        LabelB->setFont(font);
        LabelD = new QLabel(page);
        LabelD->setObjectName(QString::fromUtf8("LabelD"));
        LabelD->setGeometry(QRect(25, 0, 101, 26));
        LabelD->setFont(font);
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        InputB_2 = new QLineEdit(page_2);
        InputB_2->setObjectName(QString::fromUtf8("InputB_2"));
        InputB_2->setGeometry(QRect(125, 100, 126, 25));
        LabelA_2 = new QLabel(page_2);
        LabelA_2->setObjectName(QString::fromUtf8("LabelA_2"));
        LabelA_2->setGeometry(QRect(25, 50, 51, 26));
        LabelA_2->setFont(font);
        InputA_2 = new QLineEdit(page_2);
        InputA_2->setObjectName(QString::fromUtf8("InputA_2"));
        InputA_2->setGeometry(QRect(125, 50, 126, 25));
        ResultC_2 = new QLineEdit(page_2);
        ResultC_2->setObjectName(QString::fromUtf8("ResultC_2"));
        ResultC_2->setGeometry(QRect(125, 150, 126, 26));
        LabelB_2 = new QLabel(page_2);
        LabelB_2->setObjectName(QString::fromUtf8("LabelB_2"));
        LabelB_2->setGeometry(QRect(25, 100, 51, 26));
        LabelB_2->setFont(font);
        LabelC_2 = new QLabel(page_2);
        LabelC_2->setObjectName(QString::fromUtf8("LabelC_2"));
        LabelC_2->setGeometry(QRect(25, 150, 51, 26));
        LabelC_2->setFont(font);
        Calculate_2 = new QPushButton(page_2);
        Calculate_2->setObjectName(QString::fromUtf8("Calculate_2"));
        Calculate_2->setGeometry(QRect(25, 200, 226, 51));
        Calculate_2->setFont(font);
        LabelD_2 = new QLabel(page_2);
        LabelD_2->setObjectName(QString::fromUtf8("LabelD_2"));
        LabelD_2->setGeometry(QRect(25, 0, 101, 26));
        LabelD_2->setFont(font);
        stackedWidget->addWidget(page_2);
        MainWindow->setCentralWidget(centralwidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 279, 21));
        menuChoose = new QMenu(menuBar);
        menuChoose->setObjectName(QString::fromUtf8("menuChoose"));
        menuTaschenrechner = new QMenu(menuBar);
        menuTaschenrechner->setObjectName(QString::fromUtf8("menuTaschenrechner"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuTaschenrechner->menuAction());
        menuBar->addAction(menuChoose->menuAction());
        menuChoose->addAction(actionAdd);
        menuChoose->addAction(actionSub);
        menuTaschenrechner->addAction(actionQuit);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionQuit->setText(QApplication::translate("MainWindow", "Beenden", 0, QApplication::UnicodeUTF8));
        actionAdd->setText(QApplication::translate("MainWindow", "Addition", 0, QApplication::UnicodeUTF8));
        actionSub->setText(QApplication::translate("MainWindow", "Subtraktion", 0, QApplication::UnicodeUTF8));
        Calculate->setText(QApplication::translate("MainWindow", "Calculate", 0, QApplication::UnicodeUTF8));
        LabelA->setText(QApplication::translate("MainWindow", "A", 0, QApplication::UnicodeUTF8));
        LabelC->setText(QApplication::translate("MainWindow", "A+B", 0, QApplication::UnicodeUTF8));
        LabelB->setText(QApplication::translate("MainWindow", "B", 0, QApplication::UnicodeUTF8));
        LabelD->setText(QApplication::translate("MainWindow", "Addition", 0, QApplication::UnicodeUTF8));
        LabelA_2->setText(QApplication::translate("MainWindow", "A", 0, QApplication::UnicodeUTF8));
        LabelB_2->setText(QApplication::translate("MainWindow", "B", 0, QApplication::UnicodeUTF8));
        LabelC_2->setText(QApplication::translate("MainWindow", "A-B", 0, QApplication::UnicodeUTF8));
        Calculate_2->setText(QApplication::translate("MainWindow", "Calculate", 0, QApplication::UnicodeUTF8));
        LabelD_2->setText(QApplication::translate("MainWindow", "Subtraktion", 0, QApplication::UnicodeUTF8));
        menuChoose->setTitle(QApplication::translate("MainWindow", "Choose", 0, QApplication::UnicodeUTF8));
        menuTaschenrechner->setTitle(QApplication::translate("MainWindow", "Taschenrechner", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASCHENRECHNER_H
