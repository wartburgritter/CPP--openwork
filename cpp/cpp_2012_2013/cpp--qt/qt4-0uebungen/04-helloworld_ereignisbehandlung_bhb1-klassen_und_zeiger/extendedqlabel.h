#ifndef EXTENDEDQLABEL_H
#define EXTENDEDQLABEL_H


#include <QLabel>

class ExtendedQLabel : public QLabel
{

  Q_OBJECT

public:
  ExtendedQLabel(const QString &text, QWidget *parent =0);   //Konstruktor

protected:
  virtual void mousePressEvent(QMouseEvent *event);
  //Einzige neue (respective umprogrammierte, das sie virtual ist) Methode der Klasse ExtendedQLabel.
  //Alle anderen Methoden werden geerbt von QLabel.
};


#endif