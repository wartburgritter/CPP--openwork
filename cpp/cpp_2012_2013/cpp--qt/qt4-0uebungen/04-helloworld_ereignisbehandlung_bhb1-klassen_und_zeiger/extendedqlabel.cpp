#include <QtGui>
#include <math.h>

#include "extendedqlabel.h"


//Implementation der Klasse ExtendedQLabel
//Konstruktor
ExtendedQLabel::ExtendedQLabel(const QString &text, QWidget *parent)
  :QLabel(text, parent)
{}



//virtuelle Methode mousePressEvent
//Das heisst wir ordnen der Methode mousePressEvent der Basisklasse QLabel in unserer
//Klasse ExtendedQLabel ein neues Verhalten zu. Dies ist wohl Polymorphie
//Die Parameterliste und der Rueckgabetyp der Methode in der abgeleiteten Klasse muss zu denen 
//der Methode in der Basisklasse passen.
void ExtendedQLabel::mousePressEvent(QMouseEvent *event)
{
  event->accept();    // aufruf von accept() signalisiert, dass ereignis behandelt wurde, daher kein mehrfaches aufrufen 
  QPalette myPalette = this->palette();
  QPoint pos = event->pos();
  // Orginal
  //int r = 255 * pos.x() / width();
  //int g = 255 * pos.y() / height();
  //int b = 100;
  //Graupalette von links nach rechts
  /*int r = 255 * pos.x() / width();
  int g = 255 * pos.x() / width();
  int b = 255 * pos.x() / width();
  */
  // abhaenging von Quatranten, Variablen muessen ausserhalb der if-anweisung 
  // deklariert werden, sonst sind sie nur innerhalb dieser sichtbar
  int r=255;  //mit weiss initialisiert alles 255, tritt ein im vierten quatranten
  int g=255;
  int b=255;
  if (pos.x() < width() * 0.5  &&  pos.y() < height() * 0.5 ) {
    r = 200; 
    g = 10; 
    b = 10;
  }
  else if (pos.x() < width() * 0.5  &&  pos.y() > height() * 0.5 ) {
    r = 10;
    g = 200;
    b = 10;  
  }
  else if (pos.x() > width() * 0.5  &&  pos.y() < height() * 0.5 ) {
    r = 10;
    g = 10;
    b = 200;  
  }
  myPalette.setColor(QPalette::Foreground, QColor(r,g,b));
  setPalette(myPalette);
  update();
}

