#include <QtGui>

#include "helloworld.h"
#include "extendedqlabel.h"


HelloWorld::HelloWorld()       //Konstruktor
{

    
    //helloLabel = new QLabel("Hello World");
    
    
    // anstatt des normalel QLabels erstellen wir ein ExtendedQLabel
    // dies haben wir selber als Erweitertung von QLabel implementiert
    // siehe die dateien extendedqlabel.h und extendedqlabel.cpp
    // was genau bei klick auf das label passiert wird ausschliesslich 
    // dort implementiert. Diese klasse bzw label könnte nun in anderen programmen
    // sofort angewendet werden.
    helloLabel = new ExtendedQLabel("Hello World");
    helloLabel->setFont(QFont("Arial", 100));

    
    quitButton = new QPushButton("Close");
    quitButton->setFont(QFont("Arial", 30, QFont::Black));
    
        
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(helloLabel);
    mainLayout->addWidget(quitButton);
    setLayout(mainLayout);
    
    QObject::connect (quitButton, SIGNAL(clicked()), this, SLOT(close()));
    

}