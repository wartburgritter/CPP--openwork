#include <QtGui>

#include "imageviewer.h"

ImageViewer::ImageViewer()   //Konstruktor
{
    imageLabel = new QLabel;
    // Instanz imageLabel des Typs QLabel wurde als Zeiger deklariert
    // daher in folgender Zeile -> als Auswahloperator und nicht wie bei objekten direkt der .
    // farbe mittels der klasse QPalette setzen nur das linke obere kleine kaestchen, der rest ist mit scrollArea
    // ich checke es noch nicht, der versuche eine farbe direkt zu setzen schlaegt fehl
    imageLabel->setBackgroundRole(QPalette::Base);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    
    // imageLabel wird Kindobjekt von scrollArea
    scrollArea->setWidget(imageLabel);
    
    // scrollArea wird zentraler bereich des widgets
    setCentralWidget(scrollArea);

    //menues werden in das widget eingebunden, implementation der menues weiter unten
    createActions();
    createMenus();

    setWindowTitle(tr("Hugos Viewer"));
    resize(500, 400);
}

void ImageViewer::open()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"), QDir::currentPath());
    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Hugos Viewer"),tr("Cannot load %1.").arg(fileName));
            return;
        }
        imageLabel->setPixmap(QPixmap::fromImage(image));
        imageLabel->adjustSize();
        }
} 

void ImageViewer::aboutViewer()
    {
        QMessageBox::about(this, tr("Hugos Viewer"),
                tr("<p>The <b>Image Viewer</b> example shows how to combine QLabel "
                   "and QScrollArea to display an image. QLabel is typically used "
                   "for displaying a text, but it can also display an image. "
                   "QScrollArea provides a scrolling view around another widget. "
                   "If the child widget exceeds the size of the frame, QScrollArea "
                   "automatically provides scroll bars. </p><p>The example "
                   "demonstrates how QLabel's ability to scale its contents "
                   "(QLabel::scaledContents), and QScrollArea's ability to "
                   "automatically resize its contents "
                   "(QScrollArea::widgetResizable), can be used to implement "
                   "zooming and scaling features. </p><p>In addition the example "
                   "shows how to use QPainter to print an image.</p> <p>Natürlich "
                   "habe ich auch einige Anpassungen unternommen. Es sind einige "
                   "Funktionen des Qt-Image-Viewer hinzugekommen. Die Grundlage war "
                   "der Image-Viewer von Pro-Linux. Da es mein eigenes Programm ist, "
                   "heisst es auch Hugos Viewer</p>"));
    }



void ImageViewer::createActions()
{
    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcut(tr("Ctrl+O"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    exitAct = new QAction(tr("C&lose"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    
    aboutViewerAct = new QAction(tr("About &Hugos Viewer"), this);
    connect(aboutViewerAct, SIGNAL(triggered()), this, SLOT(aboutViewer()));        
    
    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void ImageViewer::createMenus()
{
    programmMenu = new QMenu(tr("&Hugos Viewer"),this);
    programmMenu->addAction(aboutViewerAct);
    programmMenu->addAction(aboutQtAct);
    programmMenu->addSeparator();
    programmMenu->addAction(exitAct);

    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(openAct);
    //fileMenu->addSeparator();

    menuBar()->addMenu(programmMenu);
    menuBar()->addMenu(fileMenu);
}