#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>

class QLabel;
class QPushButton;


class HelloWorld : public QWidget
{
    Q_OBJECT

public:
    HelloWorld();

private:
    QLabel *helloLabel;
    QPushButton *quitButton;
    QPushButton *ohneButton;
    QLabel *tschauLabel;
    
};

#endif