#include <QtGui>

#include "helloworld.h"

HelloWorld::HelloWorld()       //Konstruktor
{

    helloLabel = new QLabel("Hello Hugo!");
    helloLabel->setFont(QFont("Arial", 20, QFont::Bold));
    tschauLabel = new QLabel("TschauTschau!");
    
    quitButton = new QPushButton("Close");
    ohneButton = new QPushButton("OhneFunktion");
    
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(helloLabel);
    mainLayout->addWidget(quitButton);
    mainLayout->addWidget(ohneButton);
    mainLayout->addWidget(tschauLabel);
    setLayout(mainLayout);
    
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));

    
    setWindowTitle(tr("Helloooo"));

}