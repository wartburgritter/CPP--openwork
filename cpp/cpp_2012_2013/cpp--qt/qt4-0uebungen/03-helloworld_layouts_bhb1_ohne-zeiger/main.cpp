#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFont>
#include <QtGui/QVBoxLayout>


int main(int argc, char *argv[])
{
  QApplication myApp(argc, argv);

  QWidget myWindow;                        // Konstruktor fuer Hauptfenster
  myWindow.move(650, 125);
  myWindow.setWindowTitle("Helloooo");  
  //myWindow.resize(300, 200);  // macht gar keinen Sinn, da QVBoxLayout Groesse automatisch setzt
  QVBoxLayout myLayout(&myWindow);  //Instanz der Klasse QVBoxlayout mit Uebergabeparameter Zeiger auf Fenster im Konstruktor

  //Die einzelnen Widgetelemente erstellen und zeichnen
  QLabel myLabel("Hello Hugo!", &myWindow);
  myLabel.setAlignment(Qt::AlignCenter);
  myLabel.setFont(QFont("Arial", 20, QFont::Bold));
  myLayout.addWidget(&myLabel);

  QPushButton myButton("Close", &myWindow);
  //  myButton.setFont(QFont("Arial", 20));
  //  myButton.setGeometry(10, 60, 180, 30);
  myLayout.addWidget(&myButton);

  QPushButton myButton2("OhneFunktion", &myWindow);
  myLayout.addWidget(&myButton2);

  QLabel myLabel2("TschauTschau!", &myWindow);
  myLabel2.setAlignment(Qt::AlignCenter);
  myLayout.addWidget(&myLabel2);
 
  
  QObject::connect (&myButton, SIGNAL(clicked()), &myWindow, SLOT(close()));

  myWindow.show();
  //myWindow.showMinimized();
  //myWindow.showMaximized();

  return myApp.exec();
}


/*
qmake-qt4 -project
qmake-qt4
make
*/

