#include <QApplication>
#include "helloworld.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  HelloWorld helloWorld;
  helloWorld.show();
  return app.exec();
  
  // klasse dialog hat eigene exec-methode daher ist ein helloWorld.show nicht noetig
  // es d dann aber heissen return helloWorld.exec();
  // siehe 02-helloworld-minilayout-qdialog
  
}


/*
qmake-qt4 -project
qmake-qt4
make
*/

 