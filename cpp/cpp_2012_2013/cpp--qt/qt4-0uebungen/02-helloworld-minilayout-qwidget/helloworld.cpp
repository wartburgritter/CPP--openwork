#include <QtGui>

#include "helloworld.h"

HelloWorld::HelloWorld()       //Konstruktor
{

    hello1Label = new QLabel("Hello Hugo!!");
    hello2Label = new QLabel("Hello Widget!!");
        
    
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(hello1Label);
    mainLayout->addWidget(hello2Label);
    setLayout(mainLayout);
     
    
    setWindowTitle(tr("Hello Hugo App"));

}