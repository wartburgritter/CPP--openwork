#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>

class QLabel;

class HelloWorld : public QWidget
{
    Q_OBJECT

public:
    HelloWorld();

private:
    QLabel *hello1Label;
    QLabel *hello2Label;
};

#endif