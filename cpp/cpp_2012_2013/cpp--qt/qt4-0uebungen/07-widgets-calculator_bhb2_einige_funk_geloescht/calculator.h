 #ifndef CALCULATOR_H
 #define CALCULATOR_H

 #include <QDialog>

 class QLineEdit;

 class Button;

 class Calculator : public QDialog
 {
     Q_OBJECT

 public:
     Calculator(QWidget *parent = 0);

 private slots:
     void digitClicked();
     void unaryOperatorClicked();
     void pointClicked();
     void changeSignClicked();
     void backspaceClicked();
     void clearAll();
     void abbruchAktion();
 
 private:
     Button *createButton(const QString &text, const char *member);
     void abortOperation();
     bool waitingForOperand;

     QLineEdit *display;
     QLineEdit *display2;    //display2 deklarieren
     QLabel *label1;

     enum { NumDigitButtons = 10 };
     Button *digitButtons[NumDigitButtons];
 };

 #endif