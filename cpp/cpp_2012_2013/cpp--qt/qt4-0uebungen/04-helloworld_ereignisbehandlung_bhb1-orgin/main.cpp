#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QMouseEvent>
#include <QtGui/QPalette>
#include <QtGui/QVBoxLayout>
#include <math.h>

////Klasse ExtendedQLabel/////////////////
//Diese Klasse erweitert QLabel (QLabel ist die Basisklasse)
class ExtendedQLabel : public QLabel
{
//Deklaration der Klasse ExtendedQLabel 
  Q_OBJECT
public:
  ExtendedQLabel(const QString &text, QWidget *parent =0);   //Konstruktor
protected:
  virtual void mousePressEvent(QMouseEvent *event);         
  //Einzige neue (respective umprogrammierte, das sie virtual ist) Methode der Klasse ExtendedQLabel.
  //Alle anderen Methoden werden geerbt.
};

//Implementation der Klasse ExtendedQLabel
//Konstruktor
ExtendedQLabel::ExtendedQLabel(const QString &text, QWidget *parent)
  :QLabel(text, parent)
{}

//virtuelle Methode mousePressEvent
//Das heisst wir ordnen der Methode mousePressEvent der Basisklasse QLabel in unserer
//Klasse ExtendedQLabel ein neues Verhalten zu. Dies ist wohl Polymorphie
//Die Parameterliste und der Rueckgabetyp der Methode in der abgeleiteten Klasse muss zu denen 
//der Methode in der Basisklasse passen.
void ExtendedQLabel::mousePressEvent(QMouseEvent *event)
{
  event->accept();    // aufruf von accept() signalisiert, dass ereignis behandelt wurde, daher kein mehrfaches aufrufen 
  QPalette myPalette = this->palette();
  QPoint pos = event->pos();
  // Orginal
  //int r = 255 * pos.x() / width();
  //int g = 255 * pos.y() / height();
  //int b = 100;
  //Graupalette von links nach rechts
  /*int r = 255 * pos.x() / width();
  int g = 255 * pos.x() / width();
  int b = 255 * pos.x() / width();
  */
  // abhaenging von Quatranten, Variablen muessen ausserhalb der if-anweisung 
  // deklariert werden, sonst sind sie nur innerhalb dieser sichtbar
  int r=255;  //mit weiss initialisiert alles 255, tritt ein im vierten quatranten
  int g=255;
  int b=255;
  if (pos.x() < width() * 0.5  &&  pos.y() < height() * 0.5 ) {
    r = 200; 
    g = 10; 
    b = 10;
  }
  else if (pos.x() < width() * 0.5  &&  pos.y() > height() * 0.5 ) {
    r = 10;
    g = 200;
    b = 10;  
  }
  else if (pos.x() > width() * 0.5  &&  pos.y() < height() * 0.5 ) {
    r = 10;
    g = 10;
    b = 200;  
  }
  myPalette.setColor(QPalette::Foreground, QColor(r,g,b));
  setPalette(myPalette);
  update();
}



////Hauptprogrammm/////////////////
int main(int argc, char *argv[])
{
  // Im Hauptprogrammm gibt es nur einen Unterschied zu Programmbsp 03 layouts
  // Es wird anstatt ein Label des Typs QLabel ein Label des Typs ExtendedQLabel erzeugt.
  
  QApplication myApp(argc, argv);

  QWidget myWindow;
  myWindow.move(600, 125);
  QVBoxLayout myLayout(&myWindow);

  ExtendedQLabel myLabel("Hello World!", &myWindow);
  myLabel.setAlignment(Qt::AlignCenter);
  myLabel.setFont(QFont("Arial", 100));
  myLayout.addWidget(&myLabel);

  QPushButton myButton("Close", &myWindow);
  myButton.setFont(QFont("Arial", 30, QFont::Bold));
  myLayout.addWidget(&myButton);

  QObject::connect (&myButton, SIGNAL(clicked()), &myWindow, SLOT(close()));
  
  myWindow.show();
 
  return myApp.exec();
}

#include "main.moc"


/*
qmake-qt4 -project
qmake-qt4
make

*/

