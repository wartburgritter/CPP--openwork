 #ifndef DIALOG_H
 #define DIALOG_H

 #include <QDialog>

 class QLabel;

 class Dialog : public QDialog
 {
     Q_OBJECT

 public:
     Dialog();       

 private:
     QLabel *label1;
     QLabel *label2;
 };

 #endif