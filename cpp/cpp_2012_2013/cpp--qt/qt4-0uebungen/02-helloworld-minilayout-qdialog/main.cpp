#include <QApplication>
#include "dialog.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  Dialog dialog;
  return dialog.exec();
  
  // obiges funktioniert nicht mit QWidget als Basis, da diese die Methode exec nicht hat 
  // siehe 02-helloworld-minilayout-qwidget
  
}


/*
qmake-qt4 -project
qmake-qt4
make

Dateien:
main.cpp
dialog.h
dialog.cpp
*/
