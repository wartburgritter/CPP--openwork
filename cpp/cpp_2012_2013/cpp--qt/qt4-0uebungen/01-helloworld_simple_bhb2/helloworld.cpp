#include <QtGui>

#include "helloworld.h"

HelloWorld::HelloWorld()       //Konstruktor
{

    // es gibt keinen layoutmanager label und butten
    // verweisen mittels this-zeiger auf widget HelloWorld
    // genause wie der close slot mit this-zeiger auf HelloWorl-widget verweist
    
    helloLabel = new QLabel("Hello Hugo!!", this);
    helloLabel->setGeometry(0, 0, 300, 50);     
    helloLabel->setAlignment(Qt::AlignCenter);
    helloLabel->setFont(QFont("Arial", 20, QFont::Light));
    
    quitButton = new QPushButton("Close", this);
    quitButton->setGeometry(25, 75, 250, 100);
    quitButton->setFont(QFont("Arial", 30, QFont::Black));
    
    QObject::connect (quitButton, SIGNAL(clicked()), this, SLOT(close()));
    
    
    resize(300, 200);                   // breite, hoehe
    setWindowTitle(tr("Hello Hugo App"));

}