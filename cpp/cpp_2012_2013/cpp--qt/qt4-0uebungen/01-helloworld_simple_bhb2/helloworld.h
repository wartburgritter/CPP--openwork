#ifndef HELLOWORLD_H
#define HELLOWORLD_H


#include <QWidget>

class QLabel;
class QPushButton;
class QFont;

class HelloWorld : public QWidget
{
    Q_OBJECT

public:
    HelloWorld();

private:
    QLabel *helloLabel;
    QPushButton *quitButton;
    
};

#endif
