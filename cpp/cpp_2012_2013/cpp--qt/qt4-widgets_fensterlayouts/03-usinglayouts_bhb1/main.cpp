#include <QtGui>

 int main(int argc, char *argv[])
 {
     QApplication app(argc, argv);
     QWidget window;
     QLabel *label = new QLabel(QApplication::translate("windowlayout", "Name:"));
     QLineEdit *mylineEdit1 = new QLineEdit();
     QPushButton *mybutton1 = new QPushButton(QApplication::translate("windowlayout", "Press me"));
     QPushButton *mybutton2 = new QPushButton("Hau Drauf");
     QSpinBox *myspinbox1 = new QSpinBox();
     QComboBox *mycombobox1 = new QComboBox();
          mycombobox1->addItem("Ich bin die obere Auswahl");
          mycombobox1->addItem("Ich bin die zweite Auswahl");
          mycombobox1->addItem("Ich bin die letzte Auswahl");
     QSlider *myslider1 = new QSlider(Qt::Horizontal);
     QDial *myqdial1 = new QDial();
     QScrollBar *myscrollbar1 = new QScrollBar();
	  
     QHBoxLayout *layout = new QHBoxLayout();
     layout->addWidget(label);
     layout->addWidget(mylineEdit1);
     layout->addWidget(mybutton1);
     layout->addWidget(mybutton2);
     layout->addWidget(myspinbox1);
     layout->addWidget(mycombobox1);
     layout->addWidget(myslider1);
     layout->addWidget(myqdial1);
     layout->addWidget(myscrollbar1);
     window.setLayout(layout);
     window.setWindowTitle(
         QApplication::translate("windowlayout", "Hugos Window layout"));
     window.show();
     return app.exec();
 }


/*
qmake-qt4 -project
qmake-qt4
make

*/
