#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QMouseEvent>
#include <QtGui/QFrame>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>
#include <QtGui/QLabel>


class BoardWidget : public QFrame
{
  Q_OBJECT
public:
  BoardWidget(QWidget *parent = 0);

public slots:
  void clear();
  void red();
  void blue();
  void green();
  void yellow();
  void magenta();
   
protected:
  void mouseMoveEvent(QMouseEvent *event);
  void resizeEvent(QResizeEvent *event);
  void paintEvent(QPaintEvent *event);

private:
  QPixmap my_m_pixmap;   //Eine Pixmap ("zeichengeraet") mit dem Namen my_m_pixmap wird deklariert und definiert
                         //Dies ist das einzige Attribut, welches in allen Methoden bekannt ist.
                         //Dieses eine Attribut macht die Klasse aus.
};

// Konstruktor
BoardWidget::BoardWidget(QWidget *parent)
  : QFrame(parent)
{
  setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
}

// pixmap leeren
void BoardWidget::clear()
{
  my_m_pixmap.fill(Qt::white);
  update();
}

void BoardWidget::red()
{
  my_m_pixmap.fill(Qt::red);
  update();
}

void BoardWidget::blue()
{
  my_m_pixmap.fill(Qt::blue);
  update();
}

void BoardWidget::green()
{
  my_m_pixmap.fill(Qt::green);
  update();
}

void BoardWidget::yellow()
{
  my_m_pixmap.fill(Qt::yellow);
  update();
}

void BoardWidget::magenta()
{
  my_m_pixmap.fill(Qt::magenta);
  update();
}

// zeichenevent auf die pixmap wenn maus mit gedrueckter taste bewegt wird
// um auf die Pixmap zeichnen zu koennen braucht es einen Paintdevice nämlich painter
void BoardWidget::mouseMoveEvent(QMouseEvent *event)
{
  event->accept();
  QPoint my_position = event->pos();   //Member von Klasse QMouseEvent heisst pos
  QPainter my_painter1(&my_m_pixmap);  // der QPainter my_painter1 ist nur in der aktuellen methode bekannt
  //  painter.fillRect(pos.x() - 2, pos.y() - 2, 4, 4, Qt::black);  //orginal ort entfernt von mauszeiger und fuellquadrate = linienstaerke
  // Zeichenfarbe von des QPainter my_painter mittels der Methode setPen aendern
  my_painter1.setPen(Qt::blue);
  //kreise zeichnen
  my_painter1.drawArc(my_position.x() - 5, my_position.y() - 5, 20, 20, 0*16, 360*16 );
  update();
}

// trotz resize bleibt pixmap erhalten 
// woher weiss die funktion das sie aufgerufen werden muss 
// Die Urpixmap bei programmstart ist auch cyan, Warum?
void BoardWidget::resizeEvent(QResizeEvent *event)
{
  event->accept();
  QPixmap new_pixmap(event->size());  // ist das der funktionsaufruf?
  new_pixmap.fill(Qt::cyan);
  QPainter painter(&new_pixmap);
  painter.drawPixmap(QPoint(0, 0), my_m_pixmap);
  my_m_pixmap = new_pixmap;
}

// zeichnet fertige pixmap auf sich selbst
// wird durch das update() in mouseMoveEvent aufgerufen
void BoardWidget::paintEvent(QPaintEvent *event)
{
  event->accept();
  QPainter my_painter2(this);    // der QPainter my_painter2 ist nur in der aktuellen methode bekannt
  my_painter2.drawPixmap(QPoint(0, 0), my_m_pixmap);
  QFrame::paintEvent(event);
}


int main(int argc, char *argv[])
{
  // Im vertikalen layout verden das zeichenboar und ein horizontales layout angeordnet
  // Im horizontalen layout werden die Button angeordnet.
  // Der Rest ist einfach. Die Tricks stecken in der Klasse BoardWidget
  
  QApplication myApp(argc, argv);

  QWidget myWindow;
    myWindow.move(850, 125);
    myWindow.resize(500, 350);
    myWindow.setWindowTitle("Hugos Painter");
    QVBoxLayout myTop_layout(&myWindow);
      myTop_layout.setMargin(20);

      BoardWidget myBoard(&myWindow);
      myTop_layout.addWidget(&myBoard);
    
      QHBoxLayout myColorButton_layout;
       myTop_layout.addLayout(&myColorButton_layout);
  
        QPushButton myRed_button("red",&myWindow);
        QObject::connect(&myRed_button, SIGNAL(clicked()), &myBoard, SLOT(red()));
        myColorButton_layout.addWidget(&myRed_button);
        
        QPushButton myBlue_button("blue",&myWindow);
        QObject::connect(&myBlue_button, SIGNAL(clicked()), &myBoard, SLOT(blue()));
        myColorButton_layout.addWidget(&myBlue_button);
 
        QPushButton myGreen_button("green",&myWindow);
        QObject::connect(&myGreen_button, SIGNAL(clicked()), &myBoard, SLOT(green()));
        myColorButton_layout.addWidget(&myGreen_button);

        QPushButton myYellow_button("yellow",&myWindow);
        QObject::connect(&myYellow_button, SIGNAL(clicked()), &myBoard, SLOT(yellow()));
        myColorButton_layout.addWidget(&myYellow_button);
	
        QPushButton myMagenta_button("magenta",&myWindow);
        QObject::connect(&myMagenta_button, SIGNAL(clicked()), &myBoard, SLOT(magenta()));
        myColorButton_layout.addWidget(&myMagenta_button);

      QHBoxLayout myButton_layout;
      myTop_layout.addLayout(&myButton_layout);

        QLabel myLabel("Viel Spass beim malen!", &myWindow);
        myButton_layout.addWidget(&myLabel);
  
        myButton_layout.addStretch();

        QPushButton myClear_button("Leeren",&myWindow);
        QObject::connect(&myClear_button, SIGNAL(clicked()), &myBoard, SLOT(clear()));
        myButton_layout.addWidget(&myClear_button);

        QPushButton myClose_button("Schliessen", &myWindow);
        QObject::connect(&myClose_button, SIGNAL(clicked()), &myWindow, SLOT(close()));
        myButton_layout.addWidget(&myClose_button);

  
    myWindow.show();

  return myApp.exec();
}

#include "main.moc"


/*
qmake-qt4 -project
qmake-qt4
make
*/

