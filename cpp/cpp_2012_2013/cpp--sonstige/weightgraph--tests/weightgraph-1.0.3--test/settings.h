#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

struct GraphSettings
{
  enum WeightIntervalMode {
    AutomaticWithGoalWeight,
    AutomaticWithoutGoalWeight,
    Manual
  };
  bool goalWeightEnabled;
  WeightIntervalMode weightIntervalMode;
  double weightIntervalMin;
  double weightIntervalMax;
  int defaultTimeInterval;
};

class Settings : public QObject
{
  Q_OBJECT
public:
  static Settings *self() { return singleton; }
  static QString weightUnit();
  static double goalWeightMin();
  static double goalWeightMax();
  static bool grabZoomKeys();
  static GraphSettings graphSettings(const QString &graphId);
  static QStringList graphIds();

  static QString datadir();
  static QString datafile();
  static QString settingsfile();
public slots:
  static void setWeightUnit(QString wu);
  static void setWeightUnitAndSync(QString wu);
  static void setGoalWeightMin(double min);
  static void setGoalWeightMinAndSync(double min);
  static void setGoalWeightMax(double max);
  static void setGoalWeightMaxAndSync(double max);
  static void setGrabZoomKeys(bool grab);
  static void setGrabZoomKeysAndSync(bool grab);
  static void setGraphSettings(const QString &graphId, GraphSettings &gs);
  static void setGraphSettingsAndSync(const QString &graphId,
                                      GraphSettings &gs);
  static void sync();
signals:;
  //settingChanged is emitted when sync() is called.
  void settingChanged();
private:
  Settings() : QObject(0) {}
  static Settings *singleton;
  static QSettings s;
  static const QString weightUnitKey;
  static const QString goalWeightMinKey;
  static const QString goalWeightMaxKey;
  static const QString grabZoomKeysKey;
  static const QString graphSettingsGroupSuffix;
  static const QString goalWeightEnabledKey;
  static const QString weightIntervalModeKey;
  static const QString weightIntervalMinKey;
  static const QString weightIntervalMaxKey;
  static const QString defaultTimeIntervalKey;
};
#endif // SETTINGS_H
