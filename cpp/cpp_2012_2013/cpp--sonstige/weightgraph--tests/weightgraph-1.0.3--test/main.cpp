#include <QtGui/QApplication>
#include <QDir>
#include <QMainWindow>
#include <QMessageBox>
#include "mainwindow.h"
#include "weightdata.h"
#include "settings.h"
#include "settingswindow.h"

//For debugging
#include "weightgraphview.h"

//Global pointer to the weight data
WeightDataModel *wdm;
Settings *settings;

int main(int argc, char *argv[])
{
  QCoreApplication::setApplicationName("WeightGraph");
  QCoreApplication::setOrganizationDomain("vpu.me");
  QApplication a(argc, argv);
  QDir dir(Settings::datadir());
  try {
    if(!dir.exists())
      dir.mkpath(Settings::datadir());
  } catch(...) {
    QMainWindow w2;
    w2.show();
    QMessageBox::critical(&w2, "Critical error",
                          "Critical error while creating directory \""+Settings::datadir()+"\"");
    return 1;
  }
  QString datafilename = Settings::datafile();
  try {
    wdm = new WeightDataModel(datafilename, &a);
  }
  catch(QString error) {
    QMainWindow w2;
    w2.show();
    QMessageBox::critical(&w2, "Critical error",
                          "Critical error while opening weight data file:\""+error+"\"");
    return 1;
  }

  MainWindow w;
#if defined(Q_WS_S60)
  w.showMaximized();
#else
  // fuer desktoptests fenstergroesse definieren
  w.resize( 800, 350 );    // hauptfenster, fuer zweiten graphen siehe weightgraphview.cpp
  w.show();
#endif

  return a.exec();
}
