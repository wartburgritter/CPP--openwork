/****************************************************************************
** Meta object code from reading C++ file 'settings.h'
**
** Created: Mon Aug 20 10:57:53 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "settings.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'settings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Settings[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      30,   27,    9,    9, 0x0a,
      53,   27,    9,    9, 0x0a,
      87,   83,    9,    9, 0x0a,
     112,   83,    9,    9, 0x0a,
     148,  144,    9,    9, 0x0a,
     173,  144,    9,    9, 0x0a,
     210,  205,    9,    9, 0x0a,
     232,  205,    9,    9, 0x0a,
     272,  261,    9,    9, 0x0a,
     313,  261,    9,    9, 0x0a,
     361,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Settings[] = {
    "Settings\0\0settingChanged()\0wu\0"
    "setWeightUnit(QString)\0"
    "setWeightUnitAndSync(QString)\0min\0"
    "setGoalWeightMin(double)\0"
    "setGoalWeightMinAndSync(double)\0max\0"
    "setGoalWeightMax(double)\0"
    "setGoalWeightMaxAndSync(double)\0grab\0"
    "setGrabZoomKeys(bool)\0"
    "setGrabZoomKeysAndSync(bool)\0graphId,gs\0"
    "setGraphSettings(QString,GraphSettings&)\0"
    "setGraphSettingsAndSync(QString,GraphSettings&)\0"
    "sync()\0"
};

const QMetaObject Settings::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Settings,
      qt_meta_data_Settings, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Settings::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Settings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Settings::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Settings))
        return static_cast<void*>(const_cast< Settings*>(this));
    return QObject::qt_metacast(_clname);
}

int Settings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: settingChanged(); break;
        case 1: setWeightUnit((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: setWeightUnitAndSync((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: setGoalWeightMin((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: setGoalWeightMinAndSync((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: setGoalWeightMax((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: setGoalWeightMaxAndSync((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: setGrabZoomKeys((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: setGrabZoomKeysAndSync((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: setGraphSettings((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< GraphSettings(*)>(_a[2]))); break;
        case 10: setGraphSettingsAndSync((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< GraphSettings(*)>(_a[2]))); break;
        case 11: sync(); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void Settings::settingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
