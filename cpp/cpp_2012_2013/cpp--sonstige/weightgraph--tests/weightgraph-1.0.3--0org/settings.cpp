#include "settings.h"
#include <QDir>

Settings *Settings::singleton = new Settings();
QSettings Settings::s(Settings::settingsfile(), QSettings::IniFormat);
const QString Settings::weightUnitKey = "WeightUnit";
const QString Settings::goalWeightMinKey = "GoalWeightMin";
const QString Settings::goalWeightMaxKey = "GoalWeightMax";
const QString Settings::grabZoomKeysKey = "GrabZoomKeys";
const QString Settings::graphSettingsGroupSuffix = "Graph";
const QString Settings::goalWeightEnabledKey = "GoalWeightEnabled";
const QString Settings::weightIntervalModeKey = "WeightIntervalMode";
const QString Settings::weightIntervalMinKey = "WeightIntervalMin";
const QString Settings::weightIntervalMaxKey = "WeightIntervalMax";
const QString Settings::defaultTimeIntervalKey = "DefaultTimeInterval";

QString Settings::weightUnit()
{
  if(!s.contains(weightUnitKey))
    setWeightUnit("kg");
  return s.value(weightUnitKey).toString();
}
void Settings::setWeightUnit(QString wu)
{
  s.setValue(weightUnitKey, wu);
}
void Settings::setWeightUnitAndSync(QString wu)
{
  setWeightUnit(wu);
  sync();
}

double Settings::goalWeightMin()
{
  if(!s.contains(goalWeightMinKey))
    setGoalWeightMin(0.0);
  return s.value(goalWeightMinKey).toDouble();
}
void Settings::setGoalWeightMin(double min)
{
  s.setValue(goalWeightMinKey, min);
}
void Settings::setGoalWeightMinAndSync(double min)
{
  setGoalWeightMin(min);
  sync();
}

double Settings::goalWeightMax()
{
  if(!s.contains(goalWeightMaxKey))
    setGoalWeightMax(0.0);
  return s.value(goalWeightMaxKey).toDouble();
}
void Settings::setGoalWeightMax(double max)
{
  s.setValue(goalWeightMaxKey, max);
}
void Settings::setGoalWeightMaxAndSync(double max)
{
  setGoalWeightMax(max);
  sync();
}

bool Settings::grabZoomKeys()
{
  if(!s.contains(grabZoomKeysKey))
    setGrabZoomKeys(true);
  return s.value(grabZoomKeysKey).toBool();
}
void Settings::setGrabZoomKeys(bool grab)
{
  s.setValue(grabZoomKeysKey, grab);
}
void Settings::setGrabZoomKeysAndSync(bool grab)
{
  setGrabZoomKeys(grab);
  sync();
}

GraphSettings Settings::graphSettings(const QString &graphId)
{
  GraphSettings ret;
  bool changed = false;
  s.beginGroup(graphId+graphSettingsGroupSuffix);

  if (!s.contains(goalWeightEnabledKey)) {
    s.setValue(goalWeightEnabledKey, false);
    changed = true;
  }
  ret.goalWeightEnabled = s.value(goalWeightEnabledKey).toBool();

  if (!s.contains(weightIntervalModeKey)) {
    s.setValue(weightIntervalModeKey, (int)GraphSettings::AutomaticWithoutGoalWeight);
    changed = true;
  }
  ret.weightIntervalMode =
      (GraphSettings::WeightIntervalMode)s.value(weightIntervalModeKey).toInt();

  if (!s.contains(weightIntervalMinKey)) {
    s.setValue(weightIntervalMinKey, 0.0);
    changed = true;
  }
  ret.weightIntervalMin = s.value(weightIntervalMinKey).toDouble();

  if (!s.contains(weightIntervalMaxKey)) {
    s.setValue(weightIntervalMaxKey, 0.0);
    changed = true;
  }
  ret.weightIntervalMax = s.value(weightIntervalMaxKey).toDouble();

  if (!s.contains(defaultTimeIntervalKey)) {
    s.setValue(defaultTimeIntervalKey, 0);
    changed = true;
  }
  ret.defaultTimeInterval = s.value(defaultTimeIntervalKey).toInt();

  s.endGroup();
  if (changed)
    sync();
  return ret;
}
void Settings::setGraphSettings(const QString &graphId, GraphSettings &gs)
{
  s.beginGroup(graphId+graphSettingsGroupSuffix);
  s.setValue(goalWeightEnabledKey, gs.goalWeightEnabled);
  s.setValue(weightIntervalModeKey, (int)gs.weightIntervalMode);
  s.setValue(weightIntervalMinKey, gs.weightIntervalMin);
  s.setValue(weightIntervalMaxKey, gs.weightIntervalMax);
  s.setValue(defaultTimeIntervalKey, gs.defaultTimeInterval);
  s.endGroup();
}
void Settings::setGraphSettingsAndSync(const QString &graphId,
                                       GraphSettings &gs)
{
  setGraphSettings(graphId, gs);
  sync();
}

QStringList Settings::graphIds()
{
  QStringList tmp = s.childGroups();
  tmp = tmp.filter(QRegExp("^\\w+Graph$"));
  return tmp.replaceInStrings(QRegExp("^(\\w+)Graph$"), "\\1");
}
void Settings::sync()
{
  s.sync();
  emit singleton->settingChanged();
}

QString Settings::datadir()
{
#ifdef Q_WS_MAEMO_5
  return QDir::home().path()+"/MyDocs/WeightGraph";
#else
  return QDir::home().path()+"/.weightgraph";
#endif
}
QString Settings::datafile()
{
  return datadir()+"/weightdata.txt";
}
QString Settings::settingsfile()
{
  return datadir()+"/config.ini";
}
