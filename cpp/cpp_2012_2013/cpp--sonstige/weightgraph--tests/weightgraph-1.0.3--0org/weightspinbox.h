#ifndef WEIGHTSPINBOX_H
#define WEIGHTSPINBOX_H

#include <QDoubleSpinBox>
#include "settings.h"

class WeightSpinBox : public QDoubleSpinBox
{
  Q_OBJECT
public:
  WeightSpinBox(QWidget *parent=0) : QDoubleSpinBox(parent)
  {
    setDecimals(1);
    setSingleStep(0.1);
    setMinimum(0.1);
    setMaximum(999.9);
    setValue(60.0);
    updateWeightUnit();
    connect(Settings::self(), SIGNAL(settingChanged()),
            this, SLOT(updateWeightUnit()));
  }
private slots:
  void updateWeightUnit() {
    setSuffix(" "+Settings::weightUnit());
  }
};

#endif // WEIGHTSPINBOX_H
