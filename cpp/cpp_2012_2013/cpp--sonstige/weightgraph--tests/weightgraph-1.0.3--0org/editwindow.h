#ifndef EDITWINDOW_H
#define EDITWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QDateEdit>
#ifdef Q_WS_MAEMO_5
#include <QMaemo5ValueButton>
#endif
#include <QGridLayout>
#include <QDialog>
#include "weightview.h"
#include "weightdata.h"

class EditWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit EditWindow(QWidget *parent = 0);
public slots:
  void updateButtons();
  void addWeight();
  void removeSelected();
  void editSelected();
  void show();
private:
  WeightView weightView;
  QPushButton addButton, removeButton, editButton;
  bool shown;
};

class AddWeightDialog : public QDialog
{
  Q_OBJECT
public:
  AddWeightDialog(QWidget *parent=0);
  WeightDataModel::DateWeight getDateWeight();

private:
#ifdef Q_WS_MAEMO_5
  QMaemo5ValueButton *date;
#else
  QDateEdit *date;
#endif
  WeightSpinBox *weight;
};


#endif // EDITWINDOW_H
