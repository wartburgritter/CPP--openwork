#ifndef WEIGHTVIEW_H
#define WEIGHTVIEW_H

#include <QTableView>
#include <QStyledItemDelegate>
#include <QItemEditorCreatorBase>
#include <QDoubleSpinBox>
#include <QItemEditorFactory>
#include <QFont>
#include <QHeaderView>
#include "weightspinbox.h"

class WeightView : public QTableView
{
  Q_OBJECT
public:
  explicit WeightView(QWidget *parent = 0) :
        QTableView(parent)
  {
    QItemEditorFactory *editorFactory = new QItemEditorFactory;
    editorFactory->registerEditor(QVariant::Double, new WeightView::WeightEditCreator);
    delegate.setItemEditorFactory(editorFactory);
    setItemDelegate(&delegate);
    setSelectionMode(QAbstractItemView::SingleSelection);
    this->verticalHeader()->hide();
    this->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    this->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    QFont f;
    f.setPointSize(28);
    setFont(f);
  }

  class WeightEditCreator : public QItemEditorCreatorBase
  {
  public:
    WeightEditCreator() : QItemEditorCreatorBase() { }
    virtual QWidget *createWidget(QWidget *parent) const
    {
      return new WeightSpinBox(parent);
    }
    virtual QByteArray valuePropertyName() const { return "value"; }
  };
  private:
    QStyledItemDelegate delegate;
};

#endif // WEIGHTVIEW_H
