#include <QApplication>
#include "KlasseBildbetrachterBhb.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    KlasseBildbetrachterBhb fensterdestypsklassebildbetrachter;
    fensterdestypsklassebildbetrachter.resize( 750, 500 );
    fensterdestypsklassebildbetrachter.setWindowTitle( "Meen Eigener Bildbetrachter" );
    fensterdestypsklassebildbetrachter.show();
    return app.exec();
}


/*
qmake-qt4 -project
qmake-qt4
make

Dateien:
main.cpp
KlasseBildbetrachterBhb.h
KlasseBildbetrachterBhb.cpp
*/