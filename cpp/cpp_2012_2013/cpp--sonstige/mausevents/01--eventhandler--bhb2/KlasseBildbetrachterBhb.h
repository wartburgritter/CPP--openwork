#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;

class KlasseBildbetrachterBhb : public QMainWindow
{
    Q_OBJECT

public:
    KlasseBildbetrachterBhb();

private slots:
    void open();

private:
    void createActions();
    void createMenus();
    void adjustScrollBar(QScrollBar *scrollBar);

    QLabel *imageLabel;
    QScrollArea *scrollArea;

    QAction *openAct;
    QAction *exitAct;

    QMenu *fileMenu;

  protected:
    void closeEvent( QCloseEvent *event );  // Close-Event von QMainWindow überschreiben

};

#endif

