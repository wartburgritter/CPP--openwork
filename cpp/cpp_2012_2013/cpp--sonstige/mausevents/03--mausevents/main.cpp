// main.cpp
#include "MouseInfo.h"
#include <QApplication>
 
int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );
  MouseInfo m;
 
  m.show();
 
  return app.exec();
}



/*
qmake-qt4 -project
qmake-qt4
make

Dateien:
main.cpp
MouseInfo.h
MouseInfo.cpp
*/