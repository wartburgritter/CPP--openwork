// MouseInfo.h
#ifndef MOUSEINFO_H
#define MOUSEINFO_H
 
#include <QWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <iostream>
 
class MouseInfo : public QWidget
{
 
  public:
    MouseInfo();
 
  protected:
    void mouseMoveEvent( QMouseEvent *event );         // Mausbewegungs-Event überschreiben
    void mousePressEvent( QMouseEvent *event );        // Event für gedrückte Maustasten überschreiben
    void mouseReleaseEvent( QMouseEvent *event );      // Event für losgelassene Maustasten überschreiben
    void mouseDoubleClickEvent( QMouseEvent *event );  // Event für Doppelklicks überschreiben
    void wheelEvent( QWheelEvent *event );             // Mausrad-Event überschreiben
 
};

#endif // MOUSEINFO_H