// MouseInfo.cpp
#include "MouseInfo.h"
 
MouseInfo::MouseInfo()
{
  resize( 400, 400 );
}
 
 
void MouseInfo::mouseMoveEvent( QMouseEvent *event )
{
  // Koordinaten der Maus ausgeben
  std::cout << "Maus wurde mit gedrueckter Taste bewegt; neue Koordinaten: " << event->x()
            << " / " << event->y() << std::endl;
}
 
 
 
void MouseInfo::mousePressEvent( QMouseEvent *event )
{
  // Name der Maustaste und Koordinaten ausgeben
  switch( event->button() )
  {
    case Qt::LeftButton:
      std::cout << "Linke ";
      break;
    case Qt::RightButton:
      std::cout << "Rechte ";
      break;
    case Qt::MidButton:
      std::cout << "Mittlere ";
      break;
  }
  std::cout << "Maustaste wurde an den Koordinaten " << event->x()
            << " / " << event->y() << " gedrueckt" << std::endl;
}
 
 
 
void MouseInfo::mouseReleaseEvent( QMouseEvent *event )
{
  // Name der Maustaste und Koordinaten ausgeben
  switch( event->button() )
  {
    case Qt::LeftButton:
      std::cout << "Linke ";
      break;
    case Qt::RightButton:
      std::cout << "Rechte ";
      break;
    case Qt::MidButton:
      std::cout << "Mittlere ";
      break;
  }
  std::cout << "Maustaste wurde an den Koordinaten " << event->x()
            << " / " << event->y() << " ausgelassen" << std::endl;
}
 
 
void MouseInfo::mouseDoubleClickEvent( QMouseEvent *event )
{
  // Koordinaten des Doppelklicks ausgeben
  std::cout << "Doppelklick an den Koordinaten " << event->x()
            << " / " << event->y() << std::endl;
}
 
 
void MouseInfo::wheelEvent( QWheelEvent *event )
{
  // Gradänderung ausgeben; Der Rückgabewert von 'delta' entspricht der 8-fachen Gradzahl
  std::cout << "Mausrad wurde um " << event->delta() / 8 << "° gedreht" << std::endl;
}