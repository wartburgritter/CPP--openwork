// TextEdit.cpp
#include "TextEdit.h"
 
void TextEdit::closeEvent( QCloseEvent *event )
{
  // Benutzer Fragen, ob er das Programm wirklich beenden will
  if( QMessageBox::question ( this,             // Parent-Widget
                              "BHbs Schliessen Nachfrage",        // Fenstertitel
                              "Willst du dieses Programm wirklich beenden?",  // Text
                              QMessageBox::Yes | QMessageBox::No )  // Verfügbare Buttons
                              == QMessageBox::Yes )  // Prüfen, ob "Yes" gedrückt wurde
  {
    // Event wird akzeptiert und muss nicht weiter verarbeitet werden.
    event->accept();
    // Methode der Basisklasse aufrufen, damit das Widget geschlossen wird
    QTextEdit::closeEvent( event );
  }
  else
  {
    // Event wird nicht akzeptiert und weiter verarbeitet.
    event->ignore();
  }
}
 
 
void TextEdit::keyPressEvent( QKeyEvent *event )
{
  if( event->key() == Qt::Key_Alt + Qt::Key_F4 )    // Prüfen ob Escape gedrückt wurde
                                                    // file:///usr/share/qt4/doc/html/qt.html#Key-enum
  {
    event->accept();                      // Event wird akzeptiert
    close();                              // Widget schließen -> closeEvent() wird aufgerufen
  }
  else                                    // Es wurde eine andere Taste als Escape gedrückt
  {
    event->ignore();                      // Event wird nicht akzeptiert
    QTextEdit::keyPressEvent( event );    // Event an die Basisklasse weitergeben
  }
}