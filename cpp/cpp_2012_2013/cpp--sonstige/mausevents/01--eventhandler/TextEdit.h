// TextEdit.h
#ifndef TEXTEDIT_H
#define TEXTEDIT_H
 
#include <QTextEdit>
#include <QCloseEvent>
#include <QMessageBox>
 
class TextEdit : public QTextEdit
{
 
  Q_OBJECT
 
  protected:
    void closeEvent( QCloseEvent *event );  // Close-Event von QTextEdit überschreiben
 
};
 
#endif