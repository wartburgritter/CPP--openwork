// main.cpp
#include "TextEdit.h"
#include <QApplication>
 
int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );
  TextEdit widget;
 
  widget.resize( 300, 300 );
  widget.setWindowTitle( "Event-Test" );
  widget.show();
 
  return app.exec();
}




/*
qmake-qt4 -project
qmake-qt4
make

Dateien:
main.cpp
TextEdit.h
TextEdit.cpp
*/
