// TextEdit.cpp
#include "BerndsTextEditKlasse.h"
 
void BerndsTextEditKlasse::closeEvent( QCloseEvent *event )
{
  // Benutzer Fragen, ob er das Programm wirklich beenden will
  if( QMessageBox::question ( this,             // Parent-Widget
                              "Achtung",        // Fenstertitel
                              "Willst du dieses Programm wirklich beenden?",  // Text
                              QMessageBox::Yes | QMessageBox::No )  // Verfügbare Buttons
                              == QMessageBox::Yes )  // Prüfen, ob "Yes" gedrückt wurde
  {
    // Event wird akzeptiert und muss nicht weiter verarbeitet werden.
    event->accept();
    // Methode der Basisklasse aufrufen, damit das Widget geschlossen wird
    QTextEdit::closeEvent( event );
  }
  else
  {
    // Event wird nicht akzeptiert und weiter verarbeitet.
    event->ignore();
  }
}