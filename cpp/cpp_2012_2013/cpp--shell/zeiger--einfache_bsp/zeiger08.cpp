#include <iostream>

struct MyFoo
{
    void printThis()
    {
        std::cout << "Im folgenden wird der Inhalt des this-Zeigers gedruckt. " << this << std::endl;
    }
};

int main()
{
    MyFoo x;
    std::cout << "Adresse des Objekts x des Typs MyFoo " << &x << std::endl;
    x.printThis();  // Da printThis mit x aufgerufen wurde druckt printThis() die Adresse von x.

    return 0;
}

// Jede Methode einer Klasse hat einen this-Zeiger. 
// Dieser Zeiger zeigt immer auf das Objekt, von welchem diese Methode aufgerufen wurde.


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger08 zeiger08.cpp
./zeiger08
*/




