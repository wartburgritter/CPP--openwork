#include <iostream>
#include <string>

void myfunc(std::string& s)
{
  s = "Hallo Hugo!";
}

int main()
{
  std::string mystr;
  
  myfunc(mystr);                     
  // Funktion wird mit der Variablen aufgerufen, Da die Funktion die Adresse verlangt wird automatisch 
  // die Adresse und nicht der Variableninhalt uebergeben.
  std::cout << mystr << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger05 zeiger05.cpp
./zeiger05
*/
