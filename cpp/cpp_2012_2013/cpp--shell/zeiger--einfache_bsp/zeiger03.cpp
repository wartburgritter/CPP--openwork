#include <iostream>

int main()
{
  int A;
  
  int* a = &A;
  
  // mit dem Dereferenzierungsoperator wird auf die Daten, 
  // die an Adresse auf die der Zeiger zeigt stehen zugegriffen
  // also auf die Variable A, da a auf A zeigt.
  *a = 169;  
  std::cout << a << std::endl;
  std::cout << A << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger03 zeiger03.cpp
./zeiger03
*/
