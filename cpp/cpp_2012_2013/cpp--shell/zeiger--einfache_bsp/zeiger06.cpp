#include <iostream>

int myfunc(int* i)
{
  int j = *i;
  *i = 2 * j;
  return j / 2;
}

// uebergabeparameter intzeiger  (adresse die auf intvariable zeigt)
// j wird der inhalt der adresse der uebergebenen intvariable zugewiesenen
// in die Adresse der uebergebenen intvariable wird das doppelte von j geschrieben
// zurueckgegeben wird die haelfte von j

// wird funktion mit einer adresse einer intvariable aufgerufen, wird die 
// variable selber verdoppelt und die haelfte zurueckgegeben.

// also ausgabe zahl1 = 20 und zahl2 = 5
// :-) ich habs verstanden

int main()
{
  int zahl1 = 10;
  int zahl2;
  
  zahl2 =  myfunc(&zahl1);                     
  std::cout << zahl1 << ", " << zahl2 << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger06 zeiger06.cpp
./zeiger06
*/
