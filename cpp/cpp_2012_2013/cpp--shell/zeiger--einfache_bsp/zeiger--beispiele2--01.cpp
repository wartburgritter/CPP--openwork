#include <iostream>

int main()
{
  int a = 100, *pa = &a; 
  
  std::cout << "a = " << a << std::endl;
  std::cout << "*pa = " << *pa << "  Inhalt der Adresse die in pa gespeichert ist = Inhalt der Variablen a" << std::endl;

  std::cout << "&a (hexadezimal) = " << &a << "  Adresse von a." << std::endl;
  std::cout << "pa (hexadezimal) = " << pa << "  Inhalt der Zeigervariablen pa = die Adresse von a" << std::endl;
  std::cout << "*pa (hexadezimal) = " << &pa << "  Adresse wo die Zeigervariable pa gespeichert ist." << std::endl;
  
  std::cout << "Zeiger wird dereferenziert, Wert verändert ... " << std::endl;
  *pa = 200; 
  std::cout << "Neuer Wert von a = " << a << std::endl;
  
  return 0;
  
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger03a zeiger03a.cpp
./zeiger03
*/
