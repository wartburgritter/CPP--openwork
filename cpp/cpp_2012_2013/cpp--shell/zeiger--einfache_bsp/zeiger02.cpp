#include <iostream>

int main()
{
  int A;
  
  //Zeiger a auf Adresse einer Intvariable
  //Zuweisung der Adresse der Variable A
  int* a = &A;
  
  // Im zeiger steht die Adresse von A, wird auf a ohne Dereferenzierungsoperator
  // zugegriffen, wird die Adresse ausgegeben.
  std::cout << a << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger02 zeiger02.cpp
./zeiger02
*/
