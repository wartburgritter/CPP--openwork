#include <iostream>

int myfunc(int& i)    // Da Referenz als Parameter erwartet wird nicht Variable uebergeben sondern deren Adresse
{
int j = i;
i = 2 * j;
return j / 2;
}

int main()
{
  int zahl1 = 10;
  int zahl2;
  
  zahl2 =  myfunc(zahl1);    // Aufruf der Funftion mit Variablen
  
  std::cout << zahl1 << ", " << zahl2 << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger07 zeiger07.cpp
./zeiger07
*/
