#include <iostream>

void speichern(int *variable, int wert);


int main()
{
  int a, b, *pb = &b;
  
  speichern (&a, 100);
  speichern (pb, 200);
  
  std::cout << "a =  " << a << std::endl;
  std::cout << "b =  " << b << std::endl;
 
  return 0;
  
}


void speichern (int *variable, int wert)
{
  *variable = wert;
}



/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger--beispiele2--02 zeiger--beispiele2--02.cpp
./zeiger--beispiele2--02
*/
