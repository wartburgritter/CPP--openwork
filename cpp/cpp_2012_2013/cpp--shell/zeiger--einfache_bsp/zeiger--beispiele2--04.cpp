#include <iostream>

int main()
{  
  int zahlen[9] = {11,12,13,14,15,16,17,18,19};
  
  std::cout << "Int-Zahl eingeben: " << std::endl;
  std::cin >> zahlen[4] ;
  std::cout << std::endl;

  for (int i=0; i<9; i++)
         std::cout << "Element " << i << " = " << *(zahlen + i) << std::endl;

  return 0;
  
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger--beispiele2--04 zeiger--beispiele2--04.cpp
./zeiger--beispiele2--04
*/
