#include <iostream>

int main()
{
  int feld[10] = {11,12,13,14,15,16,17,18,19,20};
  int i;
  
  for (i = 0; i < 10; i++)
      std::cout << "Element " << i << " = " << *(feld + i) << std::endl;
      
  return 0;
  
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger--beispiele2--05 zeiger--beispiele2--05.cpp
./zeiger--beispiele2--05
*/
