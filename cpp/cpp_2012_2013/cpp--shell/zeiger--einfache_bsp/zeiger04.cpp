#include <iostream>
#include <string>

void myfunc(std::string* s)
{
  *s = "Hallo, Hugo!";
}


void speichern(std::string *s, std::string sinhalt)
{
  *s = sinhalt;
}


int main()
{
  std::string mystr;
  
  myfunc(&mystr);
  std::cout << mystr << std::endl;
  
  
  std::string mya, myb;
  std::string *pmyb = &myb;
  
  speichern (&mya, "hallo");
  speichern (pmyb, "huhu");
  
  std::cout << "mya =  " << mya << std::endl;
  std::cout << "myb =  " << myb << std::endl;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o zeiger04 zeiger04.cpp
./zeiger04
*/
