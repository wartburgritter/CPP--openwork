#include <iostream>
#include "vektor.h"




int main ()
{
  unsigned int i=0;
  
  // Ganzzahlenvektor
  Vektor<int> v(5);
  
  for (i=0; i<5; i++)
    v.at(i) = i+3;
  
  for (i=0; i<5; i++)
    std::cout << v.at(i) << " ";
  
  
  std::cout << std::endl;
  // ...
  
  
  return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o  vektor-templates vektor-templates.cpp
./vektor-templates
*/

