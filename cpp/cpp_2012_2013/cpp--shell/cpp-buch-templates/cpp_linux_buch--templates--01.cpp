#include <iostream>
#include <string>
// was includieren fuer strcmp?


template <typename T> T max (T a, T b)
{
  return (a>b) ? a : b;
}


// Funktionstemplate uebeladen um char zu vergleichen
template<>
const char* max (const char* a, const char* b)
{
//  return strcmp(a,b)>0? ? a : b;
}


int main ()
{
  int x = max(1, 5);            // int-Version
  double y = max(1.33, 3.14);   // double-Version

  std::cout << "Maximum int ist " << x << " und Maximum double ist " << y << std::endl;
  
  
  // was wenn int und double verglichen werden sollen
  std::cout << "Maximum int und double ist " <<  max<double>(3, 3.5) << std::endl;
  
  
  // string-vergleich mit ueberladenem funktionstemplate
  //std::cout << "Stringvergleich " << max("abc", "bcde") << std::endl;
  
  return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o  cpp_linux_buch--templates--01 cpp_linux_buch--templates--01.cpp
./cpp_linux_buch--templates--01
*/
