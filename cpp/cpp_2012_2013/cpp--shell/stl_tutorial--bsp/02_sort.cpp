#include <iostream>
#include <algorithm>
using namespace std;


int main()
{
    int zahlen[9] = {3,8,1,9,5,6,4,2,9};
    cout << "Ausgabe unsortiert" << endl;
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }
    cout << "Ausgabe nach anwendung von sort()" << endl;
    sort(zahlen, zahlen+9);
    // Der Bereich der funktion sort() wird mittels Iteratoren als Parameter uebergeben
    // sort kann nur fuer typen genutzt werden, fuer die der kleiner-operator definiert ist.
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }
}

/*Kompileraufruf
g++ -Wall -o 02_sort 02_sort.cpp
*/

