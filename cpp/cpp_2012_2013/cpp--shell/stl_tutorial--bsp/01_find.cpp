#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    //Array aus 9 Zahlen
    int zahlen[9] = {11,12,13,14,15,16,17,18,19};
    
    //Zeiger found
    int *found;
    
    //funktion find mit 3 Parameter
    // ersten beiden parameter sind die grenzen des containers
    // dritter parameter: wonach wird gesucht 
    // es wird in dem integer-array nach der Zahl 14 gesucht
    // das ergebnis ist hier ein zeiger auf einen integer
    found = find(zahlen, zahlen+9, 14);
    if (found!=zahlen+9)
    {
        cout << "Gefunden: " << *found << endl;
    }
    else
    {
        cout << "Nichts gefunden! " << *found << endl;
    }
}


/*Kompileraufruf
g++ -Wall -o 01_find 01_find.cpp
./01_find
*/

