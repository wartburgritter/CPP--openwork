#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int main()
{
    int zahlen[9] = {1,2,4,9,5,6,7,8,9};
    cout << "Ausgabe Zahlen" << endl;
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }

    // funktion fill() anwenden
    fill(zahlen, zahlen+9, 4);
    cout << "Ausgabe nach fill()" << endl;
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }
}


/*Kompileraufruf
g++ -Wall -o 05_fill 05_fill.cpp
*/


//strlen erzeugt fehler