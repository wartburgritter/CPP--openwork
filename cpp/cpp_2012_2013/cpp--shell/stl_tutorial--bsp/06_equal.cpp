#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int main()
{
    int zahlen[9] = {1,2,4,9,5,6,7,8,9};
    cout << "Ausgabe Zahlen" << endl;
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }

    // funktion equal() anwenden
    int vgla[3] = {1,2,4};
    cout << "Ausgabe vgla" << endl;
    for (int i=0; i<3; i++)
    {
        cout << vgla[i]<< endl;
    }
    if (equal(zahlen, zahlen+3, vgla))
    {
        cout << "gleich" << endl << endl;
    }
    else
    {
        cout << "nicht gleich" << endl << endl;
    }

    // funktion equal() nochmal anwenden diesmal nicht gleich
    int vglb[3] = {1,2,5};
    cout << "Ausgabe vgla" << endl;
    for (int i=0; i<3; i++)
    {
        cout << vglb[i]<< endl;
    }
    if (equal(zahlen, zahlen+3, vglb))
    {
        cout << "gleich" << endl << endl;
    }
    else
    {
        cout << "nicht gleich" << endl << endl;
    }

}


/*Kompileraufruf
g++ -Wall -o 06_equal 06_equal.cpp
*/


//strlen erzeugt fehler