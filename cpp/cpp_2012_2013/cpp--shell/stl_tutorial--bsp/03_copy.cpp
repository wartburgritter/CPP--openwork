#include <iostream>
#include <algorithm>
using namespace std;


int main()
{
    int zahlen[9] = {1,2,4,9,5,6,7,8,9};
    cout << "Ausgabe Original" << endl;
    for (int i=0; i<9; i++)
    {
        cout << zahlen[i]<< endl;
    }

    int ziel[5];
    copy(zahlen, zahlen+5, ziel);
    cout << "Ausgabe Kopie" << endl;
    for (int i=0; i<5; i++)
    {
        cout << ziel[i]<< endl;
    }

}

/*Kompileraufruf
g++ -Wall -o 03_copy 03_copy.cpp
*/

