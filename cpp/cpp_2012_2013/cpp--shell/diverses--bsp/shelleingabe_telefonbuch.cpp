#include <iostream>

using namespace std;

int main()
{
    cout<<"T E L F O N B U C H"<<endl;
    cout<<"---------------------"<<endl<<endl;
    cout<<"Menu"<<endl;
    cout<<" 1 Name einfuegen"<<endl;
    cout<<" 2 Name suchen"<<endl;
    cout<<" 3 Name loeschen"<<endl;
    cout<<" 4 Telefonbuch sortiert ausgeben"<<endl;
    cout<<" 5 Telefonbuch speichen"<<endl;
    cout<<" 6 Telefonbuch laden"<<endl;
    cout<<" 0 Telefonbuch beenden!!!"<<endl;
    cout<<endl;
    
    int eingabe;
    cin>>eingabe;
    
    if (eingabe == '0') 
    {
        cout<<"ende"<<endl;
        return 0;
    }

    switch(eingabe)
    {
        case 1:
            cout<<"Name einfuegen ist noch pendent!"<<endl;
            break;
        case 2:
            cout<<"Name suchen ist noch pendent!"<<endl;
            break;
        case 3:
            cout<<"Name loeschen ist noch pendent!"<<endl;
            break;
        case 4:
            cout<<"Telefonbuch sortiert ausgeben ist noch pendent!"<<endl;
            break;
        case 5:
            cout<<"Telefonbuch speichen ist noch pendent!"<<endl;
            break;
        case 6:
            cout<<"Telefonbuch laden ist noch pendent!"<<endl;
            break;
        default:
            cout<<"Wollten Sie nicht eine Zahl zwischen 1 und 6 eingeben"<<endl;
    
    }    

} 

/*Kompileraufruf und Programmstart
g++ -Wall -o shelleingabe_telefonbuch shelleingabe_telefonbuch.cpp
./shelleingabe_telefonbuch
*/
