// Einige Versuche und Test zu boolschen Variablen
#include <iostream>
using namespace std;

int main (void)
{
  cout << endl << "Programm booltest"<<endl;
  cout<<"------------------------------"<<endl;

  bool mybool1 = true;
  bool mybool2 = false;
  cout <<"mybool1: "  << mybool1 << endl;
  cout <<"mybool2: "  << mybool2 << endl << endl;

  int a=5;
  bool is_positive = (a>0);
  cout <<"a= "  << a << endl;
  cout <<"is_positive (a>0): "  << is_positive << endl;

  a=-5;
  is_positive = (a>0);
  cout <<"a= "  << a << endl;
  cout <<"is_positive (a>0): "  << is_positive << endl << endl;

  int j = 1900;
  bool is_schaltjahr;
  is_schaltjahr = (((j % 4 == 0) && (j % 100 != 0)) || (j % 400 == 0));
  // (Ist durch 4 teilbar) UND (Ist nicht durch 100 teilbar) ODER (Ist durch 400 teilbar) 
  // dann ist boolvariable is_schaltjahr true
  // 1900 ist kein Schaltjahr aber 2000 ist ein Schaltjahr
 /* Schaltjahrtest && ... logisches UND
                    ||     logisches ODER
                    ==     Gleichheit
                    %      Restbildungsoperator (Der R. berechnet den Rest einer
                           Division des ersten Operanden durch den zweiten. Wenn
                           bsp ein Integer-Wert durch 2 geteilt wird und sich kein
                           Rest ergibt handelt es sich um eine gerade Zahl.)*/
  
  cout <<"j= " << j << endl;
  cout <<"Ist j ein Schaltjahr? " << is_schaltjahr << endl;

  return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o booltest booltest.cc
./booltest
*/
