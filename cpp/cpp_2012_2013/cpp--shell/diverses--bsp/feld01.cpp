#include <iostream>
#define ANZAHL 50


int main ()
{
  int eingaben[ANZAHL], i;
  
  for (i = 0; i < ANZAHL; i++)
    eingaben[i] =0;  // jedes element mit 0 initialisieren

  for (i = 0; i < ANZAHL; i++)  // Werte einlesen
  {
      std::cout << "Element: " << i << "  ";
      std::cin >> eingaben[i];
      
      if (eingaben[i] == 0)
      {
        std::cout << "---------------- Ihre Eingabe: -----------------" << std::endl;
        break;
      }
  }
  
  for (i = 0; i < ANZAHL; i++)
  {
    if (eingaben[i] !=0) // nur dann wenn der wert nicht null ist, diese ...
      std::cout << "Element: " << i << " = " << eingaben[i] << std::endl;
    else   // Abbruch
      break;
  }
  
  return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o feld01 feld01.cpp
./feld01
*/
