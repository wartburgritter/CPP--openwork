// Namensraum std ohne Bereichsoperator
using namespace std;

// Stream-Header fuer Ausgaben
#include <iostream>

// Strings
#include <string>

// Password-Zugriff
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

// -----------------------------------------------
// Deklaration der Klasse
// -----------------------------------------------
class Benutzer
{
private:
  string login;
  string echterName;
  long   gruppenId;
  bool   init(uid_t _benutzerId);

public:
  Benutzer() :
    gruppenId(0) {}
  Benutzer(const string& _benutzerName)
    { init(_benutzerName); }
  Benutzer(uid_t _benutzerId)
    { init(_benutzerId); }
  bool init(const string& _benutzerName);
  bool setzeAufAktuellen();
  void ausgeben() const;
};

// -----------------------------------------------
// Initialisierung mit Benutzer-Id
// -----------------------------------------------
bool Benutzer::init(uid_t _benutzerId)
{
  // Ausgabevariable deklarieren
  struct passwd* benutzer_info = 0;

  // Benutzer-Info holen
  benutzer_info = getpwuid(_benutzerId);

  // Ungleich 0: Benutzer existiert
  if (benutzer_info)
  {
    // Daten kopieren und ausgeben
    login = benutzer_info->pw_name;
    echterName = benutzer_info->pw_gecos;
    gruppenId = benutzer_info->pw_gid;
    cout << "Benutzer " << login
       << " heisst " << echterName << endl;
  }
  else
  {
    // Fehler melden
    cerr << "Benutzer mit Id " << _benutzerId
       << " nicht gefunden!" << endl;
    login = "";
    echterName = "";
    gruppenId = 0;
    return false;
  }

  return true;
}

// -----------------------------------------------
// Initialisierung mit Namen
// -----------------------------------------------
bool Benutzer::init(const string& _benutzerName)
{
  // Ausgabevariable deklarieren
  struct passwd* benutzer_info = 0;

  // Benutzer-Info holen
  benutzer_info = getpwnam(_benutzerName.c_str());

  // Ungleich 0: Benutzer existiert
  if (benutzer_info)
  {
    // Daten kopieren und ausgeben
    login = _benutzerName;
    echterName = benutzer_info->pw_gecos;
    gruppenId = benutzer_info->pw_gid;
    cout << "Benutzer " << login
       << " heisst " << echterName << endl;
  }
  else
  {
    // Fehler melden
    cerr << "Benutzer " << _benutzerName
       << " nicht gefunden!" << endl;
    gruppenId = 0;
    return false;
  }

  return true;
}

// -----------------------------------------------
// Setze auf interaktiven Benutzer
// -----------------------------------------------
inline bool Benutzer::setzeAufAktuellen()
{
  return init(getuid());
}

// -----------------------------------------------
// Gib Daten aus
// -----------------------------------------------
void Benutzer::ausgeben() const
{
  cout << "Benutzer:    " << login << endl;
  cout << "Echter Name: " << echterName << endl;
  cout << "Gruppe:      " << gruppenId << endl;
  cout << endl;
}

// -----------------------------------------------
// Hauptprogramm
// -----------------------------------------------
int main()
{
  bool ergebnis;

  // Standardkonstruktion
  Benutzer u;

  // Konstruktor mit Argument
  // UID 0 entspricht root
  Benutzer root(0);

  cout << "Leerer Benutzer: " << endl;
  u.ausgeben();

  cout << "Objekt mit Root: " << endl;
  root.ausgeben();

  ergebnis = u.init("markus");
  if (ergebnis)
  {
    cout << "Initialisierter Benutzer markus: " << endl;
    u.ausgeben();
  }

  ergebnis = u.setzeAufAktuellen();
  if (ergebnis)
  {
    cout << "Aktueller Benutzer: "<< endl;
    u.ausgeben();
  }

  return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o 05_Benutzer 05_Benutzer.cc
./05_Benutzer
*/
