/* Datei main.cpp
   In dieser Datei wird eine Beispielanwendung der Klasse Auto implementiert.
 */

// binde die Beschreibung von Auto ein
#include "qsrechteck.h"
#include <iostream>
//using namespace std;

int main(void)
{
        // Lege ein Auto mit dem Standard-Konstruktor an.
        Qsrechteck qs1;
        // konstruktor schreiben, dass hoehe und breite gleich uebergeben werden koennen.
        
        
        
        
        ////char* s;

        std::cout << "Hier wird ein Querschnitt getestet!" << std::endl;

        std::cout << "Breite ist: " << qs1.getBreite() << std::endl;
        std::cout << "Hoehe ist: " << qs1.getHoehe() << std::endl;
        std::cout << "Flaeche ist: " << qs1.getFlaeche() << std::endl;

        

        qs1.setBreite(5);
        qs1.setHoehe(10);
        
        Qsrechteck qs2;
        qs2.setBreite(2);
        qs2.setHoehe(3);

        
        // teste die Methoden der Instanz a von Auto
        std::cout << "Breite ist: " << qs1.getBreite() << std::endl;
        std::cout << "Hoehe ist: " << qs1.getHoehe() << std::endl;
        std::cout << "Flaeche ist: " << qs1.getFlaeche() << std::endl;
        std::cout << "Flaeche ist: " << qs2.getFlaeche() << std::endl;
        std::cout << "Ix ist: " << qs1.getTraegmomx() << std::endl;
        std::cout << "Iy ist: " << qs1.getTraegmomy() << std::endl;
        std::cout << "Ix ist: " << qs2.getTraegmomx() << std::endl;
        std::cout << "Iy ist: " << qs2.getTraegmomy() << std::endl;
        
        ////s = a.getFabrikat();
        ////cout << "Fabrikat ist: " << (s==0 ? "<nicht gesetzt>" : s)<< endl;

        std::cout << "--------------------------------" << std::endl;

/*
	// Leistung jetzt 10KW
        a.setLeistung(10); 
        // Fabrikat jetzt auf Volkswagen ändern
        a.setFabrikat("Volkswagen"); 
        // ... und gleich nocheinmal auf VW setzen - Was steht jetzt in Auto?
        a.setFabrikat("VW"); 

        cout << "Leistung ist: " << a.getLeistung() << endl; // gibt den Wert 10 aus
        cout << "Fabrikat ist: " << a.getFabrikat() << endl; // gibt den Wert VW aus
*/
        std::cout << std::endl << "...beende main...." << std::endl;

        // Rückgabe 0 an das Betriebssystem soll heißen, erfolgreich beendet
        return 0;
}


/*Kompileraufruf und Programmstart
g++ -Wall -o qsberechnung qsberechnung.cpp qsrechteck.cpp
./qsberechnung
*/
