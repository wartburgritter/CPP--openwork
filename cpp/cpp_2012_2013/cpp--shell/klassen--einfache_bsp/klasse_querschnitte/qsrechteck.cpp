/* Datei qsrechteck.h
   In dieser Datei werden die Methoden der Klasse implementiert/umgesetzt.
*/

#include <iostream>   // für cout
#include <string>     // für strlen, strcpy

#include "qsrechteck.h"


using namespace std;


void Qsrechteck::setBreite(int b)
{      
  this->breite = b;
}

int Qsrechteck::getBreite()
{       
  return breite;
}

void Qsrechteck::setHoehe(int h)
{      
  this->hoehe = h;
}

int Qsrechteck::getHoehe()
{       
  return hoehe;
}
 
int Qsrechteck::getFlaeche()
{       
 //flaeche = this->breite * this->hoehe;  //i.o.
 flaeche = breite * hoehe;  //i.o.!!!
 return flaeche;   //einige
}

int Qsrechteck::getTraegmomx()
{       
 //flaeche = this->hoehe * this->breite;  //i.o.
 traegmomx = breite * hoehe * hoehe * hoehe * 1/12;  //i.o.!!!
 return traegmomx;   //einige
}

int Qsrechteck::getTraegmomy()
{       
 //flaeche = this->hoehe * this->breite;  //i.o.
 traegmomy = hoehe * breite * breite * breite * 1/12;  //i.o.!!!
 return traegmomx;   //einige
}

/*************************************************************************
   Eine Methode, mit der man das Fabrikat des Autos setzen kann 
   Parameter:
                einFabrikat - die neue Fabrikatsbezeichnung
*/
////
/*
void Auto::setFabrikat(char* einFabrikat)
{
        // nur wenn wirklich ein Name übergeben wurde
        if (einFabrikat != 0)
        {
                // falls ein altes Fabrikat existierte, lösche dieses
                if (fabrikat != 0)
                        delete fabrikat; // gib Speicher frei
                // stelle Speicher für das neue Fabrikat bereit (+ 1 für Nullbyte '\0')
                fabrikat = new char [ strlen(einFabrikat) + 1];
                if (fabrikat != 0)
                        // Speicher noch ausreichend gewesen, deshalb
                        // kopiere das Fabrikat, damit man von außerhalb diese Eigenschaft 
                        // nicht über den Pointer modifizieren kann
                        strcpy(fabrikat, einFabrikat);
        }
}
*/
/*************************************************************************
   Eine Methode, mit der man das Fabrikat des Autos auslesen kann. 
   Return:
                das Fabrikat des Autos oder 0 (NULL), falls keines gesetzt
                (genauer: einen Pointer auf die intern gespeicherte (gekapselte) Eigenschaft!?) */
/*
char* Auto::getFabrikat()
{
        return fabrikat;
}
*/
////
/*************************************************************************
   Konstruktor zum Erzeugen eines Autos. Es werden dabei die Leistung des Autos
   standardmäßig auf 74KW und das Fabrikat auf 0 (NULL) gesetzt.
*/
Qsrechteck::Qsrechteck()
{       
      //   leistung=74;
        ////fabrikat=0; // heißt hier: nichts angegeben
}

/*************************************************************************
   Der Destruktor eines Autos wird automatisch aufgerufen, wenn das Programm 
   beendet wird. 
*/
Qsrechteck::~Qsrechteck()
{       // zu Testzwecken: mache hier eine Ausgabe, damit man den Aufruf sehen kann
        cout << "Destructor!!!" << endl;

        // gib angefoderte Ressourcen frei
        ////delete fabrikat;
        // zur Sicherheit: Setze das Attribut auf 0 (NULL), damit eventuell mehrfacher Aufruf des 
        // Destruktors nicht zum Laufzeitfehler wird
        ////fabrikat = 0; 
} 
