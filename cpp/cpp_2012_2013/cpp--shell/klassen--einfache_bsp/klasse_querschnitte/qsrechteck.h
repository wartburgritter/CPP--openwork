/* Datei qsrechteck.h
  In dieser Datei befindet sich die Beschreibung/Deklaration der Klasse.
*/

/* Damit eine mehrfache Deklaration der Klasse durch mehrfaches Einbinden 
   dieser Headerdatei verhindert wird, wird über die Präprozessor-Anweisung
   "#define AUTO_H" ein Wert gesetzt, den man mittels "ifdef" bzw.
   "ifndef" abprüfen kann. Dieser gibt Auskunft, ob diese Headerdatei bereits 
   eingebunden wurde.
*/
#ifndef QSRECHTECK_H
#define QSRECHTECK_H

/*
    Die Klasse Rechteckt
*/
class Qsrechteck
{
        private:
                int breite;
                int hoehe;
                int flaeche;
                int traegmomx;
                int traegmomy;
                /* Eine weitere Eigenschaft, die das Fabrikat darstellt.
                 */
                ////char* fabrikat;

        // öffentlicher Teil der Klasse (alles darunter angegebene kann von 
        // jedem verwendet werden)
        public:
                Qsrechteck();
                ~Qsrechteck();

                void setBreite(int b);
                int getBreite();
                void setHoehe(int h);
                int getHoehe();
                int getFlaeche();
                int getTraegmomx();
                int getTraegmomy();
};

#endif