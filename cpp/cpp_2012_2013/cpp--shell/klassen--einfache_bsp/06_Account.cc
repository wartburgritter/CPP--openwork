#include "06_Account.hh"
#include <iostream>

// sobald in der Klasse Ausgabeanweisungen sind
// ist die Klasse fuer eine GUI unbrauchbar
// extra Klasse Ausgabe fuer die Konsole.

using namespace std;

Account::Account(double b)
{
  balance = b;
}

void Account::einzahlen(double amt)
{
  if (amt>0)
    balance += amt;
  else
    falscheEingabe();
  return;
}

void Account::auszahlen(double amt)
{
  if (amt>0)
    balance -= amt;
  else
    falscheEingabe();
  return;
}

double Account::getBalance() const
{
  return balance;
}

void Account::berechneZins()
{
  if (balance > 0)
    {
      cout << "Habenzins 5 %" << endl;
      balance = balance * 1.05;
    }
  else
    {
      cout << "Sollzins 15 %" << endl;
      balance = balance * 1.15;
    }
}

void Account::berechneVariablenZins(double amt)
{
      balance = balance + balance * amt * 0.01;
      //PENDENT mit eingegebenen zins berechnen und dann jahre eingeben!!!
}

void Account::falscheEingabe()  // ist eigentlich keine Klassenmethode account
{
        cout << "Fehlerhafte Eingabe" << endl;
}