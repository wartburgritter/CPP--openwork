#include <iostream>
#include "04_Datum.hh"
using namespace std;

Datum heutigesGlobalesDatum__;    //globales Objekt

int main()
{
  cout<<"Beginn Main"<< endl;

  cout << "Heutiges Globales Datum ";
    heutigesGlobalesDatum__.ausgeben();

  //Konstruktor der Klasse Datum wird ueberladen, je nach Anzahl Parameter
  //wird der richtige Konstruktor durch den Compiler gew�hlt
  Datum heuteDatum;             //Aufruf Standardkonstruktor heutiges Datum
  Datum osternDatum(4,4,1999);  //Aufruf Konstruktor Tag, Monat, Jahr
  Datum tagMonatDatum(5,10);    //Aufruf Konstruktor Tag im aktuellen Monat und Jahr
  Datum tagDatum(20);           //Aufruf Konstruktor Tag im aktuellen Monat und Jahr
  Datum kopieVonOsternDatum(osternDatum); //Aufruf Kopiekonstruktor
  Datum kopieVonHeuteDatum = heuteDatum;  //Aufruf Kopiekonstruktor mittels Zuweisungsoperator.

  cout << "Heute ist der ";
    heuteDatum.ausgeben();
  cout << "Ostern ist am ";
    osternDatum.ausgeben();
  cout << "Ein Datum mit Tag und Monat im nicht initialisierten Jahr ";
    tagMonatDatum.ausgeben();
  cout << "Ein Datum mit Tag im aktuellen Monat ist der ";
    tagDatum.ausgeben();
  cout << "Eine Kopie von Ostern sollte Ostern ausgeben ";
    kopieVonOsternDatum.ausgeben();
  cout << "Eine Kopie von heute sollte heute ausgeben ";
    kopieVonHeuteDatum.ausgeben();

  cout<<"Beende Main"<< endl;
  return 0;
}

/*Kompileraufruf und Programmstart
g++ -Wall -o 04_DatumTest 04_Datum.cc 04_DatumTest.cc
./04_DatumTest
*/
