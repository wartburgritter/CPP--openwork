// Datei 04_datum.cc
//
// Datumsklasse zur Illustration von Konstruktoren und Destruktoren
// (C) 2000, T. Wieland.

#include <iostream>
#include <ctime>
#include "04_Datum.hh"
using namespace std;

////////  K O N S T R U K T O R E N ///////////
// --------------------------------------------
// Standardkonstruktor
// --------------------------------------------
Datum::Datum()
{
  cout << "Standardkonstruktor" << endl;
  setzeAufHeute();
}

// --------------------------------------------
// Destruktuor
// --------------------------------------------
Datum::~Datum()
{
 cout << "Destruktor" << endl;
}




// --------------------------------------------
// allgemeiner Konstruktor
// --------------------------------------------
Datum::Datum(unsigned int _t,
             unsigned int _m,
             unsigned int _j)
{
  setze(_t, _m, _j);
  cout << "allgemeiner Konstruktor" << endl;
}


// --------------------------------------------
//allgemeiner Konstruktor mit Tag und Monat und mit Initialisierungsliste
// --------------------------------------------
Datum::Datum(unsigned int _t,
             unsigned int _m): t(_t), m(_m)
{
  cout << "Konstruktor Tag und Monat mit Initialisierungsliste" << endl;
}

// --------------------------------------------
// allgemeiner Konstruktor nur mit Tag
// --------------------------------------------
Datum::Datum(unsigned int _t)
{
  setzeAufHeute();
  setze(_t, m, j); // mit Bereichspruefung!
  cout << "allgemeiner Konstruktor nur mit Tag" << endl;
}

// --------------------------------------------
// Kopierkonstruktor
// --------------------------------------------
Datum::Datum(const Datum& _datum) :
  t(_datum.t), m(_datum.m), j(_datum.j)
{
}

//////// M E T H O D E N //////////////////////
// --------------------------------------------
// Schreibmethode
// --------------------------------------------
void Datum::setze(unsigned int _t,
                  unsigned int _m,
                  unsigned int _j)
{
  unsigned int altesJahr = j;
  j = _j;

  unsigned int monate[] = {31,28,31,30,31,30,31,31,30,31,30,31};

  if (istSchaltjahr())
    monate[1] = 29;

  if (_m < 1 || _m > 12 ||
      _j <= 1980 || _j >= 2299 ||
      _t < 1 || _t > monate[_m-1])
  {
    cerr << "Datum ungueltig!" << endl;
    j = altesJahr;
    return;
  }

  m = _m;
  t = _t;
}

// --------------------------------------------
// setze heutiges Datum
// --------------------------------------------
void Datum::setzeAufHeute()
{
  time_t now = time(NULL);
  tm* z = localtime(&now);

  j = z->tm_year + 1900;
  m =  z->tm_mon +1;
  t = z->tm_mday;
}

// --------------------------------------------
// gib Datum auf cout aus
// --------------------------------------------
void Datum::ausgeben() const
{
  if (t < 10)
    cout << "0";
  cout << t << ".";

  if (m < 10)
    cout << "0";

  cout << m << "." << j << endl;
}

// --------------------------------------------
// bestimme, ob Datum im Schaltjahr
// --------------------------------------------
bool Datum::istSchaltjahr()
{
  return (((j % 4 == 0) && (j % 100 != 0)) || (j % 400 == 0));
}


