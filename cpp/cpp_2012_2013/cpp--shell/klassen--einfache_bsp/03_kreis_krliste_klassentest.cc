// Minimale Implementation der Klasse Kreis mit dem Attribut radius
// und der Klasse Krliste mit Referenz als Attribut der Klasse

#include <iostream>
using namespace std;

//Deklaration der Klasse Kreis
class Kreis
{
private:
  unsigned long radius;  // Kreis ist durch seinen Radius definiert

public:
  Kreis();        // Standardkonstruktor
  ~Kreis();       // Destruktor
  bool setRadius(unsigned long _radius);
  unsigned long getRadius();
};


//Deklaration der Klasse Krliste
class Krliste
{
private:
  const unsigned int MAX_RADIUS;
  Kreis& kreisreferenz;                // Referenz kreisreferenz vom Typ Kreis

public:
  Krliste(Kreis& _kreisreferenz);     // Standardkonstruktor erwartet ein Parameter
  ~Krliste();                         // Destruktor
};


//Hauptprogramm
int main (void)
{
  Kreis meinbspkreis;
  cout << "Radius von meinbspkreis: " << meinbspkreis.getRadius() << endl;
  meinbspkreis.setRadius(1000);
  cout << "Radius von meinbspkreis: " << meinbspkreis.getRadius() << endl;
  meinbspkreis.setRadius(20);
  cout << "Radius von meinbspkreis: " << meinbspkreis.getRadius() << endl;

  Krliste krelistenmember1(meinbspkreis);
  //Krliste krelistenmember2;  //funktioniert nicht, da der Konstruktor ein Objekt der Klasse Kreis erwartet

 //Nun was kann ich mit der Referenz vom Typ Kreis auf das Objekt meinbspkreis nun anfangen ???!!!!
  return 0;
}

//Definition der Klasse Kreis
//Standardkonstruktor
Kreis::Kreis()
{
  setRadius(99);
}

//Destruktor
Kreis::~Kreis()
{
}

// Definition der Methode setRadius
bool Kreis::setRadius(unsigned long _radius)
 {
  radius=_radius;
  return true;
 }

// Definition der Methode getRadius
unsigned long Kreis::getRadius()
 {
  return radius;
 }


//Definition der Klasse Krliste
//Standardkonstruktor
Krliste::Krliste(Kreis& _kreisreferenz): MAX_RADIUS(9999), kreisreferenz(_kreisreferenz)
{
}

//Destruktor
Krliste::~Krliste()
{
}


/*Kompileraufruf und Programmstart
g++ -Wall -o 03_kreis_krliste_klassentest 03_kreis_krliste_klassentest.cc
./03_kreis_krliste_klassentest
*/

