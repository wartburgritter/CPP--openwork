// bsp aus dem kleinen c++ buechlein

#include <iostream>
#include <cmath>
#include <cstdlib>

#include "06_Account.hh"

using namespace std;

int main(int argc, char *argv[])
{
 Account  account(0.0);
 char     action;
 double   amount;
 double   years;

if (argc > 1)
  account.einzahlen(atof(argv[1]));

while (true)
{
  cout << endl
       << "Der Saldo ist "
       << account.getBalance()
       << endl;

  cout << "Auswahl: e, a, z, y oder q: ";
  cin  >> action;

  switch (action)
  {
    case 'e':
      cout << "Einzahlungsbetrag angeben: ";
      cin  >> amount;
      account.einzahlen(amount);
      break;

    case 'a':
      cout << "Auszahlungsbetrag angeben: ";
      cin  >> amount;
      account.auszahlen(amount);
      break;

    case 'z':
      cout << "Zinsberechnung: ";
      account.berechneZins();
      break;

    case 'y':
      cout << "Zinsberechnung mit variablen Zins!"
           << endl
           << "Wie hoch ist der Zins: ";
      cin  >> amount;
      if (amount > 0)
      {
        cout << "Der Zinssatz beträgt "
             << amount
             << " %."
             << endl;
        cout << "Wieviele Jahre Zins: ";
        cin  >> years;
        if (years > 0)
        {
          cout << "Zinsen für "
               << years
               << " Jahre ausrechnen."
               << endl;
          account.berechneVariablenZins(amount);
        }
      }
      else
      {
        account.falscheEingabe();
      }

      break;

    case 'q':
      exit(0);

    default:
      account.falscheEingabe();
  }
}

return 0;
}

/*Kompileraufruf und Programmstart
g++ -Wall -o 06_AccountTest 06_Account.cc 06_AccountTest.cc
./06_AccountTest
./06_AccountTest 1000
Der Parameter hinter dem Programmname wird als Guthaben gut geschrieben.
*/
