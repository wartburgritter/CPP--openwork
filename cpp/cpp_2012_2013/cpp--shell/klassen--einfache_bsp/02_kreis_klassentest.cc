// Minimale Implementation der Klasse Kreis mit dem Attribut radius

#include <iostream>
using namespace std;

//Deklaration der Klasse Kreis
class Kreis
{
private:
  unsigned long radius;  // Kreis ist durch seinen Radius definiert

public:
  Kreis();        // Standardkonstruktor
  ~Kreis();       // Destruktor
  bool setRadius(unsigned long _radius);
  unsigned long getRadius();
};


//Hauptprogramm
int main (void)
{
  Kreis k;
  cout << "Radius von k: " << k.getRadius() << endl;
  k.setRadius(10000);
  cout << "Radius von k: " << k.getRadius() << endl;
  k.setRadius(20);
  cout << "Radius von k: " << k.getRadius() << endl;

  Kreis k2 = k;
  // Es ist kein Kopierkonstruktor vorhanden, daher wird der automatisch
  // generrierte Kopierkonstruktor aufgerufen. Siehe Seite 103.
  cout << "Radius von k2: " << k2.getRadius() << endl;


  return 0;
}

//Definition der Klasse Kreis
//Standardkonstruktor
Kreis::Kreis()
{
  setRadius(99);
}

//Destruktor
Kreis::~Kreis()
{
}

// Definition der Methode setGeschwindigkeit
bool Kreis::setRadius(unsigned long _radius)
 {
  radius=_radius;
  return true;
 }

// Definition der Methode getGeschwindigkeit
unsigned long Kreis::getRadius()
 {
  return radius;
 }


/*Kompileraufruf und Programmstart
g++ -Wall -o 02_kreis_klassentest 02_kreis_klassentest.cc
./02_kreis_klassentest
*/

