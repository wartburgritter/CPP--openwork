/*Deklaration der Klasse Raumfahrzeug
*************************************/

class Raumfahrzeug
{
private:
  unsigned long geschw;

public:
  Raumfahrzeug();       // Konstruktor
  ~Raumfahrzeug();      // Destruktor
  bool setGeschwindigkeit(unsigned long _tempo);
  unsigned long getGeschwindigkeit();
};

