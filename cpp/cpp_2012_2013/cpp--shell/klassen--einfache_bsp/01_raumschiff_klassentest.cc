// Minimale Implementation des Systems der Klassen
// Es gibt eine Klasse Raumschiff. Es gibt nur ein
// Attribut, das ist die Geschwindigkeit.

#include <iostream>
#include "01_raumschiff.hh"
using namespace std;

int main (void)
{
  cout << endl << " Programm Klasse Raumfahrzeug"<<endl;
  cout<<"------------------------------"<<endl;

  Raumfahrzeug ufo;

  ufo.setGeschwindigkeit(10000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;
  ufo.setGeschwindigkeit(90000);
  cout << "Ufo fliegt "
       << ufo.getGeschwindigkeit()
       << " km/s" << endl;

  //ufo.geschw = 5000; // Compilererror, da geschw privates Attribut der Klasse Raumschiff


  cout << "beende main" << endl;
  return 0;
}

/*Kompileraufruf und Programmstart
  g++ -Wall -o 01_raumschiff_klassentest 01_raumschiff.cc 01_raumschiff_klassentest.cc
  ./01_raumschiff_klassentest
  
*/

