import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    tools: commonTools

    Label {
        id: label
        color: "#2fcfcd"
        text: "Sali Bernd!!!"
        font.pixelSize: 50
         anchors.centerIn: parent
        visible: false
    }

    Button{
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: label.bottom
            topMargin: 10
        }
        text: qsTr("Bitte Klicken!")
        onClicked: label.visible = true
    }
    Label {
        id: label2
        color: "#2fcfcd"
        text: "Ich bleibe hier!"
        font.pixelSize: 50
        anchors.centerIn: parent
    }
}
