#include <QtGui/QApplication>
#include "helloworldquit.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HelloWorldQuit w;
#if defined(Q_WS_S60)
    w.showMaximized();
#else
    w.show();
#endif
    
    return a.exec();
}
