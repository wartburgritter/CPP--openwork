#ifndef HELLOWORLDQUIT_H
#define HELLOWORLDQUIT_H

#include <QtGui/QWidget>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>


class HelloWorldQuit : public QWidget
{
    Q_OBJECT
    
public:
    HelloWorldQuit(QWidget *parent = 0);
    ~HelloWorldQuit();

private:
    QLabel *helloLabel;
    QPushButton *quitButton;

};

#endif // HELLOWORLDQUIT_H
