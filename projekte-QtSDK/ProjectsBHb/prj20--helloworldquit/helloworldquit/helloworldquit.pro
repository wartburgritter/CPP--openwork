#-------------------------------------------------
#
# Project created by QtCreator 2013-03-04T21:42:33
#
#-------------------------------------------------

QT       += core gui

TARGET = helloworldquit
TEMPLATE = app


SOURCES += main.cpp\
        helloworldquit.cpp

HEADERS  += helloworldquit.h

CONFIG += mobility
MOBILITY = 

symbian {
    TARGET.UID3 = 0xe599156c
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/helloworldquit/bin
    INSTALLS += target
}

contains(MEEGO_EDITION,harmattan) {
    icon.files = helloworldquit80.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    INSTALLS += icon
}

contains(MEEGO_EDITION,harmattan) {
    desktopfile.files = helloworldquit_harmattan.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
}

