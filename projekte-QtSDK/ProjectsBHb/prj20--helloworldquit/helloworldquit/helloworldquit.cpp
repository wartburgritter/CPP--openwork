#include "helloworldquit.h"

HelloWorldQuit::HelloWorldQuit(QWidget *parent)
    : QWidget(parent)
{
    helloLabel = new QLabel("Hello Hugo!!", this);
    helloLabel->setGeometry(0, 0, 300, 100);
    helloLabel->setAlignment(Qt::AlignCenter);
    helloLabel->setFont(QFont("Arial", 20, QFont::Light));

    quitButton = new QPushButton("Close", this);
    quitButton->setGeometry(25, 100, 250, 100);
    quitButton->setFont(QFont("Arial", 30, QFont::Black));

    QObject::connect (quitButton, SIGNAL(clicked()), this, SLOT(close()));

    resize(300, 225);                   // breite, hoehe
    setWindowTitle(tr("Hello Hugo App"));
}

HelloWorldQuit::~HelloWorldQuit()
{
    
}
