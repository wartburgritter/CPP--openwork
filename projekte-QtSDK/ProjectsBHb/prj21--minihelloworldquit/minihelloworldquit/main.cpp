#include <QtGui/QMainWindow>
#include <QtGui/QApplication>
#include <QtGui/QPushButton>
#include <QtGui/QFont>
#include <QtGui/QStatusBar>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QMainWindow window;
    //QWidget window;

    QPushButton button("Close Hallo N9!", &window);
    button.setGeometry(128, 50, 600, 250);
    button.setFont(QFont("Arial", 45, QFont::Black));
    QObject::connect (&button, SIGNAL(clicked()), &window, SLOT(close()));


    window.statusBar()->showMessage( "Dies hier ist doch aber die Statusbar" );

    window.resize(854, 480); // test on desktop
    //window.show();
    window.showFullScreen();
    return app.exec();
}
