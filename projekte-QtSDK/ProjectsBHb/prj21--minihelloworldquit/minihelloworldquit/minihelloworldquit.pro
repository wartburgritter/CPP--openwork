#-------------------------------------------------
#
# Project created by QtCreator 2013-03-04T21:42:33
#
#-------------------------------------------------

QT       += core gui

TARGET = minihelloworldquit
TEMPLATE = app


SOURCES += main.cpp

CONFIG += mobility


contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/minihelloworldquit/bin
    INSTALLS += target
}


contains(MEEGO_EDITION,harmattan) {
    desktopfile.files = minihelloworldquit_harmattan.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
}

contains(MEEGO_EDITION,harmattan) {
    icon.files = minihelloworldquit80.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    INSTALLS += icon
}
