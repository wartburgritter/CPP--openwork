#-------------------------------------------------
#
# Project created by QtCreator 2011-01-16T18:23:01
#
#-------------------------------------------------

QT       += core gui maemo5

TARGET = weightgraph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    weightdata.cpp \
    editwindow.cpp \
    weightstatsview.cpp \
    weightgraphview.cpp \
    settings.cpp \
    settingswindow.cpp

HEADERS  += mainwindow.h \
    weightdata.h \
    editwindow.h \
    weightview.h \
    weightspinbox.h \
    weightstatsview.h \
    weightgraphview.h \
    settings.h \
    settingswindow.h

FORMS    += mainwindow.ui

CONFIG += mobility console
MOBILITY = 

RESOURCES += \
    res.qrc

unix {
  INSTALLS += target desktop icon64
  target.path = /usr/bin
  desktop.path = /usr/share/applications/hildon
  desktop.files += maemofiles/weightgraph.desktop
#  icon48.path = /usr/share/icons/hicolor/48x48/apps
#  icon48.files += maemofiles/48/weightgraph.png
# icon64.path = /usr/share/icons/hicolor/64x64/apps
  icon64.path = /usr/share/icons/hicolor/scalable/apps
  icon64.files += maemofiles/64/weightgraph.png
#  iconscalable.path = /usr/share/icons/hicolor/scalable/apps
#  iconscalable.files += maemofiles/64/weightgraph.png
}

symbian {
  TARGET.UID3 = 0xecc772d7
  # TARGET.CAPABILITY +=
  TARGET.EPOCSTACKSIZE = 0x14000
  TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog

