#ifndef WEIGHTGRAPHVIEW_H
#define WEIGHTGRAPHVIEW_H

#include <QWidget>
#include <QKeyEvent>
#include "weightdata.h"
#include "settings.h"

class WeightGraphView : public QWidget
{
    Q_OBJECT
public:
  explicit WeightGraphView(WeightDataModel *wdm, const QString &id, QWidget *parent = 0);
  QSize sizeHint() const;
signals:
  void clicked();
public slots:
  void paintEvent(QPaintEvent *);
  void show();
  void update() {
//    grabZoomKeys(Settings::grabZoomKeys());
    QWidget::update();
  }

//  void grabZoomKeys(bool grab);
  void incPeriod() {
    if (period == 0)
      period = wdm->getWeights().first().date.daysTo(wdm->getWeights().last().date) + 1;
    else
      period++;
    update();
  }
  void decPeriod() {
    if (period == 0)
      period = wdm->getWeights().first().date.daysTo(wdm->getWeights().last().date) - 1;
    else
      period = qMax(1, period-1);
    update();
  }
  void setPeriod(int period) { this->period = period; update(); }
protected:
  void mousePressEvent(QMouseEvent *);
  void keyPressEvent(QKeyEvent* event);
private:
  QString id;
  WeightDataModel *wdm;
  int period;
};

#endif // WEIGHTGRAPHVIEW_H
