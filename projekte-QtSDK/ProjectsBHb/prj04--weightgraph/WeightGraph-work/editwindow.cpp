#include "editwindow.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QLabel>
#ifdef Q_WS_MAEMO_5
#include <QMaemo5DatePickSelector>
#endif
extern WeightDataModel *wdm;

EditWindow::EditWindow(QWidget *parent) :
    QMainWindow(parent), shown(false)
{
#ifdef Q_WS_MAEMO_5
  setAttribute(Qt::WA_Maemo5StackedWindow);
#endif

  setWindowTitle("WeightGraph");

  QWidget *centralContainer = new QWidget(parent);
  QHBoxLayout *horiz = new QHBoxLayout(centralContainer);

  weightView.setModel(wdm);
  weightView.setMinimumWidth(540);
  horiz->addWidget(&weightView);

  QWidget *rightContainer = new QWidget(centralContainer);
  QVBoxLayout *right = new QVBoxLayout(rightContainer);

  addButton.setText("Add");
  addButton.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  addButton.setFont(QFont("Arial", 35, QFont::Light));
  connect(&addButton, SIGNAL(clicked()), this, SLOT(addWeight()));
  right->addWidget(&addButton);

  removeButton.setText("Remove");
  removeButton.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  removeButton.setFont(QFont("Arial", 35, QFont::Light));
  connect(&removeButton, SIGNAL(clicked()), this, SLOT(removeSelected()));
  right->addWidget(&removeButton);

  editButton.setText("Edit");
  editButton.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  editButton.setFont(QFont("Arial", 35, QFont::Light));
  connect(&editButton, SIGNAL(clicked()), this, SLOT(editSelected()));
  right->addWidget(&editButton);

  horiz->addWidget(rightContainer);

  setCentralWidget(centralContainer);

  connect(weightView.selectionModel(),
          SIGNAL(selectionChanged(const QItemSelection &,const QItemSelection &)),
          this, SLOT(updateButtons()));
  updateButtons();

}
// Ab HIER MEIN WICHTIGES FENSTER !!!
AddWeightDialog::AddWeightDialog(QWidget *parent)
    : QDialog(parent)
{
  
  //setDialogTitle("Gewicht eingeben");
  
  setModal(true);
  QGridLayout *layout = new QGridLayout(this);

  QLabel *dateLabel = new QLabel("Date", this);
  //dateLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
  dateLabel->setFont(QFont("Arial", 30, QFont::Light));
  dateLabel->setAlignment(Qt::AlignCenter);
  layout->addWidget(dateLabel, 2, 0);

// /* das waere cool fuer harmatten
#ifdef Q_WS_MAEMO_5
  date = new QMaemo5ValueButton(this);
  QMaemo5DatePickSelector *picker = new QMaemo5DatePickSelector(this);
  picker->setCurrentDate(QDate::currentDate());
  date->setPickSelector(picker);
#else
  date = new QDateEdit(this);
  date->setDate(QDate::currentDate());
  date->setDisplayFormat("yyyy-MM-dd");
  date->setFont(QFont("Arial", 60, QFont::Light));
#endif

  layout->addWidget(date, 0, 0);

  QLabel *weightLabel = new QLabel("Weight", this);
  //weightLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
  weightLabel->setFont(QFont("Arial", 30, QFont::Light));
  weightLabel->setAlignment(Qt::AlignCenter);
  layout->addWidget(weightLabel, 2, 1);

  weight = new WeightSpinBox(this);
  weight->setFont(QFont("Arial", 50, QFont::Light));
  layout->addWidget(weight, 0, 1);

  QPushButton *cancelButton = new QPushButton("Cancel", this);
  cancelButton->setFont(QFont("Arial", 35, QFont::Light));
  connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
  layout->addWidget(cancelButton, 1, 0);

  QPushButton *addButton = new QPushButton("Add", this);
  addButton->setFont(QFont("Arial", 35, QFont::Light));
  connect(addButton, SIGNAL(clicked()), this, SLOT(accept()));
  layout->addWidget(addButton, 1, 1);
  
  QLabel *platzLabel = new QLabel("bhbs Platzhalter", this);
  platzLabel->setFont(QFont("Arial", 50, QFont::Light));
  platzLabel->setAlignment(Qt::AlignCenter);
  layout->addWidget(platzLabel, 3, 0, 1, 2);

}
WeightDataModel::DateWeight AddWeightDialog::getDateWeight()
{
  WeightDataModel::DateWeight ret;
#ifdef Q_WS_MAEMO_5
  ret.date = dynamic_cast<QMaemo5DatePickSelector*>(date->pickSelector())->currentDate();
#else
  ret.date = date->date();
#endif
  ret.weight = weight->value();
  return ret;
}

void EditWindow::updateButtons()
{
  if (weightView.selectionModel()->selection().indexes().size() == 1) {
    removeButton.setEnabled(true);
    editButton.setEnabled(true);
  }
  else {
    removeButton.setEnabled(false);
    editButton.setEnabled(false);
  }
}

extern WeightDataModel *wdm;
void EditWindow::addWeight()
{
  AddWeightDialog d;
  int code = d.exec();
  if (code == QDialog::Accepted) {
    WeightDataModel::DateWeight dw = d.getDateWeight();
    if (wdm->dateExists(dw.date)) {
      QMessageBox::warning(this, "Duplicate", "A weight for this date already exists.\n"
                                              "Only one weight per date allowed.");
      return;
    }
    wdm->setWeightForDate(dw);
    weightView.scrollTo(wdm->indexOfDate(dw.date), QAbstractItemView::PositionAtCenter);
  }
}

void EditWindow::removeSelected()
{
  int answer =
      QMessageBox::question(this, "Remove?", "Really remove selected entry?",
                            QMessageBox::No, QMessageBox::Yes);
  if (answer == QMessageBox::Yes) {
    QModelIndexList indexes = weightView.selectionModel()->selection().indexes();
    foreach(QModelIndex i, indexes)
      wdm->removeRows(i.row(), 1);
  }
}

void EditWindow::editSelected()
{
  QModelIndexList indexes = weightView.selectionModel()->selection().indexes();
  if (indexes.size() == 1)
    weightView.edit(wdm->index(indexes.first().row(), 1));
}

void EditWindow::show()
{
  QMainWindow::show();
  // scrollToBottom must be here: it will not scroll all the way
  // to the bottom when called in the constructor because it'll
  // receive the wrong window geometry while hidden.
  if (!shown) {
    weightView.scrollToBottom();
    shown = true;
  }
}
