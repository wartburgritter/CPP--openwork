#include "settingswindow.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QStringListModel>
#include <QPushButton>

//for debugging
#include <iostream>
#include <QDebug>

static QStringList timeIntervalStrings() {
  QStringList timeIntervals;
  timeIntervals.append("1 week");
  timeIntervals.append("1 month");
  timeIntervals.append("3 months");
  timeIntervals.append("6 months");
  timeIntervals.append("1 year");
  timeIntervals.append("all");
  return timeIntervals;
}
static int timeIntervalToIndex(int days) {
  switch(days) {
  case 0: return 5;
  case 7: return 0;
  case 30: return 1;
  case 90: return 2;
  case 180: return 3;
  case 365: return 4;
  default: Q_ASSERT(0 && "unknown time interval");
  }
  return 0; //unreachable
}
static int indexToTimeInterval(int index) {
  switch(index) {
  case 5: return 0;
  case 0: return 7;
  case 1: return 30;
  case 2: return 90;
  case 3: return 180;
  case 4: return 365;
  default: Q_ASSERT(0 && "unknown time interval index");
  }
  return 0; //unreachable
}

SettingsWindow::SettingsWindow(QWidget *parent) :
    QMainWindow(parent)
{
#ifdef Q_WS_MAEMO_5
  setAttribute(Qt::WA_Maemo5StackedWindow);
#endif

  setWindowTitle("WeightGraph");

  QWidget *rootContainer = new QWidget(this);
  QVBoxLayout *rootLayout = new QVBoxLayout(rootContainer);

  QWidget *topContainer = new QWidget(rootContainer);
  QGridLayout *topLayout = new QGridLayout(topContainer);

  QStringList units; units.append("kg"); units.append("lb");
  QStringListModel *weightUnitModel = new QStringListModel(units, topContainer);
#ifdef Q_WS_MAEMO_5
  weightUnit = new QMaemo5ValueButton("Unit", topContainer);
  weightUnit->setValueLayout(QMaemo5ValueButton::ValueUnderTextCentered);
  QMaemo5ListPickSelector *weightUnitSelector = new QMaemo5ListPickSelector(topContainer);
  weightUnitSelector->setModel(weightUnitModel);
  weightUnitSelector->setCurrentIndex(Settings::weightUnit() == "kg" ? 0 : 1);
  weightUnit->setPickSelector(weightUnitSelector);
  connect(weightUnit->pickSelector(), SIGNAL(selected(QString)),
          Settings::self(), SLOT(setWeightUnitAndSync(QString)));
#else
  weightUnit = new QComboBox(topContainer);
  weightUnit->setModel(weightUnitModel);
#endif

  topLayout->addWidget(weightUnit, 0, 0);

//  QWidget *spacer = new QWidget(topContainer);
//  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
//  topLayout->addWidget(spacer);

  QFrame *goalFrame = new QFrame(topContainer);
  goalFrame->setFrameShadow(QFrame::Sunken);
  goalFrame->setFrameStyle(QFrame::StyledPanel);
  goalFrame->setLineWidth(2);
  goalFrame->setMidLineWidth(2);
  QHBoxLayout *goalLayout = new QHBoxLayout(goalFrame);

  goalLayout->addWidget(new QLabel("Goal weight:", topContainer));

  goalMin = new WeightSpinBox(topContainer);
  goalMin->setValue(Settings::goalWeightMin());
  goalLayout->addWidget(goalMin);

  goalLayout->addWidget(new QLabel("-", topContainer));

  goalMax = new WeightSpinBox(topContainer);
  goalMax->setValue(Settings::goalWeightMax());
  goalLayout->addWidget(goalMax);
  topLayout->addWidget(goalFrame, 0, 1);

  grabZoomKeys = new QPushButton("Use zoom/volume keys to zoom", rootContainer);
  grabZoomKeys->setCheckable(true);
  grabZoomKeys->setChecked(Settings::grabZoomKeys());
  topLayout->addWidget(grabZoomKeys, 1, 0, 1, 2);

  rootLayout->addWidget(topContainer);

  QTabWidget *tabWidget = new QTabWidget(rootContainer);

  graphSettingsList = new QList<GraphSettingsWidget*>();

  QStringList graphIds; graphIds.append("Small"); graphIds.append("Big");
  foreach(QString id, graphIds) {
    GraphSettingsWidget *gsw = new GraphSettingsWidget(id, tabWidget);
    graphSettingsList->append(gsw);
    tabWidget->addTab(gsw, id+" graph");
  }

  rootLayout->addWidget(tabWidget);

  setCentralWidget(rootContainer);
}

void SettingsWindow::closeEvent(QCloseEvent *event)
{
  //Note: Weight unit not saved as it is saved on change.
  Settings::setGoalWeightMin(goalMin->value());
  Settings::setGoalWeightMax(goalMax->value());
  Settings::setGrabZoomKeys(grabZoomKeys->isChecked());
  foreach(GraphSettingsWidget *gsw, *graphSettingsList) {
    GraphSettings gs;
    gs.goalWeightEnabled = gsw->goalWeightEnabled;
#ifdef Q_WS_MAEMO_5
    QMaemo5ListPickSelector *picker =
        dynamic_cast<QMaemo5ListPickSelector*>(gsw->weightIntervalMode->pickSelector());
    gs.weightIntervalMode = (GraphSettings::WeightIntervalMode)picker->currentIndex();
#else
    gs.weightIntervalMode =
        (GraphSettings::WeightIntervalMode)gsw->weightIntervalMode->currentIndex();
#endif
    gs.weightIntervalMin = gsw->weightIntervalMin->value();
    gs.weightIntervalMax = gsw->weightIntervalMax->value();
#ifdef Q_WS_MAEMO_5
    QMaemo5ListPickSelector *picker2 =
        dynamic_cast<QMaemo5ListPickSelector*>(gsw->defaultTimeInterval->pickSelector());
    gs.defaultTimeInterval = indexToTimeInterval(picker2->currentIndex());
#else
    gs.defaultTimeInterval
        = indexToTimeInterval(gsw->defaultTimeInterval->currentIndex());
#endif
    Settings::setGraphSettingsAndSync(gsw->graphId, gs);
  }
  QMainWindow::closeEvent(event);
}

GraphSettingsWidget::GraphSettingsWidget(QString graphId, QWidget *parent) :
    QWidget(parent), graphId(graphId) {
  QWidget *rootContainer = new QWidget(this);
  QGridLayout *rootLayout = new QGridLayout(rootContainer);

  QWidget *topSpacer = new QWidget(rootContainer);
  topSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  rootLayout->addWidget(topSpacer, 0, 0);

  GraphSettings gs = Settings::graphSettings(graphId);

  goalWeightEnabled = new QPushButton("Show goal weight", rootContainer);
  goalWeightEnabled->setCheckable(true);
  goalWeightEnabled->setChecked(gs.goalWeightEnabled);
  rootLayout->addWidget(goalWeightEnabled, 1, 0);

  QStringListModel *timeIntervalModel =
      new QStringListModel(timeIntervalStrings(), rootContainer);
#ifdef Q_WS_MAEMO_5
  defaultTimeInterval= new QMaemo5ValueButton("Time interval", rootContainer);
  defaultTimeInterval->setValueLayout(QMaemo5ValueButton::ValueUnderTextCentered);
  QMaemo5ListPickSelector *timeIntervalSelector =
      new QMaemo5ListPickSelector(rootContainer);
  timeIntervalSelector->setModel(timeIntervalModel);
  timeIntervalSelector->setCurrentIndex(timeIntervalToIndex(gs.defaultTimeInterval));
  defaultTimeInterval->setPickSelector(timeIntervalSelector);
#else
  defaultTimeInterval = new QComboBox(rootContainer);
  defaultTimeInterval->setModel(timeIntervalModel);
  defaultTimeInterval->setCurrentIndex(timeIntervalToIndex(gs.defaultTimeInterval));
#endif
  rootLayout->addWidget(defaultTimeInterval, 2, 0);

//  QWidget *spacer = new QWidget(rootContainer);
//  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
//  rootLayout->addWidget(spacer, 0, 1, 2, 1);

  QFrame *weightFrame = new QFrame(rootContainer);
  weightFrame->setFrameShadow(QFrame::Sunken);
  weightFrame->setFrameStyle(QFrame::StyledPanel);
  weightFrame->setLineWidth(2);
  weightFrame->setMidLineWidth(2);
  QGridLayout *weightLayout = new QGridLayout(weightFrame);

  QStringList weightIntervalModes;
  weightIntervalModes.append("Auto with goal weight");
  weightIntervalModes.append("Auto without goal weight");
  weightIntervalModes.append("Manual");
  QStringListModel *weightIntervalModeModel =
      new QStringListModel(weightIntervalModes, rootContainer);
#ifdef Q_WS_MAEMO_5
  weightIntervalMode = new QMaemo5ValueButton("Weight interval mode", rootContainer);
  weightIntervalMode->setValueLayout(QMaemo5ValueButton::ValueUnderTextCentered);
  QMaemo5ListPickSelector *weightIntervalModeSelector =
      new QMaemo5ListPickSelector(rootContainer);
  weightIntervalModeSelector->setModel(weightIntervalModeModel);
  weightIntervalModeSelector->setCurrentIndex((int)gs.weightIntervalMode);
  weightIntervalMode->setPickSelector(weightIntervalModeSelector);
  connect(weightIntervalMode->pickSelector(), SIGNAL(selected(QString)),
          this, SLOT(weightIntervalModeChanged(QString)));
#else
  weightIntervalMode = new QComboBox(rootContainer);
  weightIntervalMode->setModel(weightIntervalModeModel);
#endif
  weightLayout->addWidget(weightIntervalMode, 0, 0, 1, 3);


  weightIntervalMin = new WeightSpinBox(rootContainer);
  weightIntervalMin->setEnabled(gs.weightIntervalMode == GraphSettings::Manual);
  weightIntervalMin->setValue(gs.weightIntervalMin);
  weightLayout->addWidget(weightIntervalMin, 1, 0);

  weightLayout->addWidget(new QLabel("-", rootContainer), 1, 1);

  weightIntervalMax = new WeightSpinBox(rootContainer);
  weightIntervalMax->setEnabled(gs.weightIntervalMode == GraphSettings::Manual);
  weightIntervalMax->setValue(gs.weightIntervalMax);
  weightLayout->addWidget(weightIntervalMax, 1, 2);

  rootLayout->addWidget(weightFrame, 1, 1, 2, 1);

  QWidget *bottomSpacer = new QWidget(rootContainer);
  bottomSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  rootLayout->addWidget(bottomSpacer, 4, 0);

}
void GraphSettingsWidget::weightIntervalModeChanged(QString mode) {
  bool isManual = mode == "Manual";
  weightIntervalMin->setEnabled(isManual);
  weightIntervalMax->setEnabled(isManual);
}

