#!/bin/sh

MAD=/opt/NokiaQtSDK/Maemo/4.6.2/bin/mad
APPNAME=weightgraph
VERSION=`head -1 debian/changelog | cut -d'(' -f 2 | cut -d')' -f 1`
DIR=/tmp/$APPNAME-$VERSION
rm -rf $DIR
cp -r . $DIR
cd $DIR
$MAD dpkg-buildpackage -sa -S
$MAD dpkg-buildpackage
