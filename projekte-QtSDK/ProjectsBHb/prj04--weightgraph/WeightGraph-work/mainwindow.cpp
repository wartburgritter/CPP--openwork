//#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "weightdata.h"
#include "weightstatsview.h"
#include "weightgraphview.h"
#include <QtGui/QX11Info>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

extern WeightDataModel *wdm;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

  //////////////////////// IMPLEMENTATION OBJEKTE ////////////////////////

  weight = new WeightSpinBox();
  if(wdm->size() > 0)
    weight->setValue(wdm->getWeights().last().weight);
  weight->setFont(QFont("Arial", 60, QFont::Light));


  QPushButton *setButton = new QPushButton("SET");
  setButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  setButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(setButton, SIGNAL(clicked()), this, SLOT(setTodaysWeight()));


  WeightStatsView *stats = new WeightStatsView(wdm, this);


  ew = new EditWindow(this);
  QPushButton *listButton = new QPushButton("EDIT");
  listButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  listButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(listButton, SIGNAL(clicked()), ew, SLOT(show()));


  bigGraph = new WeightGraphView(wdm, "Big", this);               // extra Fenster
  bigGraph->setWindowFlags(bigGraph->windowFlags() | Qt::Window);
  QPushButton *bigGraphButton = new QPushButton("Detail View");
  bigGraphButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  bigGraphButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(bigGraphButton, SIGNAL(clicked()), bigGraph, SLOT(show()));


  smallGraph = new WeightGraphView(wdm, "Small", this);           // extra Fenster
  smallGraph->setWindowFlags(smallGraph->windowFlags() | Qt::Window);
  QPushButton *smallGraphButton = new QPushButton("Total View");
  smallGraphButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  smallGraphButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(smallGraphButton, SIGNAL(clicked()), smallGraph, SLOT(show()));


  aboutDialog = new QMessageBox(QMessageBox::NoIcon, "About WeightGraph",
                                "Copyright (C) 2011 Visa Putkinen. Licence: GPLv2 "
                                "N9 (Harmattan) Version 2013 Bernd Hahnebach"
                                "VersionBHB 0.1.2. July 28, 2013",
                                QMessageBox::Close, this);
  aboutDialog->setIconPixmap(QPixmap(":/img/icon48"));
  aboutDialog->setFont(QFont("Arial", 15, QFont::Light));
  aboutDialog->setInformativeText("Usage: enter your weight every day using "
                                  "the main screen's \"Today's weight?\" box "
                                  "or the List / edit window. You may enter "
                                  "at most one weight per day."
                                  "\n\n"
                                  "A graph of the weights will be drawn when "
                                  "two or more weights are entered. Tap the "
                                  "graph to open a larger graph view. Use the "
                                  "external zoom buttons to adjust the shown "
                                  "time period."
                                  "\n\n"
                                  "The weights are stored in human readable (and "
                                  "writeable) form in "
                                  "MyDocs/WeightGraph/weightdata.txt");
  QPushButton *aboutButton = new QPushButton("About");
  aboutButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  aboutButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(aboutButton, SIGNAL(clicked()), aboutDialog, SLOT(show()));


  //Important: SettingsWindow must be created after all graph
  //views are created or settings won't show all graphs
  settingsWindow = new SettingsWindow(this);
  QPushButton *settingsButton = new QPushButton("Settings");
  settingsButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  settingsButton->setFont(QFont("Arial", 30, QFont::Light));
  connect(settingsButton, SIGNAL(clicked()), settingsWindow, SLOT(show()));



  //////////////////////// LAYOUT ////////////////////////

  //statusBar()->showMessage( "The job failed.", 5000 );


  setWindowTitle("WeightGraph");
  resize(854, 480); // Aufloesung N9 wegen Verhaeltnis Schriftgroesse zu Screen
  //resize(100, 100);   // zu klein es minimal benoetigte Grosse gewaehlt


  QGroupBox *gridGroupBox1 = new QGroupBox;
  QGridLayout *topLayout = new QGridLayout;
  topLayout->addWidget(setButton,        0,0);
  topLayout->addWidget(listButton,       1,0);
  topLayout->addWidget(weight,           0,1,1,4);
  topLayout->addWidget(stats,            1,3,1,2);
  gridGroupBox1->setLayout(topLayout);


  QGroupBox *gridGroupBox2 = new QGroupBox;
  QGridLayout *bottomLayout = new QGridLayout;
  bottomLayout->addWidget(smallGraphButton, 0,0);
  bottomLayout->addWidget(aboutButton,      1,0);
  bottomLayout->addWidget(bigGraphButton,   0,1);
  bottomLayout->addWidget(settingsButton,   1,1);
  gridGroupBox2->setLayout(bottomLayout);


  QWidget *central = new QWidget(this);
  QVBoxLayout *mainLayout = new QVBoxLayout(central);
  mainLayout->addWidget(gridGroupBox1);
  mainLayout->addWidget(gridGroupBox2);
  this->setCentralWidget(central);


  // gibt error, daher ausdokumentiert
  // grabZoomKeys(Settings::grabZoomKeys());

  connect(Settings::self(), SIGNAL(settingChanged()), this, SLOT(update()));
}




/*
// ohne rueckfrage
void MainWindow::setTodaysWeight()
{
  wdm->setWeightForDate(QDate::currentDate(), weight->value());
}
*/


/*// mit rueckfrage mit hilfsdaten extra DW
void MainWindow::setTodaysWeight()
{
      DW dw;
      dw.date = QDate::currentDate();
      dw.weight = weight->value();
      if (wdm->dateExists(dw.date)) {
        QMessageBox::warning(this, "Duplicate", "A weight for this date already exists.\n"
                                                   "Only one weight per date allowed.");
        return;
      }
      wdm->setWeightForDate(dw);
 }
*/

// mit rueckfrage ohne extra Hilfsdaten DW
void MainWindow::setTodaysWeight()
{
      if (wdm->dateExists(QDate::currentDate())) {
        QMessageBox::warning(this, "Duplicate", "A weight for this date already exists.\n"
                                                   "Only one weight per date allowed.");
        return;
      }
      wdm->setWeightForDate(QDate::currentDate(), weight->value());
}


void MainWindow::keyPressEvent(QKeyEvent* event)
{
  //qDebug() << "Main window: key pressed: " << event->key();
  switch (event->key()) {
  case Qt::Key_F7:
      smallGraph->decPeriod();
      event->accept();
      break;

  case Qt::Key_F8:
      smallGraph->incPeriod();
      event->accept();
      break;
  }
  QWidget::keyPressEvent(event);
}

/* gibt error
void MainWindow::grabZoomKeys(bool grab)
{
  if (!winId()) {
    qWarning("Can't grab keys unless we have a window id");
    return;
  }

  unsigned long val = (grab) ? 1 : 0;
  Atom atom = XInternAtom(QX11Info::display(), "_HILDON_ZOOM_KEY_ATOM", False);
  if (!atom) {
    qWarning("Unable to obtain _HILDON_ZOOM_KEY_ATOM. This example will only work "
             "on a Maemo 5 device!");
    return;
  }


  XChangeProperty (QX11Info::display(),
                   winId(),
                   atom,
                   XA_INTEGER,
                   32,
                   PropModeReplace,
                   reinterpret_cast<unsigned char *>(&val),
                   1);

  //qDebug() << "Main window grabbed zoom keys: " << winId();
}
*/
