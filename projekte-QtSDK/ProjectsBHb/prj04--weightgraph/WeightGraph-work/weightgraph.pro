#-------------------------------------------------
#
# Project created by QtCreator 2011-01-16T18:23:01
#
#-------------------------------------------------

QT       += core gui maemo5

TARGET = weightgraph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    weightdata.cpp \
    editwindow.cpp \
    weightstatsview.cpp \
    weightgraphview.cpp \
    settings.cpp \
    settingswindow.cpp

HEADERS  += mainwindow.h \
    weightdata.h \
    editwindow.h \
    weightview.h \
    weightspinbox.h \
    weightstatsview.h \
    weightgraphview.h \
    settings.h \
    settingswindow.h

FORMS    += mainwindow.ui


CONFIG += mobility console
MOBILITY = 

RESOURCES += \
    res.qrc




OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog


contains(MEEGO_EDITION,harmattan) {
    icon.files = weightgraph80.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    INSTALLS += icon
}

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/weightgraph/bin
    INSTALLS += target
}

maemo5 {
    target.path = /opt/weightgraph/bin
    INSTALLS += target
}

contains(MEEGO_EDITION,harmattan) {
    desktopfile.files = weightgraph_harmattan.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
}
