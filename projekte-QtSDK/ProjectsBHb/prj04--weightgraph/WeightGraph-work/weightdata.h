#ifndef WEIGHTDATA_H
#define WEIGHTDATA_H

#include <QAbstractTableModel>
#include <QDate>
#include <QList>
#include <QFile>
#include <QDoubleSpinBox>
#include <QItemEditorCreatorBase>

//For debugging:
#include <iostream>

// A table model with 2 columns: Date | Weight
class WeightDataModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  struct DateWeight
  {
    QDate date;
    double weight;
    bool operator<(const DateWeight &o) const { return date < o.date; }
  };
  typedef QList<DateWeight> WeightList;
  WeightDataModel(QString &datafile, QObject *parent = 0);
  int rowCount(const QModelIndex &/**/) const { return weights.size(); }
  int columnCount(const QModelIndex &/**/) const { return 2; }
  Qt::ItemFlags flags(const QModelIndex &index) const;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role);
  bool setDataForRow(int row, const DateWeight &dw);
  void setWeightForDate(const QDate &date, double weight);
  void setWeightForDate(const DateWeight &dw);
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
  bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
  int rowOfDate(const QDate &date) const;
  QModelIndex indexOfDate(const QDate &date) const;
  bool dateExists(const QDate &date) const;
  int rowForNewDate(const QDate &date) const;
  void clear();

  int size() const { return weights.size(); }
  const WeightList &getWeights() const { return weights; }
private:
  void writeToDisk();
  void readFromDisk();

private:
  WeightList weights;
  QFile datafile;
};

typedef WeightDataModel::DateWeight DW;

#endif // WEIGHTDATA_H
