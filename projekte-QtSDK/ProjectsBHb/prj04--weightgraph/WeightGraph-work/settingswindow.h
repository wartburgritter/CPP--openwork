#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <QPushButton>
#include <QList>
#ifdef Q_WS_MAEMO_5
# include <QtMaemo5>
#else
# include <QComboBox>
#endif
#include "weightspinbox.h"

class GraphSettingsWidget;

class SettingsWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit SettingsWindow(QWidget *parent = 0);
protected:
  void closeEvent(QCloseEvent *);
signals:

public slots:
private:
#ifdef Q_WS_MAEMO_5
  QMaemo5ValueButton *weightUnit;
#else
  QComboBox *weightUnit;
#endif
  WeightSpinBox *goalMin;
  WeightSpinBox *goalMax;
  QPushButton *grabZoomKeys;
  QList<GraphSettingsWidget*> *graphSettingsList;
};

class GraphSettingsWidget : public QWidget {
  Q_OBJECT
public:
  GraphSettingsWidget(QString graphId, QWidget *parent);
private slots:
  void weightIntervalModeChanged(QString mode);
public:
  QString graphId;
  QPushButton *goalWeightEnabled;
#ifdef Q_WS_MAEMO_5
  QMaemo5ValueButton *weightIntervalMode;
  QMaemo5ValueButton *defaultTimeInterval;
#else
  QComboBox *weightIntervalMode;
  QComboBox *defaultTimeInterval;
#endif

  WeightSpinBox *weightIntervalMin;
  WeightSpinBox *weightIntervalMax;
};

#endif // SETTINGSWINDOW_H
