//#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "weightdata.h"
#include "weightstatsview.h"
#include "weightgraphview.h"
#include <QtGui/QX11Info>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

extern WeightDataModel *wdm;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
  setWindowTitle("WeightGraph");

  QWidget *central = new QWidget(this);
  QHBoxLayout *topLayout = new QHBoxLayout(central);


  // vv LEFT SIDE BEGINS vv
  QWidget *leftContainer = new QWidget(central);
  QVBoxLayout *leftLayout = new QVBoxLayout(leftContainer);
  leftLayout->setSpacing(1);
  leftLayout->setMargin(1);

  // vvv LEFT TOP BEGINS vvv
  //QGroupBox * leftTopContainer = new QGroupBox("Today's weight?", leftContainer);
  QFrame *leftTopContainer = new QFrame(leftContainer);
  leftTopContainer->setFrameShadow(QFrame::Sunken);
  leftTopContainer->setFrameStyle(QFrame::StyledPanel);
  leftTopContainer->setLineWidth(2);
  leftTopContainer->setMidLineWidth(2);
  QGridLayout *leftTopLayout = new QGridLayout(leftTopContainer);

  QLabel *todayLabel = new QLabel("Today's weight?", leftContainer);
  todayLabel->setFont(QFont("Arial", 20, QFont::Light));
  todayLabel->setAlignment(Qt::AlignCenter);
  leftTopLayout->addWidget(todayLabel, 1, 0);

  weight = new WeightSpinBox(leftTopContainer);
  if(wdm->size() > 0)
    weight->setValue(wdm->getWeights().last().weight);
  weight->setFont(QFont("Arial", 55, QFont::Light));
  leftTopLayout->addWidget(weight, 0, 0);

  QPushButton *setButton = new QPushButton("Set", leftTopContainer);
  setButton->setFont(QFont("Arial", 25, QFont::Light));
  connect(setButton, SIGNAL(clicked()), this, SLOT(setTodaysWeight()));
  leftTopLayout->addWidget(setButton, 2, 0);

  leftLayout->addWidget(leftTopContainer);
  // ^^^ LEFT TOP ENDS ^^^



//  QWidget *vspacer0 = new QWidget(leftContainer);
//  vspacer0->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
//  leftLayout->addWidget(vspacer0);



  WeightStatsView *stats = new WeightStatsView(wdm, this);
  leftLayout->addWidget(stats);

//  QWidget *vspacer = new QWidget(leftContainer);
//  vspacer->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
//  leftLayout->addWidget(vspacer);

  ew = new EditWindow(this);
  QPushButton *listButton = new QPushButton("List / edit", leftContainer);
  listButton->setFont(QFont("Arial", 25, QFont::Light));
  connect(listButton, SIGNAL(clicked()), ew, SLOT(show()));
  leftLayout->addWidget(listButton);

  topLayout->addWidget(leftContainer);
  // ^^ LEFT SIDE ENDS ^^


/*
  smallGraph = new WeightGraphView(wdm, "Small", central);  // im mainwindow
  bigGraph = new WeightGraphView(wdm, "Big", this);         // in extra fenster


//#ifdef Q_WS_MAEMO_5
//  bigGraph->setAttribute(Qt::WA_Maemo5StackedWindow);
//  bigGraph->grabZoomKeys(Settings::grabZoomKeys());
//#endif
  bigGraph->setWindowFlags(bigGraph->windowFlags() | Qt::Window);
  connect(smallGraph, SIGNAL(clicked()), bigGraph, SLOT(show()));
  topLayout->addWidget(smallGraph);

  this->setCentralWidget(central);
*/


/////varianten bhb///////
/*
  // smallGraph und bigGraph vertauscht
  smallGraph = new WeightGraphView(wdm, "Small", this);       // extra
  bigGraph = new WeightGraphView(wdm, "Big", central);        // main
  smallGraph->setWindowFlags(smallGraph->windowFlags() | Qt::Window);
  connect(bigGraph, SIGNAL(clicked()), smallGraph, SLOT(show()));
  topLayout->addWidget(bigGraph);

  this->setCentralWidget(central);
*/

/*  // beide graphen im mainwindow
  smallGraph = new WeightGraphView(wdm, "Small", central);
  bigGraph = new WeightGraphView(wdm, "Big", central);
  topLayout->addWidget(smallGraph);
  topLayout->addWidget(bigGraph);

  this->setCentralWidget(central);
*/

  // beide graphen mit buttons
  // warum muessen die elemente nicht im header deklariert werden ???
  QPushButton *smallGraphButton = new QPushButton("SmallGraph", central);
  smallGraphButton->setFont(QFont("Arial", 25, QFont::Light));
  //topLayout->addWidget(smallGraphButton);

  QPushButton *bigGraphButton = new QPushButton("BigGraph", central);
  bigGraphButton->setFont(QFont("Arial", 25, QFont::Light));
  //topLayout->addWidget(bigGraphButton);


  bigGraph = new WeightGraphView(wdm, "Big", this);        // extra
  bigGraph->setWindowFlags(bigGraph->windowFlags() | Qt::Window);
  connect(bigGraphButton, SIGNAL(clicked()), bigGraph, SLOT(show()));

  smallGraph = new WeightGraphView(wdm, "Small", this);       // extra
  smallGraph->setWindowFlags(smallGraph->windowFlags() | Qt::Window);
  connect(smallGraphButton, SIGNAL(clicked()), smallGraph, SLOT(show()));

  this->setCentralWidget(central);


  // da muss wohl evtl. noch eine container rein???

  QWidget *rightContainer = new QWidget(central);
  QVBoxLayout *rightLayout = new QVBoxLayout(rightContainer);
  rightLayout->setSpacing(1);
  rightLayout->setMargin(1);


  ///ende varianten///


  aboutDialog = new QMessageBox(QMessageBox::NoIcon, "About WeightGraph",
                                "Copyright (C) 2011 Visa Putkinen. Licence: GPLv2",
                                QMessageBox::Close, this);
  aboutDialog->setIconPixmap(QPixmap(":/img/icon48"));
  aboutDialog->setFont(QFont("Arial", 15, QFont::Light));
  aboutDialog->setInformativeText("Usage: enter your weight every day using "
                                  "the main screen's \"Today's weight?\" box "
                                  "or the List / edit window. You may enter "
                                  "at most one weight per day."
                                  "\n\n"
                                  "A graph of the weights will be drawn when "
                                  "two or more weights are entered. Tap the "
                                  "graph to open a larger graph view. Use the "
                                  "external zoom buttons to adjust the shown "
                                  "time period."
                                  "\n\n"
                                  "The weights are stored in human readable (and "
                                  "writeable) form in "
                                  "MyDocs/WeightGraph/weightdata.txt");

  QPushButton *aboutButton = new QPushButton("About", central);
  aboutButton->setFont(QFont("Arial", 25, QFont::Light));
  topLayout->addWidget(aboutButton);
  connect(aboutButton, SIGNAL(clicked()), aboutDialog, SLOT(show()));


  //Important: SettingsWindow must be created after all graph
  //views are created or settings won't show all graphs
  settingsWindow = new SettingsWindow(this);

  QPushButton *settingsButton = new QPushButton("Settings", central);
  settingsButton->setFont(QFont("Arial", 25, QFont::Light));
  topLayout->addWidget(settingsButton);
  connect(settingsButton, SIGNAL(clicked()), settingsWindow, SLOT(show()));


  rightLayout->addWidget(settingsButton);
  rightLayout->addWidget(aboutButton);
  rightLayout->addWidget(smallGraphButton);
  rightLayout->addWidget(bigGraphButton);

  topLayout->addWidget(rightContainer);


//  grabZoomKeys(Settings::grabZoomKeys());

  connect(Settings::self(), SIGNAL(settingChanged()), this, SLOT(update()));
}




void MainWindow::setTodaysWeight()
{
  wdm->setWeightForDate(QDate::currentDate(), weight->value());
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
  //qDebug() << "Main window: key pressed: " << event->key();
  switch (event->key()) {
  case Qt::Key_F7:
      smallGraph->decPeriod();
      event->accept();
      break;

  case Qt::Key_F8:
      smallGraph->incPeriod();
      event->accept();
      break;
  }
  QWidget::keyPressEvent(event);
}

/* gibt error
void MainWindow::grabZoomKeys(bool grab)
{
  if (!winId()) {
    qWarning("Can't grab keys unless we have a window id");
    return;
  }

  unsigned long val = (grab) ? 1 : 0;
  Atom atom = XInternAtom(QX11Info::display(), "_HILDON_ZOOM_KEY_ATOM", False);
  if (!atom) {
    qWarning("Unable to obtain _HILDON_ZOOM_KEY_ATOM. This example will only work "
             "on a Maemo 5 device!");
    return;
  }


  XChangeProperty (QX11Info::display(),
                   winId(),
                   atom,
                   XA_INTEGER,
                   32,
                   PropModeReplace,
                   reinterpret_cast<unsigned char *>(&val),
                   1);

  //qDebug() << "Main window grabbed zoom keys: " << winId();
}
*/
