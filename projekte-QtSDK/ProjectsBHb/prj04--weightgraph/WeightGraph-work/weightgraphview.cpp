#include "weightgraphview.h"
#include "settings.h"
#include <QPainter>
#include <QDebug>
#include <QTimer>
#include <cmath>
#include <QtGui/QX11Info>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

//////////////////////////////////////////////////////////////////////
WeightGraphView::WeightGraphView(WeightDataModel *wdm,
                                 const QString &id, QWidget *parent) :
    QWidget(parent), id(id), wdm(wdm),
    period(Settings::graphSettings(id).defaultTimeInterval)
{
  setWindowTitle("WeightGraph meener");

  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  connect(wdm, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
          this, SLOT(update()));
  connect(wdm, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
          this, SLOT(update()));
  connect(wdm, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
          this, SLOT(update()));
  connect(Settings::self(), SIGNAL(settingChanged()),
          this, SLOT(update()));
}

//////////////////////////////////////////////////////////////////////
void WeightGraphView::mousePressEvent(QMouseEvent *)
{
  emit clicked();
}

//////////////////////////////////////////////////////////////////////
QSize WeightGraphView::sizeHint() const
{
  return QSize(300, 400);
}

//////////////////////////////////////////////////////////////////////
void WeightGraphView::keyPressEvent(QKeyEvent* event)
{
  //qDebug() << "Key pressed: " << event->key();
  switch (event->key()) {
  case Qt::Key_F7:
      decPeriod();
      event->accept();
      break;

  case Qt::Key_F8:
      incPeriod();
      event->accept();
      break;
  }
  QWidget::keyPressEvent(event);
}

//////////////////////////////////////////////////////////////////////
// Macros to transform dates and weights to paintdevice coords
#define D_X(d) ((d)*qreal(width())/days)
#define DW_X(dw) D_X(f.daysTo((dw).date))
#define W_Y(w) (height()*(maxW-(w))/weightInterval)
#define DW_Y(dw) W_Y((dw).weight)
#define DW_POINT(dw) QPointF(DW_X(dw), DW_Y(dw))

inline double weightIntervalToMult(double wi)
{
  if (wi <= 0.2) return 0.01;
  else if (wi <= 0.5) return 0.025;
  else if (wi <= 1.0) return 0.05;
  else if (wi <= 2.0) return 0.1;
  else if (wi <= 5.0) return 0.25;
  else if (wi <= 10.0) return 0.5;
  else if (wi <= 20.0) return 1.0;
  else if (wi <= 50.0) return 2.5;
  else if (wi <= 100.0) return 5.0;
  else if (wi <= 200.0) return 10.0;
  else if (wi <= 500.0) return 25.0;
  else if (wi <= 1000.0) return 50.0;
  else return 100.0;
}

//////////////////////////////////////////////////////////////////////
void WeightGraphView::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);

  GraphSettings gs = Settings::graphSettings(id);
  const QList<DW> &allWeights = wdm->getWeights();

  double min, max;
  QList<DW> weights;
  const DW *beforeFirst = NULL;
  if (allWeights.size() < 2) {
    min = 0;
    max = 100;
  }
  else {
    bool firstFound = false;
    min = 1e30;
    max = -1e30;
    const QDate &l = allWeights.last().date;
    for(int i=0; i < allWeights.size(); i++) {
      if (period == 0 || firstFound || allWeights[i].date.daysTo(l) <= period) {
        weights.append(allWeights[i]);
        if (allWeights[i].weight < min)
          min = allWeights[i].weight;
        if (allWeights[i].weight > max)
          max = allWeights[i].weight;
        firstFound = true;
      }
      if (!firstFound)
        beforeFirst = &allWeights[i];
    }
  }

  if (gs.weightIntervalMode == GraphSettings::AutomaticWithGoalWeight
      && gs.goalWeightEnabled) {
    min = qMin(min, Settings::goalWeightMin());
    max = qMax(max, Settings::goalWeightMax());
  }
  else if(gs.weightIntervalMode == GraphSettings::Manual) {
    if (gs.weightIntervalMax > gs.weightIntervalMin) {
      min = gs.weightIntervalMin;
      max = gs.weightIntervalMax;
    }
  }
  // else default is min and max of actual data



  double margin = (max - min)*0.03;
  double minW = min - margin;
  double maxW = max + margin;
  double weightInterval = maxW-minW;
  if (maxW-minW < 0.1) {
    minW = min-0.003;
    maxW = max+0.003;
    weightInterval = maxW - minW;
  }
  QDate f, l;
  int days;
  if (weights.size() < 2) {
    l = QDate::currentDate();
    f = l.addDays(-7);
    days = 7;
  }
  else if (period==0) {
    f = weights.first().date;
    l = weights.last().date;
    days = f.daysTo(l);
  } else {
    l = weights.last().date;
    f = l.addDays(-period);
    days = period;
  }


  // Interpolate to fill gap in left part of the graph
  if (weights.size() >= 2 && weights.first().date != f && beforeFirst != NULL) {
    DW dw;
    dw.date = f;
    dw.weight = (weights.first().weight - beforeFirst->weight)
                /beforeFirst->date.daysTo(weights.first().date)
                *beforeFirst->date.daysTo(f)
                +beforeFirst->weight;
    weights.prepend(dw);
  }

  // DW ist eine zeile in der liste, oder ???
  // versuch auch eine DW des Typs QList anzulegen und mit averangewerten zu belegen
  // ich checke das listenzeug einfach nicht
  // ich kann ja nicht mal die berechnungen oben nach vollziehen, da ich ja noch
  // nicht mal die berechneten werte irgendwie ausgeben koennte.
  // Ich habe einfach keine ahnung von C++
  //  DW average;



  // jetzt wird wirklich gezeichnet !!!!!!!!!!!!!

  painter.setWindow(-50, 0, width()+85, height()+25);
  // test: rote linie von links unten bis rechts oben
  //painter.setPen(QColor("red"));
  //painter.drawLine(QPointF(0,height()), QPointF(width(),0));
  //painter.setPen(QColor("black"));


  // rechteck zielgewicht (aktuelle gruen)
  if (gs.goalWeightEnabled
      && ((Settings::goalWeightMin() > minW && Settings::goalWeightMin() < maxW)
          ||(Settings::goalWeightMax() > minW && Settings::goalWeightMax() < maxW))) {
    QPen oldPen = painter.pen();
    QBrush oldBrush = painter.brush();
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0,255,0,100));
    //painter.setBrush(QColor("red")); // rechteck in rot
    painter.setClipRect(QRectF(0,0,width(),height()));
    painter.drawRect(QRectF(QPointF(0, W_Y(Settings::goalWeightMax())),
                            QPointF(width(), W_Y(Settings::goalWeightMin()))));
    painter.setClipping(false);
    painter.setPen(oldPen);
    painter.setBrush(oldBrush);
  }



  // ACHSEN (lange linie, die kleinen linien, Text)
  // noetig fuer beide achsen
  QFont font = painter.font();
  double mult;

  // Y-axis und die horizontalen linien
  font.setPixelSize(16);
  painter.setFont(font);
  painter.drawLine(QPointF(0.0,0.0), QPointF(0,height()));
  mult = weightIntervalToMult(weightInterval);
  int count = 0;
  for(double w=ceil(minW/mult)*mult; w < maxW; w += mult, count++) {
    double len = count%5==0 ? 7.0 : 4.0;
    painter.drawLine(QPointF(-len,W_Y(w)),QPointF(len,W_Y(w)));
    if (count%5 == 0) {
      QPen p = painter.pen();
      painter.setPen(QColor(50,50,50));
      painter.drawLine(QPointF(len,W_Y(w)),QPointF(width(),W_Y(w)));
      painter.setPen(p);
      QString text = tr("%1").arg(double(w),0,'f', mult <= 0.25 ? 2 : 1);
      QSize textSize = painter.fontMetrics().size(0, text);
      painter.drawText(QPointF(-len-3-textSize.width(), W_Y(w)+6), text);
    }
  }

  // X-axis
  font.setPixelSize(13);
  painter.setFont(font);
  double endOfLastDate = -1e6;
  mult = days/30+1;
  painter.drawLine(QPointF(0.0,height()), QPointF(width(),height()));
  for(int day=0; day <= days; day+=mult) {
    QString text = f.addDays(day).toString(Qt::ISODate);
    QSize textSize = painter.fontMetrics().size(0, text);
    double tickLen;
    if (D_X(day)-textSize.width()/2 > endOfLastDate + 10) {
      tickLen = 5.0;
      painter.drawText(QPointF(D_X(day)-textSize.width()/2,
                               W_Y(minW)+18), text);
      endOfLastDate = D_X(day)+textSize.width()/2;
    }
    else
      tickLen = 3.0;
    painter.drawLine(QPointF(D_X(day), W_Y(minW)-tickLen),
                     QPointF(D_X(day), W_Y(minW)+tickLen));
  }



  // The weight data (eigentliche linie des gewichts)
  QPolygonF linepoints;
  foreach(DW dw, weights) {
    linepoints << DW_POINT(dw);
  }
  painter.drawPolyline(linepoints);



/*  // The weight data (zweite linie, meine linie)
  QPolygonF linepoints2;
  foreach(DW dw, mydw) {
    linepoints2 << DW_POINT(dw);
  }
  painter.setPen(QColor("red"));
  painter.drawPolyline(linepoints2);
*/

}


//////////////////////////////////////////////////////////////////////
void WeightGraphView::show()
{
  QWidget::show();
//  grabZoomKeys(Settings::grabZoomKeys()); //Need to be regrabbed somewhy
  //Work around a bug: hidden graphs don't update. Must wait for the
  //graph to actually show up, then call update.
  QTimer *tmp = new QTimer(this);
  tmp->setSingleShot(true);
  tmp->setInterval(500);
  connect(tmp, SIGNAL(timeout()), this, SLOT(update()));
  tmp->start();
}

/* gibt errors
void WeightGraphView::grabZoomKeys(bool grab)
{
  if (!winId()) {
    qWarning("Can't grab keys unless we have a window id");
    return;
  }

  unsigned long val = (grab) ? 1 : 0;
  Atom atom = XInternAtom(QX11Info::display(), "_HILDON_ZOOM_KEY_ATOM", False);
  if (!atom) {
    qWarning("Unable to obtain _HILDON_ZOOM_KEY_ATOM. This example will only work "
             "on a Maemo 5 device!");
    return;
  }


  XChangeProperty (QX11Info::display(),
                   winId(),
                   atom,
                   XA_INTEGER,
                   32,
                   PropModeReplace,
                   reinterpret_cast<unsigned char *>(&val),
                   1);

  //qDebug() << "Grabbed for winId " << winId();
}
*/
