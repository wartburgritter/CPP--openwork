#-------------------------------------------------
#
# Project created by QtCreator 2011-11-06T16:45:09
#
#-------------------------------------------------

QT       += core gui

TARGET = fenstertextprj
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

symbian {
    TARGET.UID3 = 0xeca15c99
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog



contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/fenstertextprj/bin
    INSTALLS += target
}

maemo5 {
    target.path = /opt/fenstertextprj/bin
    INSTALLS += target
}
