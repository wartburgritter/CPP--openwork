#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include "editwindow.h"
#include "weightspinbox.h"
#include "weightgraphview.h"
#include "settingswindow.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
public slots:
  void grabZoomKeys(bool grab);
private slots:
  void setTodaysWeight();
  void update() {
    grabZoomKeys(Settings::grabZoomKeys());
    QWidget::update();
  }
protected:
  void keyPressEvent(QKeyEvent* event);
private:
  //Ui::MainWindow *ui;
  WeightSpinBox *weight;
  EditWindow *ew;
  WeightGraphView *smallGraph;
  WeightGraphView *bigGraph;
  SettingsWindow *settingsWindow;
  QMessageBox *aboutDialog;
};

#endif // MAINWINDOW_H
