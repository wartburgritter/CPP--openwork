#-------------------------------------------------
#
# Project created by QtCreator 2010-11-12T00:48:56
#
#-------------------------------------------------

QT       += core gui

TARGET = BatteryIndicator
TEMPLATE = app


SOURCES += main.cpp\
        batteryindicator.cpp

HEADERS  += batteryindicator.h

FORMS    += batteryindicator.ui

CONFIG += mobility
MOBILITY = systeminfo

symbian {
    TARGET.UID3 = 0xe1c86fe6
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}
